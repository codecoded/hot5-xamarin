﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class SearchResponse
  {
  
    public string Term { get; set;}
    public SearchResults Results { get; set;} 

  }
}

