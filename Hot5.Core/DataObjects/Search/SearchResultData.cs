﻿using System;

namespace Hot5.Core
{
  public class SearchResultData
  {
    public string Slug { get; set; }
    public string Title { get; set; }
  }
}

