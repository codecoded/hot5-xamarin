﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class SearchResults
  {
    public IList<SearchResult> System { get; set;}
    public IList<SearchResult> City { get; set;}
    public IList<SearchResult> Country { get; set;}
    public IList<SearchResult> Hotel { get; set;}
    public IList<SearchResult> Place { get; set;}
    public IList<SearchResult> Landmark { get; set;}
    public IList<SearchResult> Region { get; set;}
  }
}

