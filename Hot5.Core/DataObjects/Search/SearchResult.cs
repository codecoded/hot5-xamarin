﻿using System;

namespace Hot5.Core
{
  public class SearchResult
  {
    public int Id { get; set; }
    public string Term { get; set; }
    public int? Score { get; set; }
    public SearchResultData Data { get; set; }

    public SearchResultSelected SearchResultSelectedDelegate {get;set;}
    public delegate void SearchResultSelected(SearchResult searchResult);

  }
}

