﻿using System;
using System.Collections.Generic;
using LeadedSky.Shared;
using System.Linq;

namespace Hot5.Core
{
  public class FilterOptions
  {
    public enum Headers
    {
      Guests,
      StarRatings,
      Amenities
    }

    public static List<TableRow> CreateOptionsList(List<string> starRatings= null, List<string> amenities = null)
    {
      var options = new List<TableRow>();
//      options.AddRange(Guests());
      options.AddRange(StarRatings(starRatings));
      options.AddRange(Amenities(amenities));
      return options;
    }

    public static List<TableRow> Guests()
    {
      var group_id = Headers.Guests.ToString();

      return new List<TableRow> {
        new TableRowHeader {
          Value = group_id,
          Title = "Guests",
          Detail = "All"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "1 room, 1 guest",
          SelectType = TableRowItemSelectType.Radio,
          Value = "1"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "1 room, 2 guests",
          SelectType = TableRowItemSelectType.Radio,
          Value = "2"
        }
      };
    }

    public static List<TableRow> StarRatings(List<string> selectedValues)
    {
      var group_id = Headers.StarRatings.ToString();

      var options = new List<TableRow> {
        new TableRowHeader {
          Value = group_id,
          Title = "Star rating",
          ChoiceType = TableHeaderChoiceType.Any
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "★★★★★",
          Value = "5"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "★★★★",
          Value = "4"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "★★★",
          Value = "3"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "★★",
          Value = "2"
        }
      };

      return SelectItems(options, selectedValues);

    }

    public static List<TableRow> Amenities(List<string> selectedValues)
    {
      var group_id = Headers.Amenities.ToString();

      var options = new List<TableRow> {
        new TableRowHeader {
          Value = group_id,
          Title = "Amenities",
          ChoiceType = TableHeaderChoiceType.All
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Wifi",
          Value = "wifi"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Central location",
          Value = "central-location"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Family friendly",
          Value = "family-friendly"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Parking",
          Value = "parking"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Gym",
          Value = "gym"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Non-smoking rooms",
          Value = "non-smoking-rooms"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Pet friendly",
          Value = "pet-friendly"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Pool",
          Value = "pool"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Spa",
          Value = "spa"
        }
      };

      return SelectItems(options, selectedValues);
    }

    public static List<TableRow> SelectItems(List<TableRow> items, List<string> selectedValues)
    {
      if(selectedValues == null)
        return items;

      foreach(var option in items.Select(e=> e as TableRowItem).Where(e=>e!=null))
        option.Selected = selectedValues.Any(e => e.Equals(option.Value, StringComparison.OrdinalIgnoreCase));

      return items;
    }

  }
}

