﻿using System;
using System.Collections.Generic;
using LeadedSky.Shared;
using System.Linq;

namespace Hot5.Core
{
  public class SortOptions
  {
    public static List<TableRow> CreateOptionsList(string selected)
    {
      var group_id = "sort_option";

      var options = new List<TableRow> {
        new TableRowHeader {
          Value = group_id,
          Selectable = false,
          Expanded = true,
          Title = "Sort hotels"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Recommended",
          SelectType = TableRowItemSelectType.Radio,
          Value = "recommended"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Price - low to high",
          SelectType = TableRowItemSelectType.Radio,
          Value = "price"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Price - high to low",
          SelectType = TableRowItemSelectType.Radio,
          Value = "price_reverse"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Star rating",
          SelectType = TableRowItemSelectType.Radio,
          Value = "rating"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "User rating",
          SelectType = TableRowItemSelectType.Radio,
          Value = "user"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Distance",
          SelectType = TableRowItemSelectType.Radio,
          Value = "distance"
        },
        new TableRowItem {
          HeaderValue = group_id,
          Title = "Saving",
          SelectType = TableRowItemSelectType.Radio,
          Value = "saving"
        }
      };
      if(string.IsNullOrEmpty(selected))
        return options;

      var selectedOption = options.SingleOrDefault(e => e.Value.Equals(selected, StringComparison.CurrentCultureIgnoreCase)) as TableRowItem;
      if(selectedOption != null)
        selectedOption.Selected = true;
      return options;
    }

  }
}

