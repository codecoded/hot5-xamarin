﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class Currencies
  {
    public List<Currency> CurrenciesList { get; set; }

    public List<Currency> Common = new List<Currency> {
      new Currency {
        Country = "Australia",
        Code = "AUD",
        Symbol = "AU$"
      },
      new Currency {
        Country = "Japan",
        Code = "YEN",
        Symbol = "¥"
      },
      new Currency {
        Country = "Great Britain",
        Code = "GBP",
        Symbol = "£"
      },
      new Currency {
        Country = "United States",
        Code = "USD",
        Symbol = "$"
      },
      new Currency {
        Country = "Dubai",
        Code = "AED",
        Symbol = "AED"
      }
    };

  }

  public class Currency
  {
    public string Country { get; set; }

    public string Code { get; set; }

    public string Symbol { get; set; }

    public override string ToString()
    {
      return string.Format("[Currency: Country={0}, Code={1}, Symbol={2}]", Country, Code, Symbol);
    }
  }
}

