﻿using System;
using System.Runtime.Serialization;
using Pusher;

namespace Hot5.Core
{
  [DataContract]
  public class AvailabilityUpdateContract
  {
    [DataMember(Name = "key")]
    public string Key { get; set; }
  }

}

