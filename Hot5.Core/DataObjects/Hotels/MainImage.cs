﻿using System;

namespace Hot5.Core
{
  public class MainImage
  {
    public string ImageUrl {get;set;}
    public string ThumbnailUrl {get;set;}
  }
}

