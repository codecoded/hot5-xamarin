﻿using System;

namespace Hot5.Core
{
  public class SearchCriteria
  {
    public DateTime StartDate {get;set;}
    public DateTime EndDate {get;set;}
    public int TotalNights {get;set;}
    public string CurrencyCode { get; set;}
    public string CurrencySymbol {get;set;}
    public string CountryCode {get;set;}

  }
}

