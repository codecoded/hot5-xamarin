﻿using System;

namespace Hot5.Core
{
  public class HotelRoom
  {
    public string Provider { get; set; }

    public int? ProviderId { get; set; }

    public string Description { get; set; }

    public float Price { get; set; }

    public string Link { get; set; }

    public bool? Breakfast { get; set; }

    public bool? Wifi { get; set; }

    public bool? Cancellation { get; set; }

    public bool? PayLater { get; set; }

    public string Offer { get; set; }

    public string OfferPrice
    { 
      get
      { 
        var nfi = SessionStore.CurrencyFormat;

        return string.Format(nfi, "{0:C0}", Price);
      } 
    }

    public bool MainAmenities { get { return Wifi.GetValueOrDefault() || Breakfast.GetValueOrDefault(); } }
  }
}

