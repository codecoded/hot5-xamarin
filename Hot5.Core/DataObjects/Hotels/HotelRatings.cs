﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class HotelRatings
  {
    public float? Overall { get; set; }

    public float? Agoda { get; set; }

    public float? Booking { get; set; }

    public float? Splendia { get; set; }

    public float? Laterooms { get; set; }

    public float? EasyToBook { get; set; }

    public float? Venere { get; set; }

    public List<HotelRating> ToList()
    {
      var ratings = new List<HotelRating>();
      AddRating(ratings, Overall, "overall");
      AddRating(ratings, Agoda, "agoda");
      AddRating(ratings, Booking, "booking");
      AddRating(ratings, Venere, "venere");

      AddRating(ratings, Splendia, "splendia");
      AddRating(ratings, Laterooms, "laterooms");
      AddRating(ratings, EasyToBook, "easy_to_book");
      return ratings;
    }

    public void AddRating(List<HotelRating> ratings, float? rating, string provider)
    {
      if(IsRating(rating))
        ratings.Add(new HotelRating(provider, (float)Math.Ceiling(rating.Value)));

    }

    public bool IsRating(float? rating)
    {
      return rating.GetValueOrDefault() > 0;
    }
  }

  public class HotelRating
  {
    public HotelRating(string provider, float rating)
    {
      Provider = provider;
      Rating = rating;
    }

    public string Provider { get; set; }

    public float Rating { get; set; }

    public bool IsOverall { get { return Provider.Equals("overall", StringComparison.OrdinalIgnoreCase); } }
  }
}

