﻿using System;

namespace Hot5.Core
{
  public enum Providers
  {
    Booking,
    EasyToBook,
    Agoda,
    Expedia,
    LateRooms,
    Splendia
  }

  public enum SearchState
  {
    new_search,
    Searching,
    Finished,
    Errored
  }
}

