﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Hot5.Core
{

  public class HotelsResponse
  {
    public SearchResultsInfo Info { get; set; }

    public SearchCriteria Criteria { get; set; }

    public SearchState State { get; set; }

    public List<Hotel> Hotels { get; set; }


    public bool NoHotels { get { return (Hotels == null || Hotels.Count == 0) && State == SearchState.Finished; } }

    public void SetHotelIndices()
    {
      if(Hotels == null)
        return;
      for(int i = 0; i < Hotels.Count; i++)
        Hotels[i].Index = i + 1;
    }

    public void SetCurrency()
    {
      if(SessionStore.CurrencyFormat != null && SessionStore.CurrencyFormat.CurrencySymbol == Criteria.CurrencySymbol)
        return;
      var nfi = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
      nfi.CurrencySymbol = Criteria.CurrencySymbol;
      SessionStore.CurrencyFormat = nfi;
    }


  }
}

