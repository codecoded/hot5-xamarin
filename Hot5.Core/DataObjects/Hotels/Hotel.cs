﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Hot5.Core
{
  public class Hotel
  {
    public delegate void HotelChangedDelegate(Hotel hotel);


    public int Id { get; set; }

    public string Name { get; set; }

    public string Address { get; set; }

    public string City { get; set; }

    public string StateProvince { get; set; }

    public string PostalCode { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public float? StarRating { get; set; }

    public string Description { get; set; }

    public List<HotelProvider> Providers { get; set; }


    public HotelAmenity.HotelAmenitiesFlags? Amenities { get; set; }

    public string Slug { get; set; }

    public HeroOffer Offer { get; set; }

    public HotelRatings Ratings { get; set; }

    public MainImage MainImage { get; set; }


    public List<HotelRoom> Rooms { get; set; }

    public List<HotelImage> Images { get; set; }

    public string Channel { get; set; }

    public string Key { get; set; }


    public bool ShowRating { get { return Ratings.Overall > 0; } }

    public bool ShowSaving { get { return Offer.Saving >= 5; } }

    public bool NoOffer { get { return Offer == null || string.IsNullOrEmpty(Offer.Provider); } }

    public string OfferPrice
    { 
      get
      { 
        var nfi = SessionStore.CurrencyFormat;

        return string.Format(nfi, "{0:C0}", Offer.MinPrice);
      } 
    }

    public string DisplayStarRating { get { return new String('★', (int)Math.Ceiling((decimal)StarRating.GetValueOrDefault())); } }

    public int Index { get; set; }

    public float? Distance { get; set; }

    public int ImageCountLimited(int limit = 10)
    {
      if(Images == null || Images.Count == 0)
        return 1;
      return Images.Count > 10 ?limit : Images.Count;
    }

    public double DistanceInMiles()
    {
      if(Distance == null)
        return 0;
      return Distance.Value * 0.00062137;
    }

    public List<HotelAmenity> AmenitiesList()
    {
      return HotelAmenity.ConvertToList(Amenities.GetValueOrDefault());
    }
    //
    //    $scope.trackClick = function(clickDetails){
    //      var params = $scope.buildParams();
    //
    //      var url = '/offer/' + clickDetails.provider + '?';
    //      params.price = clickDetails.price;
    //      params.hotel_id = clickDetails.hotel_id;
    //      params.target_url = clickDetails.url;
    //      params.currency = Page.criteria.currency_code;
    //      Hotels.removeEmptyKeys(params)
    //      var result = decodeURIComponent($.param(params));
    //      window.open(url + result);
    //    }

    //    public string OfferUrl()
    //    {
    //
    //    }
  }
}

