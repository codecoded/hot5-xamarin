﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{

  public class HotelResponse
  {
    public SearchCriteria Criteria { get; set; }

    public Hotel Hotel { get; set; }


  }
}

