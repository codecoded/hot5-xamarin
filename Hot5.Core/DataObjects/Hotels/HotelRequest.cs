﻿using System;
using LeadedSky.Shared;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class HotelRequest
  {
    public string Slug;
    public string Currency;
    public DateTime StartDate;
    public DateTime EndDate;
    public string Key;


    public DateRange CalendarDates()
    {
      return new DateRange(StartDate, EndDate);
    }

 
  }
}

