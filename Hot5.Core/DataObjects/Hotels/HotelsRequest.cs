﻿using System;
using LeadedSky.Shared;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class HotelsRequest
  {
    public string Slug;
    public double? Latitude;
    public double? Longitude;
    public int? Count;
    public string Currency;
    public DateTime StartDate;
    public DateTime EndDate;
    public string Key;
    public float? MaxPrice;
    public float? MinPrice;
    public string Sort;
    public List<string> Amenities;
    public List<string> StarRatings;
    public bool InitSearch;

    public DateRange CalendarDates()
    {
      return new DateRange(StartDate, EndDate);
    }

    public string Coordinates()
    {
      if(Latitude == null || Longitude == null)
        return null;
      return string.Join(",", Latitude, Longitude);
    }

    public void UpdateRequestDates(DateRange dateRange)
    {
      StartDate = dateRange.StartDate;
      EndDate = dateRange.EndDate;
    }

    public void UpdateSelectedLocation(SearchResultView selectedLocation)
    {
      if(selectedLocation.MyLocation)
        return;
      Slug = selectedLocation.Slug;
      Longitude = null;
      Latitude = null;
    }

    public void UpdateCurrency(Currency currency)
    {
      MinPrice = null;
      MaxPrice = null;
      Currency = currency.Code;
    }

    public void SetMyLocation(double latitude, double longitude)
    {
      Latitude = latitude;
      Longitude = longitude;
      Slug = "my-location";
    }

    public HotelRequest ToHotelRequest(Hotel hotel)
    {
      return  new HotelRequest {
        StartDate = StartDate,
        EndDate = EndDate,
        Key = Key,
        Currency = Currency,
        Slug = hotel.Slug
      };
    }

    public string Channel()
    {
      if(Currency == null)
        Currency = "GBP";
      var channel = string.Format("hot5-com-{0:yyyy-M-dd}-{1:yyyy-M-dd}-{2}-{3}", StartDate, EndDate, Currency.ToLower(), Slug);
      if(Slug == "my-location")
        channel = string.Format("{0}-{1}-{2}", channel, Math.Abs(Longitude.Value), Math.Abs(Latitude.Value)).Replace(".", "-");
      return channel;

    }
  }
}

