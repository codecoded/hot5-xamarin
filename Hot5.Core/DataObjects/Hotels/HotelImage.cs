﻿using System;

namespace Hot5.Core
{
  public class HotelImage
  {
    public string Url {get;set;}
    public string ThumbnailUrl {get;set;}
    public int? Width {get;set;}
    public int? Height {get;set;}
  }
}

