﻿using System;

namespace Hot5.Core
{
  public class HeroOffer
  {
    public string Provider { get; set; }

    public int? ProviderId { get; set; }

    public string Link { get; set; }

    public float MinPrice { get; set; }

    public float MaxPrice { get; set; }

    public float Saving { get; set; }


  }
}

