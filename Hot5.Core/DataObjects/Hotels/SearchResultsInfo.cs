﻿using System;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class SearchResultsInfo
  {
    public string Query { get; set; }

    public string Slug { get; set; }

    public string Channel { get; set; }

    public string Key { get; set; }

    public string Sort { get; set; }

    public int? TotalHotels { get; set; }

    public int? AvailableHotels { get; set; }

    public float? MinPrice { get; set; }

    public float? MaxPrice { get; set; }

    public float? MinPriceFilter { get; set; }

    public float? MaxPriceFilter { get; set; }

    public List<float> PriceValues { get; set; }

    public List<float> StarRatings { get; set; }

    public List<string> Amenities { get; set; }

    public double Longitude { get; set; }

    public double Latitude { get; set; }

    public int? Zoom { get; set; }

    public int? PageSize { get; set; }

    public double? Timestamp { get; set; }


    public bool MyLocation
    {
      get
      {
        return "my-location".Equals(Slug, StringComparison.CurrentCultureIgnoreCase); 

      }
    }

  }
}

