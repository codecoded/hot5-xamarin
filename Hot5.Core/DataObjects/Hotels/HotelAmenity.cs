﻿using System;
using System.Collections.Generic;

//using System.Globalization;
using System.Linq;

namespace Hot5.Core
{
  public class HotelAmenity
  {
    [Flags]
    public enum HotelAmenitiesFlags
    {
      WiFi = 1,
      Central_Location = 2,
      Family_Friendly = 4,
      Parking = 8,
      Gym = 16,
      Boutique = 32,
      Non_Smoking_Rooms = 64,
      Pet_Friendly = 128,
      Pool = 256,
      Restaurant = 512,
      Spa = 1024
    }


    public HotelAmenitiesFlags Flag { get; private set; }


    public string Description
    {
      get
      {
        return Flag.ToString().Replace('_', ' ');
      }
    }

    public HotelAmenity(HotelAmenitiesFlags flag)
    {
      Flag = flag;
    }

    public static List<HotelAmenity> ConvertToList(HotelAmenitiesFlags amenityFlag)
    {
      var flags = new List<HotelAmenity>();

      foreach(HotelAmenitiesFlags amenity in Enum.GetValues(typeof(HotelAmenitiesFlags)))
      {
        if((amenity & amenityFlag) == amenity)
          flags.Add(new HotelAmenity(amenity));
      }
      return flags;
    }


  }
}

