﻿using System;

namespace Hot5.Core
{
  public class ProviderDeal
  {
    public string Provider {get;set;}
    public int ProviderHotelId {get;set;}
    public int RoomCount {get;set;}
    public float MinPrice {get;set;}
    public float MaxPrice {get;set;}
    public int Ranking {get;set;}
    public string Link {get;set;}
    public bool Loaded {get;set;}

  }
}

