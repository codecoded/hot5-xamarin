﻿using System;

namespace Hot5.Core
{
  public class HotelProvider
  {
    public string Provider { get; set; }

    public int? ProviderId { get; set; }

    public string Description { get; set; }

    public float? UserRating { get; set; }

    public string EnglishProviderName()
    {
      switch (Provider.ToLower())
      {
      case "booking":
        return "Booking.com";
      case "expedia":
        return "Expedia";
      case "laterooms":
        return "Laterooms";
      case "agoda":
        return "Agoda";
      case "easy_to_book":
        return "EasyToBook.com";
      case "venere":
        return "Venere";
      default:
        return Provider;
      }
    }
  }
}

