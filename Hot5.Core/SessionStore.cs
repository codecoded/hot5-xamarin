﻿using System;
using Hot5.Core;
using System.Globalization;

namespace Hot5.Core
{
  public static class SessionStore
  {
    public static HotelRequest HotelRequest { get; set; }

    public static HotelsRequest HotelsRequest { get; set; }

    public static NumberFormatInfo CurrencyFormat { get; set; }

    public static Hotel CurrentHotel { get; set; }
  }
}

