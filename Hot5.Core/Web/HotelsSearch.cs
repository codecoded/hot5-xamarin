﻿using System;
using RestSharp;
using System.Diagnostics;

namespace Hot5.Core
{
  public class HotelsSearch
  {
    HotelsRequest Request;

    public static string StartSearchURI
    {
      get
      {
        return string.Format("http://{0}", ApiClient.DOMAIN);
      }
    }

    public static string HotelsResultsURI
    {
      get
      {
        return string.Format("http://hotels.{0}", ApiClient.DOMAIN);
      }
    }

    public HotelsSearch(HotelsRequest request)
    {
      Request = request;
    }

    public RestRequestAsyncHandle StartSearch(Action<HotelsRequest, IRestResponse> callback)
    {
      return SendRequest(StartSearchURI, callback);         
    }

    public RestRequestAsyncHandle HotelsResults(Action<HotelsRequest, IRestResponse> callback)
    {
      return SendRequest(HotelsResultsURI, callback);
    }

    RestRequestAsyncHandle SendRequest(string endpoint, Action<HotelsRequest, IRestResponse> callback)
    {
      var restClient = new RestClient(endpoint);
      var request = CreateRestRequest();

      Debug.WriteLine(string.Format("HotelsSearch::SendRequest: uri={0}", restClient.BuildUri(request)));

      return restClient.ExecuteAsync(request, response => callback(Request, response)); 

    }

    RestRequest CreateRestRequest()
    {
      var apiRequest = new RestRequest(string.Format("mobile/{0}", Request.Slug), Method.GET);
      apiRequest.RequestFormat = DataFormat.Json;

      apiRequest.AddParameter("count", Request.Count);
      apiRequest.AddParameter("start_date", Request.StartDate.ToString("yyyy-MM-dd"));
      apiRequest.AddParameter("end_date", Request.EndDate.ToString("yyyy-MM-dd"));

      addParameter(apiRequest, "currency", Request.Currency);
      addParameter(apiRequest, "max_price", Request.MaxPrice);
      addParameter(apiRequest, "key", Request.Key);
      addParameter(apiRequest, "sort", Request.Sort);
      if(Request.Amenities != null)
        addParameter(apiRequest, "amenities", String.Join(",", Request.Amenities));
      if(Request.StarRatings != null)
        addParameter(apiRequest, "star_ratings", String.Join(",", Request.StarRatings));

      if(Request.Slug == "my-location")
        addParameter(apiRequest, "coordinates", Request.Coordinates());
      return apiRequest;
    }

    static void addParameter(RestRequest request, string key, object value)
    {
      ApiClient.AddParameter(request, key, value);
    }
  }
}

