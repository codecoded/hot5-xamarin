﻿using System;
using RestSharp;
using System.Diagnostics;
using System.Linq;

namespace Hot5.Core
{
  public class HotelSearch
  {
    HotelRequest Request;

    public static string HotelDetailsURI
    {
      get
      {
        return string.Format("http://hotels.{0}", ApiClient.DOMAIN);
      }
    }


    public HotelSearch(HotelRequest request)
    {
      Request = request;
    }

    public RestRequestAsyncHandle Search(Action<string> callback)
    {
      return SendRequest(string.Format("mobile/hotels/{0}", Request.Slug), callback);
    }

    public RestRequestAsyncHandle Rooms(Action<string> callback)
    {
      return SendRequest(string.Format("hotels/{0}/rooms", Request.Slug), callback);

    }

    RestRequestAsyncHandle SendRequest(string endpoint, Action<string> callback)
    {
      var restClient = new RestClient(HotelDetailsURI);
      var request = CreateRestRequest(endpoint);

      var sParams = "";

     
      Debug.WriteLine(string.Format("HotelSearch::SendRequest: uri={0}", restClient.BuildUri(request)));
      return restClient.ExecuteAsync(request, response => {
        if(response.StatusCode == System.Net.HttpStatusCode.OK)
          callback(response.Content);
        else
          callback(null);
      }); 

    }

    RestRequest CreateRestRequest(string endpoint)
    {
      var apiRequest = new RestRequest(endpoint, Method.GET);
      apiRequest.RequestFormat = DataFormat.Json;
      AddParameter(apiRequest, "currency", Request.Currency);
      AddParameter(apiRequest, "start_date", Request.StartDate.ToString("yyyy-MM-dd"));
      AddParameter(apiRequest, "end_date", Request.EndDate.ToString("yyyy-MM-dd"));
      AddParameter(apiRequest, "key", Request.Key);
      return apiRequest;
    }

    public void AddParameter(RestRequest request, string key, object value)
    {
      ApiClient.AddParameter(request, key, value);
    }

  }
}

