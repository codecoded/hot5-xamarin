﻿using System;
using RestSharp;
using System.Linq;
using LeadedSky.Shared;
using System.Diagnostics;
using System.Collections.Generic;

namespace Hot5.Core
{
  public class ApiClient
  {
    public static int SearchLimit = 7;
    //public static string[] SearchTypes = new string[]{ "city", "hotel", "place", "landmark", "country", "region" };
    public static string[] SearchTypes = new string[]{ "city", "place", "hotel" };

    public static RestClient AutocompleteHttpClient { get { return new RestClient("http://www.hot5.com/sm/search"); } }

    public static string DOMAIN = "hot5.com";
    //public static string DOMAIN = "staging.hot5.com";
    //public static string DOMAIN = "hotels.dev";



    public static RestRequestAsyncHandle AutocompleteSearch(string term, Action<SearchResponse> callback)
    {
      var request = new RestRequest(Method.GET);
      request.RequestFormat = DataFormat.Json;

      request.AddParameter("term", term);
      request.AddParameter("limit", SearchLimit);
      foreach(var type in SearchTypes)
        request.AddParameter("types[]", type);
        
      return AutocompleteHttpClient.ExecuteAsync(request, response => {
        if(response.StatusCode == System.Net.HttpStatusCode.OK)
          callback(DeserialiseResponse<SearchResponse>(response.Content));
      });

    }

    public static RestRequestAsyncHandle StartHotelsSearch(HotelsRequest hotelsRequest, Action<HotelsRequest, IRestResponse> callback)
    {
      return new HotelsSearch(hotelsRequest).StartSearch(callback);
    }

    public static RestRequestAsyncHandle HotelsResults(HotelsRequest hotelsRequest, Action<HotelsRequest, IRestResponse> callback)
    {
      return new HotelsSearch(hotelsRequest).HotelsResults(callback);
    }

    public static RestRequestAsyncHandle HotelSearch(HotelRequest hotelRequest, Action<string> callback)
    {
      return new HotelSearch(hotelRequest).Search(callback);
    }

    public static RestRequestAsyncHandle HotelRooms(HotelRequest hotelRequest, Action<string> callback)
    {
      return new HotelSearch(hotelRequest).Rooms(callback);
    }


    public static RestRequestAsyncHandle Currencies(Action<List<Currency>> callback)
    {
      var request = new RestRequest("currencies", Method.GET);
      var restClient = new RestClient("http://" + ApiClient.DOMAIN);

      request.RequestFormat = DataFormat.Json;
      return restClient.ExecuteAsync(request, response => {
        if(response.StatusCode == System.Net.HttpStatusCode.OK)
          callback(DeserialiseResponse<List<Currency>>(response.Content));
      });
    }

    public static RestRequestAsyncHandle UserInfo(Action<UserInfo> callback)
    {
      var request = new RestRequest("userinfo", Method.GET);
      var restClient = new RestClient("http://" + ApiClient.DOMAIN);

      request.RequestFormat = DataFormat.Json;
      return restClient.ExecuteAsync(request, response => {
        if(response.StatusCode == System.Net.HttpStatusCode.OK)
          callback(DeserialiseResponse<UserInfo >(response.Content));
      });
    }

    public static T DeserialiseResponse<T>(string data)
    {
      return JsonMarshaller.Deserialise<T>(data, JsonMarshallerSettings.Default);
    }

    public static void AddParameter(RestRequest request, string key, object value)
    {
      if(value == null || string.IsNullOrEmpty(value.ToString()))
        return;
      request.AddParameter(key, value);
    }
  }
}

