﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hot5.Core
{
  public class SearchResultView
  {
    public string SearchType { get; set; }

    public string Title { get; set; }

    public string Detail { get; set; }

    public string Slug { get; set; }

    public bool NoDetail { get { return string.IsNullOrEmpty(Detail); } }

    public string TitlelessDetail { get { return NoDetail || Detail.Equals(Title) ?string.Empty : Detail.Remove(0, Title.Length); } }

    public bool MyLocation
    {
      get
      {
        return Slug == "my-location"; 

      }
    }

    public static SearchResultView FromSearchResult(string searchType, SearchResult result)
    {
      return new SearchResultView {
        SearchType = searchType,
        Title = result.Term,
        Detail = result.Data.Title,
        Slug = result.Data.Slug
      };
    }

    public static List<SearchResultView> ConvertSearchResult(string searchType, IList<SearchResult> results)
    {
      return results.Select(e => SearchResultView.FromSearchResult(searchType, e)).ToList();
    }

    public static Dictionary<string, List<SearchResultView>> FromSearchResults(string searchType, IList<SearchResult> results)
    {
      return CreateDictionary(ConvertSearchResult(searchType, results));
    }


    public static Dictionary<string, List<SearchResultView>> CreateDictionary(IList<SearchResultView> searchResultsView)
    {
      return searchResultsView.GroupBy(e => e.SearchType).ToDictionary(e => e.Key, f => f.ToList());
    }

    public static SearchResultView MyLocationStub()
    {
      return new SearchResultView {
        Detail = "Current Location",
        Title = "Current Location", 
        SearchType = "place",
        Slug = "my-location"
      };
    }
  }
}

