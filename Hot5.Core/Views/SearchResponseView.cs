﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace Hot5.Core
{
  public class SearchResponseView
  {
    public Dictionary<string, List<SearchResultView>> Sections { get; private set; }

    public bool Empty { get { return Sections == null || Sections.Count == 0 || Sections.All(e => e.Value.Count == 0); } }

    public string[] SearchTypes { get { return Sections.Keys.ToArray(); } }

    public string[] SimpleSections = new string[]{ "System", "Recent" };

    public SearchResponseView()
    {
      Sections = new Dictionary<string, List<SearchResultView>>();			
    }

    public bool IsSimpleView(int index)
    {
      string header = GetSearchTermAt(index);
      return SimpleSections.Any(e => e.Equals(header, StringComparison.CurrentCultureIgnoreCase));
    }

    public SearchResponseView(SearchResponse searchResponse, List<SearchResultView> recentResponses) : this()
    {
      AddRecentSection(recentResponses);
      AddSectionFromSearchResults("city", searchResponse.Results.City);
      AddSectionFromSearchResults("place", searchResponse.Results.Place);
      AddSectionFromSearchResults("hotel", searchResponse.Results.Hotel);
    }

    public static SearchResponseView Default(List<SearchResultView> defaultResults)
    {
      if(defaultResults == null)
        return null;

      var view = new SearchResponseView();
      view.Sections = SearchResultView.CreateDictionary(defaultResults);
      return view;
    }

    public void AddSectionFromSearchResults(string section, IList<SearchResult> searchResults)
    {
      Sections.Add(section, SearchResultView.ConvertSearchResult(section, searchResults));
    }

    public string GetSearchTermAt(int index)
    {
      if(index < 0 || index > SearchTypes.Length)
        index = 0;
      return SearchTypes[index];
    }

    public IList<SearchResultView> GetSectionAt(int index)
    {
      var searchTerm = GetSearchTermAt(index);
      if(Sections.ContainsKey(searchTerm))
        return Sections[searchTerm];
      return null;
    }

    public IList<SearchResultView> GetSectionFor(string searchTerm)
    {
      if(!Sections.ContainsKey(searchTerm))
        return null;
      return Sections[searchTerm];
    }

    public void AddRecentSection(List<SearchResultView> recentResponses)
    {
      if(recentResponses == null)
        return;

      Sections.Add("recent", recentResponses);
    }

  }
}

