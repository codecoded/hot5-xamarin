﻿using System;
using System.IO;
using System.Collections.Generic;
using Hot5.Core;
using Newtonsoft.Json;
using LeadedSky.Shared;
using System.Linq;
using PubSub;

namespace Hot5
{
  public class UserCache
  {
    public const int RECENT_COUNT = 5;

    public List<SearchResultView> RecentSearches { get; set; }

    Currency currency;

    public Currency Currency
    {
      get
      {
        return currency;
      }
      set
      {
        currency = value;
        this.Publish<Currency>(currency);
      }
    }


    public static string FileId = "Hot5Cache.xml";

    public static string Folder { get { return Environment.GetFolderPath(Environment.SpecialFolder.Personal); } }

    public static string Filename { get { return Path.Combine(Folder, FileId); } }

    public UserCache()
    {
      RecentSearches = new List<SearchResultView>(RECENT_COUNT);
    }

    public static UserCache Load()
    {

      string json = null;
      if(File.Exists(Filename))
        json = File.ReadAllText(Filename);
      return JsonMarshaller.Deserialise<UserCache>(json, JsonMarshallerSettings.Default) ?? new UserCache();
    }

    public void AddSearchAndSave(SearchResultView searchResult)
    {
      if(searchResult == null || searchResult.SearchType == "System")
        return;
      if(RecentSearches.Any(e => e.SearchType == searchResult.SearchType && e.Slug == searchResult.Slug))
        return;
      if(RecentSearches.Count >= RECENT_COUNT)
      {
        var copy = RecentSearches.Take(RECENT_COUNT - 1).ToList();
        RecentSearches = copy;
      }

      RecentSearches.Insert(0, searchResult);
      Save();
    }

    public void Save()
    {
      File.WriteAllText(Filename, ToJson());
    }

    public static void Delete()
    {
      File.Delete(Filename);
    }

    public string ToJson()
    {
//      return JsonConvert.SerializeObject(this);
      return JsonMarshaller.Serialise(this, JsonMarshallerSettings.Default);
    }

    public string CurrencyCode
    {
      get{ return currency == null ?null : currency.Code.ToUpper(); }
    }
  }
}

