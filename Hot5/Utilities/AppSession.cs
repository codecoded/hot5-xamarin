﻿using System;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;
using Pusher;
using Hot5.Core;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.CoreLocation;
using PubSub;
using System.Threading.Tasks;
using Xamarin;

namespace Hot5
{
  public static class AppSession
  {
    public static UserCache UserCache;

    public static UserInfo UserInfo { get; set; }

    public static Currencies Currencies { get; set; }

    public static LocationManager LocManager { get; set; }

    //    public static HotelsRequest HotelsRequest { get; private set; }
    //
    //    public static HotelsResponse HotelsResponse { get; set; }

    //    public static bool ReloadRequest { get; set; }
    //
    //    public static bool ReloadResponse { get; set; }
    //
    //    public static bool CancelRequest { get; set; }

    public static CLLocation CurrentLocation { get; set; }

    public static Pusher.Pusher PusherClient { get; set; }

    public static bool HasLocationServices{ get { return CLLocationManager.LocationServicesEnabled; } }

    public static bool HasConnectivity { get { return  Reachability.InternetConnectionStatus() != NetworkStatus.NotReachable; } }

    public static RootViewController RootController
    { 
      get
      {
        return (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController;
      } 
    }

    public static void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
    {
      CurrentLocation = e.Location;
    }

    //    public static void UpdateRequestCoordinates(LocationUpdatedEventArgs e)
    //    {
    //      HotelsRequest.Latitude = e.Location.Coordinate.Latitude;
    //      HotelsRequest.Longitude = e.Location.Coordinate.Longitude;
    //
    //      "HotelsRequest Coordinates Updated: latitude={0}, longitude={1}".Log(HotelsRequest.Latitude, HotelsRequest.Longitude);
    //    }


    public static void Init()
    {
      try
      {
        UserCache = UserCache.Load();

        InitLocationManager();
        InitInsights();
        AppRequestResponse.InitHotelsRequest();
        InitCurrencies();
      }
      catch(Exception ex)
      {
        ShowAlert("Unable to Start", string.Format("It seems as though there is a very unexpected error in starting the app. Please contact support@hot.com with this message: {0}", ex.Message));
      }
     
    }


    public static void End()
    {
      if(PusherClient != null)
        PusherClient.Disconnect();
      LocManager.LocManager.StopUpdatingLocation();
      PusherClient = null;
      LocManager = null;
    }

    static void InitInsights()
    {
      Insights.Initialize("c5d67c666fde69bcd6b1d05b3454ebc992756cca");
    }

    static void InitLocationManager()
    {
      LocManager = new LocationManager();
      LocManager.LocationUpdated += HandleLocationChanged;
      LocManager.StartLocationUpdates();
    }

    static void InitCurrencies()
    {
      ApiClient.Currencies(currencies => {
        Currencies = new Currencies{ CurrenciesList = currencies };
        InitUserInfo();
      });
    }

    static void InitUserInfo()
    {
      ApiClient.UserInfo(userinfo => {
        UserInfo = userinfo;
        if(UserCache.Currency == null && UserInfo != null)
        {
          var currency = Currencies.CurrenciesList.Find(e => e.Code.ToLower() == UserInfo.Currency.ToLower());
          UserCache.Currency = currency;
          UserCache.Save();
        }
      });


    }

    //    static void InitHotelsRequest()
    //    {
    //      AppRequestResponse = new HotelsRequest {
    //        Count = 10,
    //        Currency = UserCache.CurrencyCode
    //      };
    //
    //      HotelsRequest.UpdateRequestDates(DateRange.DaysFromNow(1));
    //      if(CurrentLocation != null)
    //        HotelsRequest.SetMyLocation(CurrentLocation.Coordinate.Longitude, CurrentLocation.Coordinate.Longitude);
    //    }
    //
    public static void ShowAlert(string title, string message)
    {
      using (var pool = new NSAutoreleasePool())
      {
        pool.BeginInvokeOnMainThread(delegate {
          new UIAlertView(title, message, null, "Ok").Show();
        });
      }
    }

    public static void RunOnMainThread(NSAction action)
    {
      using (var pool = new NSAutoreleasePool())
        pool.BeginInvokeOnMainThread(action);

    }
      
  }


}

