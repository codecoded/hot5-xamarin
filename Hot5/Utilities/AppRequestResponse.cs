﻿using System;
using Hot5.Core;
using LeadedSky.UI.iOS;
using System.Linq;
using LeadedSky.Shared;

namespace Hot5
{
  public static class AppRequestResponse
  {

    public static HotelsRequest Request;
    public static HotelsResponse Response;
    public static bool ReloadRequest;
    public static bool ReloadResponse;
    public static bool CancelRequest;
    public static double CurrentTimestamp;
    public static  bool Errored;

    public static bool Finished
    {
      get
      { 
        return (Response != null && Response.State == SearchState.Finished) || CancelRequest;
      } 
    }


    public static bool SameRequest
    { 
      get
      {
        if(Response == null || Request == null)
          return false;
        return Response.Info.Slug == Request.Slug
        && Response.Criteria.StartDate == Request.StartDate
        && Response.Criteria.EndDate == Request.EndDate
        && Response.Criteria.CurrencyCode == Request.Currency; 
      } 
    }

    public static bool NoHotels
    {
      get{ return Response == null || Response.Hotels == null || Response.Hotels.Count == 0; }
    }

    public static void InitHotelsRequest()
    {
      Request = new HotelsRequest {
        Count = 10,
        Currency = AppSession.UserCache.CurrencyCode
      };

      Request.UpdateRequestDates(DateRange.DaysFromNow(1));    
      if(AppSession.CurrentLocation != null)
        Request.SetMyLocation(AppSession.CurrentLocation.Coordinate.Longitude, AppSession.CurrentLocation.Coordinate.Longitude);
    }


    public static void LogResponse()
    {
      if(Response == null)
        "AppRequestResponse Response Null".Log();
      else
        "AppRequestResponse::UpdateHotels: responseState={0} timestamp={1:G} hotelCount={2}".Log(Response.State, Response.Info.Timestamp.Value, (Response.Hotels == null ?0 : Response.Hotels.Count()));
    }


    public static void RemoveBlankHotels()
    {
      if(!NoHotels)
        Response.Hotels.RemoveAll(e => e.NoOffer);
    }

    public static bool IgnoreResponse(SearchResultsInfo newInfo)
    {
      if(IsEarlierResponse(newInfo) || !SameRequest)
      {
        "Results don't match the Request. Skipping".Log();
        return true;
      }
      return false;
    }


    public static bool IsEarlierResponse(SearchResultsInfo newInfo)
    {
      if(Response == null || Response.Info == null || Response.Info.Timestamp == null || newInfo == null || newInfo.Timestamp == null)
        return false;
      if(newInfo.Timestamp >= CurrentTimestamp)
      {
        CurrentTimestamp = newInfo.Timestamp.Value;
        return false;
      }
      "Results are from earlier request".Log();
      return true;
    }

    public static void ResetRequest(bool includeFilters = true)
    {
      ReloadRequest = false;
      ReloadResponse = false;

      if(includeFilters)
      {
        Request.Sort = null;
        Request.Amenities = null;
        Request.MaxPrice = null;
        Request.MinPrice = null;
      }

      Request.Key = null;
      Response = null;
      CurrentTimestamp = 0;
      Errored = false;
      CancelRequest = false;
      Request.Count = 15;
    }

    public static void UpdateRequestCoordinates(LocationUpdatedEventArgs e)
    {
      Request.Latitude = e.Location.Coordinate.Latitude;
      Request.Longitude = e.Location.Coordinate.Longitude;

      "HotelsRequest Coordinates Updated: latitude={0}, longitude={1}".Log(Request.Latitude, Request.Longitude);
    }
  }
}

