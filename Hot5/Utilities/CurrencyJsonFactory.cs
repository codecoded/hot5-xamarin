﻿
using System;
using Hot5.Core;
using LeadedSky.Shared;
using System.IO;
using System.Collections.Generic;

namespace Hot5
{
  public static class CurrencyJsonFactory
  {
    public static List<Currency> LoadCurrencies()
    {
      var r = JsonMarshaller.Deserialise<List<Currency>>(File.ReadAllText("currencies.json"), JsonMarshallerSettings.Default);
      return r;
    }
  }
}




