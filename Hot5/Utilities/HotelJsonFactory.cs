﻿using System;
using Hot5.Core;
using LeadedSky.Shared;
using System.IO;

namespace Hot5
{
  public class HotelJsonFactory
  {
    public static HotelsResponse LoadHotels()
    {
      var r = JsonMarshaller.Deserialise<HotelsResponse>(File.ReadAllText("hotels.json"), JsonMarshallerSettings.Default);
      return r;
    }

    public static HotelResponse LoadHotel()
    {
      var r = JsonMarshaller.Deserialise<HotelResponse>(File.ReadAllText("hotel.json"), JsonMarshallerSettings.Default);
      return r;
    }
  }
}

