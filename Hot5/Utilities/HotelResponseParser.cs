﻿using System;
using Hot5.Core;
using LeadedSky.Shared;
using System.IO;

namespace Hot5
{
  public class HotelResponseParser
  {
    public static T Parse<T>()
    {
      var r = JsonMarshaller.Deserialise<T>(File.ReadAllText("hotels.json"), JsonMarshallerSettings.Default);
      return r;
    }

    public static T Parse<T>(string content)
    {
      var r = JsonMarshaller.Deserialise<T>(content, JsonMarshallerSettings.Default);
      return r;
    }
  }
}

