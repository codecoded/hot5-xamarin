﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class UIParallaxView : UIView
  {
    public bool Animating { get; set; }

    public PointF CurrentOffset { get; set; }


    float scrollOffset = 0;
    const float distanceToSnap = 15f;

    public float OriginalCenterY { get; set; }

    public UIParallaxView()
    {
    }

    public UIParallaxView(RectangleF frame) : base(frame)
    {
    }

    public UIParallaxView(IntPtr handle) : base(handle)
    {
    }


    public void ControllerScrolled(object sender, EventArgs e)
    {
      var scrollView = sender as UIScrollView;
      if(!scrollView.Tracking && scrollOffset == 0)
        return;
      ControllerScrolled(scrollView.ContentOffset);
    }

    public void ControllerScrolled(PointF contentOffset)
    {

      if(Animating)
        return;

      bool scrollingDown = CurrentOffset.Y > contentOffset.Y;

      CurrentOffset = contentOffset;

      if(scrollingDown)
      {
        if(!shouldScrollUp())
          return;
      }
      else if(!shouldScrollDown())
        return;
      translateView();
    }

    void translateView()
    {
      var f = Center;
      f.Y = OriginalCenterY + scrollOffset;
      Center = f;
    }

    bool shouldScrollUp()
    {
      if(Center.Y <= OriginalCenterY)
        return false;

      scrollOffset -= 2;

      if(scrollOffset <= 0 || CurrentOffset.Y <= 1)
        scrollOffset = 0;

      if(scrollOffset < Frame.Height - distanceToSnap)
      {
        slideUpView();
        return false;
      }

      return true;
    }

    bool shouldScrollDown()
    {
      if(Center.Y >= OriginalCenterY + distanceToSnap)
        return false;

      scrollOffset += 2;

      if(scrollOffset >= Frame.Height)
        scrollOffset = Frame.Height;

      if(scrollOffset > distanceToSnap)
      {
        slideDownView();
        return false;
      }

      return true;
    }

    void slideUpView()
    {
      if(Animating)
        return;
      Animating = true;

      UIView.Animate(
        duration: 0.40,
        delay: 0,
        options: UIViewAnimationOptions.CurveEaseOut,
        animation: () => {
          Center = new PointF(Center.X, OriginalCenterY);
        },
        completion: () => {
          Animating = false;
          scrollOffset = 0;
        }
      );

    }

    void slideDownView()
    {
      if(Animating)
        return;
      Animating = true;

      UIView.Animate(
        duration: 0.40,
        delay: 0,
        options: UIViewAnimationOptions.CurveEaseOut,
        animation: () => {
          Center = new PointF(Center.X, OriginalCenterY + Frame.Height);
        },
        completion: () => {
          Animating = false;
          scrollOffset = Frame.Height;
        }
      );
    }
  }
}

