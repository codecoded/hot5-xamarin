﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace Hot5
{
  public partial class UIPaddedTextField : UITextField
  {

    public UIEdgeInsets EdgeInsets { get; set; }

    public UIPaddedTextField()
    {
      EdgeInsets = UIEdgeInsets.Zero;
    }

    public UIPaddedTextField(IntPtr intPtr) : base(intPtr)
    {
      EdgeInsets = UIEdgeInsets.Zero;
    }

    public override RectangleF TextRect(RectangleF forBounds)
    {
      return base.TextRect(InsetRect(forBounds, EdgeInsets));
    }

    public override RectangleF EditingRect(RectangleF forBounds)
    {
      return base.EditingRect(InsetRect(forBounds, EdgeInsets));
    }

    // Workaround until this method is available in Xamarin.iOS

    public static RectangleF InsetRect(RectangleF rect, UIEdgeInsets insets)
    {
      return new RectangleF(rect.X + insets.Left,
        rect.Y + insets.Top,
        rect.Width - insets.Left - insets.Right,
        rect.Height - insets.Top - insets.Bottom);
    }
  }
}

