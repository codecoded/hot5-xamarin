﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using System.ComponentModel;
using LeadedSky.Shared;

namespace Hot5
{
  partial class CalendarOptionView : UIView
  {

    public DateTime Date { get; private set; }
    public CalendarViewDataSource.DateSelectedDelegate DateSelected { get; set; }
    public UILabel Title { get; set; }
    public UILabel Detail { get; set; }

    public CalendarOptionView()
    {
      initialize();
    }

    public CalendarOptionView(IntPtr handle) : base(handle)
    {
      initialize();
    }
    //
    //    public CalendarOptionView(RectangleF frame) : base (frame)
    //    {
    //      initialize();
    //    }

    [Export("initWithCoder:")]
    public CalendarOptionView(NSCoder coder) : base(coder)
    {
      initialize();
    }
      
    void initialize()
    {
      createTitleLabel();
      createDetailLabel();
      Date = DateTimeExtensions.Today;
    }
      
    public void SetStartDate(DateTime date)
    {
      Date = date;
      Title.Text = getStartTitleText();
      Detail.Text = Date.ToString("d MMM");
    }

    public void SetEndDate(DateRange dateRange)
    {
      Date = dateRange.EndDate;
      Title.Text = getEndTitleText(dateRange.TotalDays);
      Detail.Text = Date.ToString("Leave d MMM");   
    }

    string getStartTitleText()
    {
      if(Date == DateTimeExtensions.Today)
        return "Today";
      else if(Date == DateTimeExtensions.Tomorrow)
        return "Tomorrow";
      else
        return Date.DayOfWeek.ToString();
    }

    string getEndTitleText(int totalNights)
    {
      return string.Format("{0} {1}", totalNights, totalNights == 1 ? "Night" : "Nights");
    }
      
    public override void TouchesBegan(NSSet touches, UIEvent evt)
    {
      base.TouchesBegan(touches, evt);
      this.BackgroundColor = AppConfig.PrimaryColor;
    }

    public override void TouchesEnded(NSSet touches, UIEvent evt)
    {
      base.TouchesEnded(touches, evt);
      var touch = touches.AnyObject as UITouch;
      this.BackgroundColor = UIColor.White;

      if(touch.TapCount == 1 && DateSelected != null)
        DateSelected(Date, null);
    }

    void createTitleLabel()
    {
      Title = new UILabel(new RectangleF(0, 5, 106, 21)) { 
        Font = AppConfig.Font(16f, "Inconsolata"),
        Opaque = true,
        TextAlignment = UITextAlignment.Center,
        TextColor = UIColor.DarkTextColor
      };

      AddSubview(Title);
    }

    void createDetailLabel()
    {
      Detail = new UILabel(new RectangleF(0, 26, 106, 21)) { 
        Font = AppConfig.Font(10f, "Inconsolata"),
        Opaque = true,
        TextAlignment = UITextAlignment.Center,
        TextColor = UIColor.DarkTextColor
      };

      AddSubview(Detail);
    }
  }
}
