// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("CalendarViewController")]
	partial class CalendarViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem btnClose { get; set; }

		[Outlet]
		MonoTouch.UIKit.UICollectionView CalendarCollection { get; set; }

		[Outlet]
		Hot5.CalendarOptionButton calendarOptionA { get; set; }

		[Outlet]
		Hot5.CalendarOptionButton calendarOptionB { get; set; }

		[Outlet]
		Hot5.CalendarOptionButton calendarOptionC { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView vwDays { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
