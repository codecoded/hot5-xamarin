﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using System.ComponentModel;
using LeadedSky.Shared;

namespace Hot5
{
  partial class CalendarOptionButton : UIButton
  {

    public DateTime Date { get; private set; }
    public CalendarViewDataSource.DateSelectedDelegate DateSelected { get; set; }
    public UILabel CalendarTitle { get; set; }
    public UILabel CalendarDetail { get; set; }

    public CalendarOptionButton()
    {
      initialize();
    }

    public CalendarOptionButton(IntPtr handle) : base(handle)
    {
      initialize();
    }
    //
    //    public CalendarOptionView(RectangleF frame) : base (frame)
    //    {
    //      initialize();
    //    }

    [Export("initWithCoder:")]
    public CalendarOptionButton(NSCoder coder) : base(coder)
    {
      initialize();
    }
      
    void initialize()
    {
      createTitleLabel();
      createDetailLabel();
      doHooks();
      Date = DateTimeExtensions.Today;

    }
      
    public void SetStartDate(DateTime date)
    {
      Date = date;
      CalendarTitle.Text = getStartTitleText();
      CalendarDetail.Text = Date.ToString("d MMM").ToUpper();
    }

    public void SetEndDate(DateRange dateRange)
    {
      Date = dateRange.EndDate;
      CalendarTitle.Text = getEndTitleText(dateRange.TotalDays);
      CalendarDetail.Text = string.Format("Leave {0}", Date.ToString("d MMM").ToUpper());   
    }

    string getStartTitleText()
    {
      if(Date == DateTimeExtensions.Today)
        return "Today";
      else if(Date == DateTimeExtensions.Tomorrow)
        return "Tomorrow";
      else
        return Date.DayOfWeek.ToString();
    }

    string getEndTitleText(int totalNights)
    {
      return string.Format("{0} {1}", totalNights, totalNights == 1 ? "Night" : "Nights");
    }
   
    void doHooks()
    {
      this.AddTarget((object sender, EventArgs e) => {
        this.BackgroundColor = UIColor.White;
      },UIControlEvent.TouchUpOutside | UIControlEvent.TouchUpInside);

      this.TouchDown += delegate {
        this.BackgroundColor = AppConfig.PrimaryColor;
      };

      this.TouchUpInside += delegate {
        if(DateSelected != null)
          DateSelected(Date, null);
      };
    }

    void createTitleLabel()
    {
      CalendarTitle = new UILabel(new RectangleF(0, 5, 106, 21)) { 
        Font = AppConfig.Font(AppConfig.FontSize-2f),
        Opaque = true,
        TextAlignment = UITextAlignment.Center,
        TextColor = UIColor.DarkTextColor
      };

      AddSubview(CalendarTitle);
    }

    void createDetailLabel()
    {
      CalendarDetail = new UILabel(new RectangleF(0, 28, 106, 21)) { 
        Font = AppConfig.Font(10f),
        Opaque = true,
        TextAlignment = UITextAlignment.Center,
        TextColor = UIColor.DarkTextColor
      };

      AddSubview(CalendarDetail);
    }
  }
}
