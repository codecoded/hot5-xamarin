using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

using LeadedSky.Shared;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  partial class CalendarViewController : UIViewController
  {
    CalendarViewDataSource DataSource;

    public bool SettingEndDate { get; private set; }

    public DateRange CalendarRange { get; set; }

    public DateRange SelectedDates { get; set; }

    public DatesSelectedDelegate DatesSelected { get; set; }

    public delegate void DatesSelectedDelegate(DateRange selectedDats);

    public CalendarViewController(IntPtr handle) : base(handle)
    {
      var startDate = DateTime.Today.AddDays(-6.0d - (int)DateTime.Today.DayOfWeek);
      CalendarRange = new DateRange(startDate, DateTime.Today.AddMonths(11));
      SelectedDates = DateRange.DaysFromNow(1);
      SettingEndDate = false;
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
    }

    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      loadData();
      DoHooks();
      doLayout();
    }

    public override void ViewDidAppear(bool animated)
    {
      base.ViewDidAppear(animated);
      //layoutOptions();
    }

    void DoHooks()
    {
      CalendarCollection.RegisterClassForCell(typeof(CalendarCollectionCell), CalendarViewDataSource.ReusableTableCellId);

      btnClose.Clicked += delegate {
        DismissViewController(true, null);
      };

      DataSource.DateSelected = DateSelected;
      SetStartDateCalendarOptions();
    }

    void doLayout()
    {
      var layout = (UICollectionViewFlowLayout)CalendarCollection.CollectionViewLayout;
      layout.SectionInset = UIEdgeInsets.Zero;
      layout.ItemSize = new SizeF(45f, 45f);
      layout.MinimumLineSpacing = 1f;
      layout.MinimumInteritemSpacing = 0f;
      vwDays.AddBorders(UIViewExtensions.BorderFlags.Bottom, UIColor.LightGray);
      layoutOptions();
    }

    void layoutOptions()
    {
      const int height = 59;
      calendarOptionA.AdjustFrame(y: UIScreen.MainScreen.Bounds.Bottom - height);
      calendarOptionB.AdjustFrame(y: UIScreen.MainScreen.Bounds.Bottom - height);
      calendarOptionC.AdjustFrame(y: UIScreen.MainScreen.Bounds.Bottom - height);
//      CalendarCollection.AdjustFrame(height: UIScreen.MainScreen.Bounds.Bottom - height - 200);
    }

    void loadData()
    {
      DataSource = new CalendarViewDataSource(CalendarRange.Days, SelectedDates, DateRange.MonthsFromNow(11));
      CalendarCollection.Source = DataSource;
    }

    void DateSelected(DateTime date, NSIndexPath indexPath)
    {

      if(!SettingEndDate)
        StartDateSelected(date, indexPath);
      else
        EndDateSelected(date);

      SettingEndDate = !SettingEndDate;
      CalendarCollection.ReloadData();
    }

    void StartDateSelected(DateTime startDate, NSIndexPath indexPath)
    {
      DataSource.SelectedDates = new DateRange(startDate);
      DataSource.AvailableDates = new DateRange(startDate.AddDays(1), startDate.AddDays(CalendarViewDataSource.MAX_DAYS_STAY));
      setEndDateCalendarOptions(startDate);
      scrollIfRequired(indexPath);
    }

    void scrollIfRequired(NSIndexPath selectedIndex)
    {
      if(selectedIndex == null)
        return;
      CalendarCollection.ScrollToItem(selectedIndex.IndexPathByAddingIndex(31), UICollectionViewScrollPosition.CenteredVertically, true);

    }

    void EndDateSelected(DateTime endDate)
    {
      DataSource.SelectedDates = new DateRange(DataSource.SelectedDates.StartDate, endDate);
      DataSource.AvailableDates = DateRange.MonthsFromNow(11);
      if(DatesSelected != null)
        DatesSelected(DataSource.SelectedDates);
      DismissViewController(true, null);
      SetStartDateCalendarOptions();
    }

    void SetStartDateCalendarOptions()
    {
      setUpCalendarOption(calendarOptionA, DateTimeExtensions.Today);
      setUpCalendarOption(calendarOptionB, DateTimeExtensions.Tomorrow);
      setUpCalendarOption(calendarOptionC, DateTimeExtensions.Today.AddDays(3));
    }

    void setUpCalendarOption(CalendarOptionButton option, DateTime dateOption)
    {
      option.SetStartDate(dateOption);
      option.DateSelected = DateSelected;
      option.AddBorders(UIViewExtensions.BorderFlags.Top, UIColor.LightGray);
    }

    void setEndDateCalendarOptions(DateTime startDate)
    {
      calendarOptionA.SetEndDate(new DateRange(startDate, startDate.AddDays(1)));
      calendarOptionB.SetEndDate(new DateRange(startDate, startDate.AddDays(2)));
      calendarOptionC.SetEndDate(new DateRange(startDate, startDate.AddDays(3)));
    }
  }
}
