using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using LeadedSky.Shared;

namespace Hot5
{
  partial class CalendarCollectionCell : UICollectionViewCell
  {
    private UILabel lblCalendarDay;
    public UILabel Label { get { return lblCalendarDay; } }


    public CalendarCollectionCell(IntPtr handle) : base(handle) {}
   
    [Export("initWithFrame:")]
    public CalendarCollectionCell(System.Drawing.RectangleF frame) : base(frame)
    {
      lblCalendarDay = new UILabel(ContentView.Frame);
      lblCalendarDay.TextAlignment = UITextAlignment.Center;
      lblCalendarDay.Lines = 2;
      ContentView.AddSubview(lblCalendarDay);
    }

  }
}
