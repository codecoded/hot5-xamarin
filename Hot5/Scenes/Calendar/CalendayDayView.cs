﻿using System;
using LeadedSky.Shared;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Hot5
{
  public class CalendayDayView
  {
    public DateTime Date { get; private set; }

    public string Title
    {
      get
      {
        return FirstOfMonthOrToday ? LongTitle : ShortTitle;
      }
    }


    public bool FirstOfMonthOrToday
    {
      get
      {
        return (Date.Day == 1 && Date.FutureDate()) || Date.PresentDate();
      }
    }

    public string ShortTitle
    {
      get{ return Date.Day.ToString(); }
    }

    public string LongTitle
    {
      get
      {
        return string.Format("{0}{1}{2}", Date.Day, Environment.NewLine, Date.ToString("MMM").ToUpper());
      }
    }

    public CalendayDayView(DateTime day)
    {
      this.Date = day.Date;
    }

    public UIFont Font(UIFont curFont)
    {
      return FirstOfMonthOrToday ? AppConfig.FontBold(12f) : curFont;
    }

    public UIColor BackgroundColour(DateRange selectedDates, DateRange availableDates)
    {
      if(Date.WithinDateRange(selectedDates))
        return AppConfig.PrimaryColor;
        
      var bgColor = UIColor.White;
      switch(Date.Month % 2)
      {
        case 0:
          bgColor = UIColor.FromRGB(0.95f, 0.95f, 0.95f);
          break;
        default:
          bgColor = UIColor.FromRGB(0.85f, 0.85f, 0.875f);
          break;
      }

      return Date.OutsideDateRange(availableDates) ? UIColor.White : bgColor;
    }

    public UIColor Colour(DateRange selectedRange, DateRange availableRange)
    {
      if(Date.WithinDateRange(selectedRange))
        return UIColor.White;

      if(Date.OutsideDateRange(availableRange))
        return UIColor.LightGray;

      return UIColor.Black;
    }

    public string CheckInDateDescription()
    {
      if(Date == DateTimeExtensions.Today)
        return "Today";
      else if(Date == DateTimeExtensions.Tomorrow)
        return "Tomorrow";
      else if(Date.WithinDateRange(DateRange.DaysFromNow(6)))
        return Date.DayOfWeek.ToString();
      else
        return Date.ToString("ddd dd MMM");
    }
  }
}

