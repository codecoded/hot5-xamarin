﻿using System;
using MonoTouch.UIKit;

using LeadedSky.Shared;
using MonoTouch.Foundation;
using System.Collections;
using System.Collections.Generic;

namespace Hot5
{
  public class CalendarViewDataSource : UICollectionViewSource
  {
    public const int MAX_DAYS_STAY = 30;

    public IList<DateTime> Days { get; private set; }
    public DateRange SelectedDates { get;  set; }
    public DateRange AvailableDates { get;  set; }
    public static NSString ReusableTableCellId = new NSString("CalendarCell");

    public DateSelectedDelegate DateSelected { get; set; }

    public delegate void DateSelectedDelegate(DateTime startDate, NSIndexPath index);

    public CalendarViewDataSource(IList<DateTime> days, DateRange selectedDates, DateRange availableDates)
    {
      Days = days;
      SelectedDates = selectedDates;
      AvailableDates = availableDates;
    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, MonoTouch.Foundation.NSIndexPath indexPath)
    {
      var cell = (CalendarCollectionCell)collectionView.DequeueReusableCell(ReusableTableCellId, indexPath);
      layoutCell(cell, Days[indexPath.Row]);
      return cell;
    }

    void layoutCell(CalendarCollectionCell cell, DateTime selectedDate)
    {
      var dayView = new CalendayDayView(selectedDate);

      cell.ContentView.BackgroundColor = dayView.BackgroundColour(SelectedDates, AvailableDates);
      cell.Label.Text = dayView.Title;
      cell.Label.TextColor = dayView.Colour(SelectedDates, AvailableDates);
      cell.Label.Font = dayView.Font(AppConfig.Font(14f));
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return Days.Count;
    }

    public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var date = Days[indexPath.Row];
      if(date.OutsideDateRange(AvailableDates))
        return;        
      if(DateSelected != null)
        DateSelected(date, indexPath);
    }

  }
}