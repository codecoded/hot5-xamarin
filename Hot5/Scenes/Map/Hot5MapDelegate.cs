﻿using System;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreLocation;
using System.Linq;
using System.Drawing;

using SDWebImage;
using Hot5.Core;
using LeadedSky.UI.iOS;
using System.Threading.Tasks;


namespace Hot5
{
  class Hot5MapDelegate : MKMapViewDelegate
  {
    public delegate void HotelSelectedDelegate(Hotel hotel);

    public HotelSelectedDelegate HandleHotelSelected { get; set; }

    public SearchingMap HandleSearchingMap { get; set; }

    public delegate void SearchingMap();

    public override MKAnnotationView GetViewForAnnotation(MKMapView mapView, NSObject annotation)
    {
      var hotelAnnotation = annotation as HotelAnnotation;

      if(hotelAnnotation == null)
        return null;

      var anView = mapView.DequeueReusableAnnotation(HotelAnnotation.Identifier) as Hot5MapAnnotationView;

      if(anView == null)
        anView = new Hot5MapAnnotationView(hotelAnnotation, HotelAnnotation.Identifier);
      else
        anView.HotelIndex = hotelAnnotation.Hotel.Index;
      return anView;
    }

    public override void DidSelectAnnotationView(MKMapView mapView, MKAnnotationView view)
    {
      HotelAnnotation annotation = view.Annotation as HotelAnnotation;

      if(annotation == null)
        return;

      if(HandleHotelSelected != null)
        HandleHotelSelected(annotation.Hotel);  
    }

    public override void RegionChanged(MKMapView mapView, bool animated)
    {
      var hot5Map = (mapView as Hot5MapView);
      if(hot5Map == null)
        return;

//      if(!hot5Map.Searching)
      ((Hot5MapView)mapView).SearchingLabel.ShowTapToSearch();

//      if(HandleSearchingMap != null)
//        HandleSearchingMap();
    }


  }
}

