﻿using System;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using Hot5.Core;

namespace Hot5
{
  public class HotelAnnotation : MKAnnotation
  {
    public Hotel Hotel { get; private set; }

    public static string Identifier = "HotelAnnotationId";

    public HotelAnnotation(Hotel hotel, CLLocationCoordinate2D coord)
    {
      Hotel = hotel;
      Coordinate = coord;
    }

    public override string Title{ get { return Hotel.Name; } }

    public override CLLocationCoordinate2D Coordinate { get; set; }
  }
}

