// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("MapHotelCell")]
	partial class MapHotelCell
	{
		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView ActivityInd { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HotelAddress { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView HotelImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HotelIndex { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HotelName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HotelPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel HotelStarRating { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgMask { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Saving { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ActivityInd != null) {
				ActivityInd.Dispose ();
				ActivityInd = null;
			}
			if (HotelAddress != null) {
				HotelAddress.Dispose ();
				HotelAddress = null;
			}
			if (HotelImage != null) {
				HotelImage.Dispose ();
				HotelImage = null;
			}
			if (HotelIndex != null) {
				HotelIndex.Dispose ();
				HotelIndex = null;
			}
			if (HotelName != null) {
				HotelName.Dispose ();
				HotelName = null;
			}
			if (HotelPrice != null) {
				HotelPrice.Dispose ();
				HotelPrice = null;
			}
			if (HotelStarRating != null) {
				HotelStarRating.Dispose ();
				HotelStarRating = null;
			}
			if (imgMask != null) {
				imgMask.Dispose ();
				imgMask = null;
			}
			if (Saving != null) {
				Saving.Dispose ();
				Saving = null;
			}
		}
	}
}
