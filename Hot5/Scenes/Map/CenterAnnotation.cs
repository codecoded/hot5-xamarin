﻿using System;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;

namespace Hot5
{
  public class CenterAnnotation : MKAnnotation
  {
    public override CLLocationCoordinate2D Coordinate { get; set; }

    public CenterAnnotation(CLLocationCoordinate2D coordinate)
    {
      Coordinate = coordinate;

    }
  }

}

