// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXSearchingLabel")]
	partial class UIXSearchingLabel
	{
		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView actIndicator { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblSearching { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
