﻿using System;
using MonoTouch.MapKit;
using MonoTouch.UIKit;
using Hot5.Core;
using System.Drawing;

namespace Hot5
{
  public class Hot5MapAnnotationView : MKAnnotationView
  {
    public const string LOCATION_PIN_SELECTED = "location_pin_orange_noshadow.png";
    public const string LOCATION_PIN = "location_pin_grey_noshadow.png";

    public UILabel IndexLabel { get; private set; }

    private int hotelIndex;

    public int HotelIndex
    {
      get{ return hotelIndex; }
      set
      {
        if(value == hotelIndex)
          return;
        hotelIndex = value;
        setLabelDynamics();
      }
    }

    public override bool Selected
    {
      get
      {
        return base.Selected;
      }
      set
      {
        if(value == base.Selected)
          return;
        base.Selected = value;
        setImage();
      }
    }

    public Hot5MapAnnotationView(Hotel hotel)
    {
      Initialize(hotel);
    }

    public Hot5MapAnnotationView(HotelAnnotation annotation, string reuseIdentifier) : base(annotation, reuseIdentifier)
    {
      Initialize(annotation.Hotel);
    }

    public void Initialize(Hotel hotel)
    {
      HotelIndex = hotel.Index;
      setImage();
      Draggable = false;
      CenterOffset = new PointF(-Frame.Width / 4, -Frame.Height / 2);
      if(hotelIndex > 0)
        AddSubview(indexLabel());
    }


    UILabel indexLabel()
    {
      IndexLabel = new UILabel();
      IndexLabel.TextAlignment = UITextAlignment.Center;
      IndexLabel.Font = AppConfig.FontBold(14);
      IndexLabel.TextColor = UIColor.White;
      setLabelDynamics();
      return IndexLabel;
    }

    void setImage()
    {
      Image = Selected || HotelIndex == 0 ?UIImage.FromBundle(LOCATION_PIN_SELECTED) : UIImage.FromBundle(LOCATION_PIN); 
      ContentMode = UIViewContentMode.ScaleAspectFit;
      Frame = new RectangleF(0, 0, 25, 42);
    }

    void setLabelDynamics()
    {
      if(IndexLabel == null)
        return;
      IndexLabel.Text = HotelIndex.ToString();
      IndexLabel.Frame = new RectangleF(PointF.Empty, IndexLabel.SizeThatFits(IndexLabel.Frame.Size));
      IndexLabel.Center = new PointF(HotelIndex > 9 ?12 : 13, 13);
    }

  }
}

