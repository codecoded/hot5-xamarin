﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace Hot5
{
  [Register("UIXSearchingLabel")]
  public partial class UIXSearchingLabel : UIView
  {
    public UILabel SearchingLabel { get { return lblSearching; } }

    public UIXSearchingLabel(IntPtr handle) : base(handle)
    {
    }

    public UIXSearchingLabel(RectangleF frame) : base(frame)
    {
    }

    public UIXSearchingLabel()
    {

    }

    public void ShowTapToSearch()
    {
      lblSearching.Text = "Tap here to search";
      actIndicator.Hidden = true;
      UserInteractionEnabled = true;

      Show();
    }

    public void ShowSearching()
    {
      lblSearching.Text = "Searching...";
      actIndicator.Hidden = false;
      Show();
      UserInteractionEnabled = false;
    }

    public void ShowNoResults()
    {
      lblSearching.Text = "No hotels found";
      actIndicator.Hidden = true;
      UserInteractionEnabled = false;
      Show();
    }


    public void Show()
    {
      actIndicator.StartAnimating();
      UIView.Animate(0.250, () => {
        Alpha = 1;
      });
    }

    public void Hide()
    {
      actIndicator.StopAnimating();

      UIView.Animate(0.250, () => {
        Alpha = 0;
      });
    }
  }
}

