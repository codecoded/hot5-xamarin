﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using Hot5.Core;
using MonoTouch.Foundation;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class UIMapInfoView : UIView
  {
    UIPageControl PageController;
    MapHotelCollectionView HotelCollectionView;

    public HotelsResponse Response { get; set; }

    public const float HEIGHT = 130f;

    public HotelChangedDelegate HandleHotelChanged { get; set; }

    public delegate void HotelChangedDelegate(Hotel hotel);

    public int? SelectedHotelId;

    public UIMapInfoView(IntPtr handle) : base(handle)
    {
    }


    public UIMapInfoView(HotelsResponse response)
    {
      Response = response;

      Frame = new RectangleF(0, 
        UIScreen.MainScreen.Bounds.Bottom, 
        320, 
        HEIGHT);
      Hidden = true;

      HotelCollectionView = MapHotelCollectionView.Create(HEIGHT);

      addHotelCollection();
      AddSubview(HotelCollectionView);

      addPageControl();

    }

    public void Show()
    {
      Hidden = false;
      UIView.Animate(0.5, () => {
        Frame = new RectangleF(0, UIScreen.MainScreen.Bounds.Bottom - HEIGHT, UIScreen.MainScreen.Bounds.Width, HEIGHT);
      });
    }

    public void Hide()
    {
      UIView.Animate(0.5, () => {
        Frame = new RectangleF(0, UIScreen.MainScreen.Bounds.Bottom, UIScreen.MainScreen.Bounds.Width, HEIGHT);
      }, () => {
        Hidden = true;
      });
    }

    public void UpdateResponse(HotelsResponse response)
    {
      Response = response;
      addHotelCollection();
      ResetPageControl();
      if(!Hidden)
        manualSyncControls(this, null);
    }


    void addHotelCollection()
    {
      HotelCollectionView.Source = new UIMapsHotelCollectionSource(Response);
      HotelCollectionView.ScrollAnimationEnded += autoSyncControls;
      HotelCollectionView.DecelerationEnded += manualSyncControls;
    }

    void ResetPageControl()
    {
      PageController.Pages = Response.Hotels != null ?Response.Hotels.Count : 0;
    }

    void addPageControl()
    {
      PageController = new UIPageControl { 
        CurrentPage = 1, 
        DefersCurrentPageDisplay = true
      };


      PageController.Frame = new RectangleF(0, HEIGHT - 36, 320, 36);
      PageController.UserInteractionEnabled = true;
      PageController.ValueChanged += (sender, e) => {
        SetCurrentPage(PageController.CurrentPage, true);
        fireHotelChangedEvent();
      };
      AddSubview(PageController);
      BringSubviewToFront(PageController);

      ResetPageControl();
    }

    public void HandleHotelSelected(Hotel hotel)
    {
      if(hotel == null)
      {
        Hide();
        return;
      }

      SelectedHotelId = hotel.Id;

      var hotelPath = NSIndexPath.FromRowSection(hotel.Index - 1, 0);
      if(Hidden)
      {
        this.AddShadow();
        Show();
      }

      HotelCollectionView.ScrollToItem(hotelPath, UICollectionViewScrollPosition.Left, true);
    }

    void manualSyncControls(object sender, EventArgs e)
    {
      SetCurrentPage(checkScrollPage());
      fireHotelChangedEvent();
    }

    void fireHotelChangedEvent()
    {
      if(HandleHotelChanged != null && Response.Hotels != null && Response.Hotels.Count >= PageController.CurrentPage)
        HandleHotelChanged(Response.Hotels[PageController.CurrentPage]);
    }

    void autoSyncControls(object sender, EventArgs e)
    {
      SetCurrentPage(checkScrollPage());
    }

    int checkScrollPage()
    {
      return (int)Math.Round(HotelCollectionView.ContentOffset.X / Frame.Width);
    }

    public void SetCurrentPage(int pageNo, bool forceRefresh = false)
    {
      if(!forceRefresh && pageNo == PageController.CurrentPage)
        return;
      PageController.CurrentPage = pageNo;
    }

  }
}

