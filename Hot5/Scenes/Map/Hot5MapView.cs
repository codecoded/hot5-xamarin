using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using Hot5.Core;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Threading.Tasks;
using PubSub;

namespace Hot5
{
  partial class Hot5MapView : MKMapView
  {
    List<HotelAnnotation> HotelAnnotations = new List<HotelAnnotation>();
    public UIXSearchingLabel SearchingLabel;
    public bool Searching;
    public bool FirstSearch = true;

    public Hot5MapView(IntPtr handle) : base(handle)
    {
    }

    public void Initialize(HotelsResponse response, Hot5MapDelegate mapDelegate)
    {
      Delegate = mapDelegate;
      ShowsUserLocation = true;

      CreateSearchingLabel();

      SetCenterCoordinate(ResponseCenter(response.Info), true);
      SetResponse(response);
      ShowAnnotations(HotelAnnotations.ToArray(), true);

      AddTapGesture();
    }


    void AddTapGesture()
    {
      SearchingLabel.AddGestureRecognizer(new UITapGestureRecognizer(() => {
        RemoveAnnotations(HotelAnnotations.ToArray());
        SearchingLabel.ShowSearching();
        this.Publish(SearchingLabel);
      }));
    }

    public void SetResponse(HotelsResponse response, int? selectedId = null)
    {
      RemoveAnnotations(Annotations);

      if(response == null || response.Hotels == null)
      {
        SearchingLabel.ShowNoResults();
        Searching = false;
        return;
      }

      HotelAnnotations = response.Hotels.Select(hotel => createAnnotation(hotel)).ToList();

      var annotations = new List<MKAnnotation>(HotelAnnotations);

      if(response.Info.MyLocation)
        annotations.Add(new CenterAnnotation(ResponseCenter(response.Info)));

      AddAnnotations(annotations.ToArray());

      Searching = response.State != SearchState.Finished;

      if(selectedId != null)
      {
        var selectedAnnotation = HotelAnnotations.FirstOrDefault(e => e.Hotel.Id == selectedId);
        if(selectedAnnotation != null)
          SelectAnnotation(selectedAnnotation, false);
      }

      if(!Searching)
        ShowAnnotations(annotations.ToArray(), true);

      if(!Searching)
        SearchingLabel.Hide();

      FirstSearch = false;
    }

    public void ViewDisappearing()
    {
      Searching = false;
      SearchingLabel.Hide();
    }

    CLLocationCoordinate2D ResponseCenter(SearchResultsInfo info)
    {
      return new CLLocationCoordinate2D(info.Latitude, info.Longitude);
    }

    public void HandleHotelChanged(Hotel hotel)
    {
      var annotation = HotelAnnotations.Find(e => e.Hotel.Index == hotel.Index);
      if(SelectedAnnotations != null)
        SelectedAnnotations.ToList().ForEach(e => DeselectAnnotation(e, false));
      SelectAnnotation(annotation, true);
    }

    static HotelAnnotation createAnnotation(Hotel hotel)
    {
      return new HotelAnnotation(hotel, new CLLocationCoordinate2D(hotel.Latitude, hotel.Longitude));
    }

    static MKMapCamera createCamera(HotelsResponse response)
    {
      var camera = new MKMapCamera();
      camera.CenterCoordinate = new CLLocationCoordinate2D(response.Info.Latitude, response.Info.Longitude);
      camera.Altitude = 17000;
      return camera;
    }

    void CreateSearchingLabel()
    {
      SearchingLabel = NibLoader.Create<UIXSearchingLabel>();
      SearchingLabel.Center = new PointF(UIScreen.MainScreen.Bounds.Width / 2, 70);
      SearchingLabel.Alpha = 0;
      Add(SearchingLabel);
    }

  }
}
