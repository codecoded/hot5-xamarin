// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("MapViewController")]
	partial class UIMapViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CloseItem { get; set; }

		[Outlet]
		Hot5.Hot5MapView MapView { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
