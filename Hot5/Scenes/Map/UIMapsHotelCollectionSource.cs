﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.Foundation;

using MonoTouch.UIKit;
using Hot5.Core;
using SDWebImage;

namespace Hot5
{
  public class UIMapsHotelCollectionSource : UICollectionViewSource
  {
    public HotelsResponse Response;

    public UIMapsHotelCollectionSource(HotelsResponse response)
    {
      Response = response;
    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (MapHotelCell)collectionView.DequeueReusableCell(MapHotelCell.Key, indexPath);
      var hotel = Response.Hotels[indexPath.Row];
      hotel.Index = indexPath.Row + 1;
      cell.AddHotel(hotel);
      return cell;
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return (Response == null || Response.Hotels == null) ?0 : Response.Hotels.Count;
    }
  }
}

