﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using Hot5.Core;
using SDWebImage;
using LeadedSky.UI.iOS;
using System.Threading.Tasks;
using PubSub;
using Xamarin;

namespace Hot5
{
  public partial class MapHotelCell : UICollectionViewCell
  {
    public static readonly UINib Nib = UINib.FromName("MapHotelCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("MapHotelCell");
    public static float DefaultHeight = 150f;
    UITapGestureRecognizer TapGesture;

    UIImage Placeholder = UIImage.FromBundle("image_placeholder_small.png");
    UIActivityIndicatorView activityIndicator;

    public bool NewCell = true;

    public Hotel Hotel;

    public MapHotelCell(IntPtr handle) : base(handle)
    {
      createActivityIndicator();

    }

    public void AddHotel(Hotel hotel)
    {
      Initialize();
      SetHotel(hotel);
      SetImage(hotel);
    }

    public void Initialize()
    {
//      if(NewCell)
//      {
//        HotelImage.BackgroundColor = UIColor.Clear;
//      }
//
//      NewCell = false;
    }



    public void SetImage(Hotel hotel)
    {
      Hotel = hotel;
      string imageUrl = Hotel.MainImage.ImageUrl;
      HotelImage.Hidden = false;
      BringSubviewToFront(HotelImage);
      if(string.IsNullOrEmpty(imageUrl))
      {
        activityIndicator.StopAnimating();
        HotelImage.Image = Placeholder;
      }
      else
      {
        try
        {

          activityIndicator.StartAnimating();
          HotelImage.SetImage(new NSUrl(imageUrl), null, SDWebImageOptions.ContinueInBackground, (UIImage image, NSError error, SDImageCacheType cache) => {

            if(image != null && cache == SDImageCacheType.None)
            {
              HotelImage.Alpha = 0.0f;
              FadeInView(HotelImage, 1);

            }
            activityIndicator.StopAnimating();
            if(error != null)
            {
              HotelImage.Image = Placeholder;
              "Error loading image: {0}".Log(error);
            }
          });
        }
        catch(Exception ex)
        {
          Insights.Report(ex);
        }
      }
    }



    void FadeInView(UIView view, float duration = 0.2f)
    {
      view.Hidden = false;
      UIView.AnimateAsync(duration, () => {
        view.Alpha = 1;
      });
    }

    public void SetHotel(Hotel hotel)
    {
      Hotel = hotel;
      HotelName.Text = hotel.Name;
      HotelIndex.Text = string.Format("{0}.", hotel.Index);

      SetPriceLabel();
      DisplaySaving();

      HotelAddress.Text = hotel.Address;
      displayStarRating(hotel);
      AddTapGesture(hotel);
    }

    void displayStarRating(Hotel hotel)
    {
      HotelStarRating.Hidden = hotel.StarRating.GetValueOrDefault() == 0;
      if(HotelStarRating.Hidden)
        return;
      HotelStarRating.Text = hotel.DisplayStarRating;
      HotelStarRating.Font = AppConfig.FontBold(20);
    }

    void createActivityIndicator()
    {
      activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
      activityIndicator.HidesWhenStopped = true;
      activityIndicator.Hidden = false;
      activityIndicator.StartAnimating();
      activityIndicator.Center = ContentView.Center;
      ContentView.AddSubview(activityIndicator);
      // return activityIndicator;
    }

    void AddTapGesture(Hotel hotel)
    {
      TapGesture = new UITapGestureRecognizer();
      TapGesture.AddTarget(() => this.Publish<Hotel>(hotel));
      AddGestureRecognizer(TapGesture);
    }


    void SetPriceLabel()
    {
      HotelPrice.Hidden = Hotel.NoOffer;
      FadeInView(HotelPrice);
      var firstAttributes = new UIStringAttributes { Font = AppConfig.FontBold(14)  };

      var price = string.Format(SessionStore.CurrencyFormat, "{0:C0}", Hotel.Offer.MinPrice).ToMutAttrib();
      price.SetAttributes(firstAttributes.Dictionary, new NSRange(0, SessionStore.CurrencyFormat.CurrencySymbol.Length));
      HotelPrice.AttributedText = price;
    }

    void DisplaySaving()
    {

      float saving = Hotel.Offer.Saving;
      Saving.Hidden = !Hotel.ShowSaving;

      if(Saving.Hidden)
        return;
      FadeInView(Saving);
      Saving.Font = AppConfig.Font(14);
      Saving.Text = string.Format("{0}% saving", Convert.ToInt16(saving));

    }
  }
}

