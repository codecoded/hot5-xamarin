using System;
using MonoTouch.UIKit;
using Hot5.Core;
using LeadedSky.UI.iOS;
using PubSub;
using System.Threading.Tasks;
using System.Linq;

namespace Hot5
{
  partial class UIMapViewController : UIViewController
  {
    static HotelsResponse Response { get { return AppRequestResponse.Response; } set { AppRequestResponse.Response = value; } }

    static HotelsRequest Request { get { return AppRequestResponse.Request; } }

    UIMapInfoView InfoPanel;
    public HotelsSearchService SearchService;

    const int MaxPollCount = 15;
    const int POLL_INTERVAL_MS = 2500;

    public UIMapViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      doLoad();
      doHooks();
      doLayout();
    }

    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      InfoPanel.HandleHotelChanged = MapView.HandleHotelChanged;
      CloseItem.Clicked += Dismiss;
      this.Subscribe<Hotel>(PresentHotel);
      this.Subscribe<HotelsResponse>(Refresh);
      this.Subscribe<UIXSearchingLabel>(SearchMap);

//      "reloadRequest={0} reloadResponse={1} ".Log(AppSession.ReloadRequest, AppSession.ReloadResponse);
//      if(AppSession.ReloadRequest)
//        OnSearchinghMap();
//      else if(AppSession.ReloadResponse)
//        UpdateHotels(Response);

    }

    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
      InfoPanel.HandleHotelChanged = null;
      CloseItem.Clicked -= Dismiss;
      this.Unsubscribe<Hotel>();
      this.Unsubscribe<HotelsResponse>();
      this.Unsubscribe<UIXSearchingLabel>();

      MapView.ViewDisappearing();
    }

    public void PresentHotel(Hotel hotel)
    {
      "Hotel selected: Handle: {0}, Hotel={1}".Log(Handle, hotel.Name);
      var HotelController = (UIHotelController)Storyboard.InstantiateViewController("UIHotelController");
      HotelController.Hotel = hotel;
      HotelController.Request = Request.ToHotelRequest(hotel);
      NavigationController.PushViewController(HotelController, true);
    }

    void Dismiss(object sender, EventArgs e)
    {
      DismissViewController(true, null);
    }

    void doLoad()
    {
      InfoPanel = new UIMapInfoView(Response);
      View.AddSubview(InfoPanel);
      SearchService = new HotelsSearchService();
    }

    void doHooks()
    {
      var mapDelegate = new Hot5MapDelegate();
      mapDelegate.HandleHotelSelected = InfoPanel.HandleHotelSelected;
      //mapDelegate.HandleSearchingMap = OnSearchinghMap;

      MapView.Initialize(Response, mapDelegate);

    }

    void SearchMap(UIXSearchingLabel label)
    {
      OnSearchinghMap();
    }

    void OnSearchinghMap()
    {
      var coords = MapView.CenterCoordinate;
      "Searching Map. Lat: {0}, Long: {1}".Log(coords.Latitude, coords.Longitude);

      //MapView.StartSearch();
      InfoPanel.Hide();
      Request.SetMyLocation(coords.Latitude, coords.Longitude);
      NewSearchRequest();
      MapView.FirstSearch = true;
    }

    public void Refresh(HotelsResponse response)
    {
      AppSession.RunOnMainThread(() => UpdateHotels(response));
    }

    void UpdateHotels(HotelsResponse response)
    {
      Response = response;

      AppRequestResponse.LogResponse();

      if(Response == null)
      {
        DisplayNoHotels();
        return;
      }

      if(AppRequestResponse.IgnoreResponse(response.Info))
        return;

      if(Response.NoHotels)
      {
        DisplayNoHotels();
        return;
      }

      Request.Key = Response.Info.Key;
      AppRequestResponse.RemoveBlankHotels();        
      InfoPanel.UpdateResponse(Response);
      MapView.SetResponse(Response, InfoPanel.SelectedHotelId);
    }


    void DisplayNoHotels()
    {
      AppRequestResponse.CancelRequest = true;
      InfoPanel.Hide();
      MapView.SetResponse(null, null);
    }


    async void StartPollingTimer()
    {
      int pollCount = 0;
      while(!AppRequestResponse.Finished && pollCount <= MaxPollCount && !AppRequestResponse.Errored)
      {
        "Polling for results. count={0}".Log(pollCount++);
        SearchService.Search(Request);
        if(pollCount == 1)
          await Task.Delay(2000);
        else
          await Task.Delay(POLL_INTERVAL_MS);
      }
    }

    void NewSearchRequest()
    {
      AppRequestResponse.ResetRequest(false);
      SearchService.ResetCount();
      InfoPanel.SelectedHotelId = null;
      InfoPanel.Hide();
      AppRequestResponse.ReloadResponse = true;
//      Request.Sort = "distance";
      StartPollingTimer();
    }

    void doLayout()
    {
      NavigationItem.Title = Response.Info.MyLocation ?"Current Location" : Response.Info.Query;
    }
  }
}
