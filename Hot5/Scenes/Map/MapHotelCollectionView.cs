﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using Hot5.Core;
using MonoTouch.Foundation;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class MapHotelCollectionView : UICollectionView
  {

    public MapHotelCollectionView(RectangleF frame, UICollectionViewLayout layout) : base(frame, layout)
    {
      initialize();
    }

    public static MapHotelCollectionView Create(float height)
    {
      var frame = new RectangleF(0, 0, 
                    UIScreen.MainScreen.Bounds.Width, 
                    height);
      return new MapHotelCollectionView(frame, layoutCollection(height));
    }

    void initialize()
    {
      PagingEnabled = true;
      ClipsToBounds = false;
      ShowsHorizontalScrollIndicator = false;
      BackgroundColor = UIColor.DarkGray;;
      RegisterNibForCell(MapHotelCell.Nib, MapHotelCell.Key);
    }

    static UICollectionViewFlowLayout layoutCollection(float height)
    {
      UICollectionViewFlowLayout layout = new UICollectionViewFlowLayout();
      layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
      layout.ItemSize = new SizeF(320f, height);
      layout.MinimumLineSpacing = 0f;
      layout.MinimumInteritemSpacing = 0f;
      return layout;
    }
  }
}

