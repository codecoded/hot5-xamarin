// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("MainViewController")]
	partial class MainViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnViewHotels { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnWhen { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnWhere { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
