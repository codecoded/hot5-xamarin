using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using MonoTouch.CoreImage;
using LeadedSky.Shared;
using System.Drawing;
using Hot5.Core;
using LeadedSky.UI.iOS;
using MonoTouch.CoreLocation;

namespace Hot5
{
  partial class MainViewController : UIViewController
  {
    protected  LeadedSky.UI.iOS.SidebarController SidebarController
    { 
      get
      {
        return (UIApplication.SharedApplication.Delegate as AppDelegate).RootViewController.SidebarController;
      } 
    }

    UserCache UserCache { get { return AppSession.UserCache; } }


    public SearchResultView SelectedSearchResult { get; set; }

    public DateRange SelectedDates { get; set; }

    public HotelsRequest SearchRequest { get; set; }


    CLLocation CurrentLocation;

    public MainViewController(IntPtr handle) : base(handle)
    {
//      initialize();
    }

    void initialize()
    {
      SelectedDates = DateRange.DaysFromNow(1);

      SelectedSearchResult = new SearchResultView {
        Detail = "Current Location",
        Title = "Current Location", 
        SearchType = "place",
        Slug = "my-location"
      };



    }

    //    public override void ViewDidLoad()
    //    {
    //      base.ViewDidLoad();
    //      btnWhen.SetTitleColor(UIColor.Black, UIControlState.Normal);
    //      btnWhere.SetTitleColor(UIColor.Black, UIControlState.Normal);
    //      SearchResultChanged(SelectedSearchResult);
    //      CalendarDatesSelected(SelectedDates);
    //      DoHooks();
    //      LayoutScene();
    //    }


    public override void ViewWillAppear(bool animated)
    {
      base.ViewDidAppear(animated);
      NavigationController.NavigationBar.SetBackgroundImage(UIImage.FromBundle("navbar.jpg"), UIBarMetrics.Default);
    }

    public override void ViewWillDisappear(bool animated)
    {
      base.ViewDidDisappear(animated);
//      NavigationController.NavigationBar.SetBackgroundImage(null, UIBarMetrics.Default);

//      NavigationController.SetNavigationBarHidden(true,false);
    }

    HotelsRequest HotelsRequest()
    {
      var request = new HotelsRequest {
        Count = 15,
        Currency = UserCache.CurrencyCode,
        Slug = SelectedSearchResult.Slug,
        StartDate = SelectedDates.StartDate,
        EndDate = SelectedDates.EndDate
      };
      if(CurrentLocation != null)
      {
        request.Latitude = CurrentLocation.Coordinate.Latitude;
        request.Longitude = CurrentLocation.Coordinate.Longitude;
      }
      return request;
    }

    HotelRequest HotelRequest()
    {
      return new HotelRequest {
        Currency = UserCache.CurrencyCode,
        Slug = SelectedSearchResult.Slug,
        StartDate = SelectedDates.StartDate,
        EndDate = SelectedDates.EndDate
      };
    }


    void DoHooks()
    {
      AppSession.LocManager.LocationUpdated += HandleLocationChanged;
      btnViewHotels.TouchUpInside += (object sender, EventArgs e) => DoSearch();
    }

    void DoSearch()
    {
      if(SelectedSearchResult.SearchType == "hotel")
        PrepareHotelController();
      else
        PrepareHotelsController();
    }

    void PresentHotelController(UIViewController controller)
    {
      NavigationController.NavigationBar.SetBackgroundImage(null, UIBarMetrics.Default);
      NavigationController.PushViewController(controller, true);
    }

    public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
    {
      base.PrepareForSegue(segue, sender);

      switch (segue.Identifier)
      {
      case "CalendarSegue":
        PrepareCalendarController(segue.DestinationViewController as CalendarViewController);
        break;
      case "SearchSegue":
        PrepareSearchController(segue.DestinationViewController as UISearchViewController);
        break;
      }
    }

    void AddSettingsButton()
    {
      var btnCurrency = new UIBarButtonItem(UIImage.FromFile("settings.png")
        , UIBarButtonItemStyle.Plain
        , (sender, args) => SidebarController.ToggleMenu());
      btnCurrency.TintColor = AppConfig.SecondayColor;
      NavigationItem.SetLeftBarButtonItem(btnCurrency, false);
    }

    public void PresentCurrencyController()
    {

    }

    void PrepareSearchController(UISearchViewController searchViewController)
    {
      if(searchViewController != null)
        searchViewController.SearchComplete = SearchResultChanged;
    }

    void PrepareCalendarController(CalendarViewController calendarViewController)
    {
      if(calendarViewController == null)
        return;

      calendarViewController.SelectedDates = SelectedDates;     
      calendarViewController.DatesSelected = CalendarDatesSelected;
    }

    void PrepareHotelsController()
    {
      var hotelsViewController = Storyboard.InstantiateViewController("HotelsViewController") as UIHotelsViewController;
//      hotelsViewController.Request = HotelsRequest();
      hotelsViewController.LocationTitle = SelectedSearchResult.Title;
      PresentHotelController(hotelsViewController);
    }

    void PrepareHotelController()
    {
      var hotelController = Storyboard.InstantiateViewController("UIHotelController") as UIHotelController;

      hotelController.Request = HotelRequest();

//      var hotel = new Hotel {
//        Address = "Test",
//        Amenities = HotelAmenity.HotelAmenitiesFlags.Boutique,
//        Channel = "test",
//        City = "test cit",
//        Description = "wah",
//        Distance = 1f,
//        Id = 1,
//        Latitude = 50f,
//        Longitude = 50,
//        Name = "test"
//      };

//      hotelController.Hotel = hotel;

      hotelController.Hotel = new Hotel {
        Name = SelectedSearchResult.Title,
        Slug = SelectedSearchResult.Slug
      };
//
      PresentHotelController(hotelController);
//
      ApiClient.HotelSearch(HotelRequest(), response => {
        var hotelsResponse = HotelResponseParser.Parse<HotelResponse>(response);


        using (var pool = new NSAutoreleasePool())
          pool.BeginInvokeOnMainThread(() => hotelController.Hotel = hotelsResponse.Hotel);

      }); 

     
    }


    void LayoutScene()
    {
      btnWhen.Font = AppConfig.Font();
      btnWhere.Font = AppConfig.Font();
//      View.SetBackgroundImage(AppConfig.MainBackgroundImage);

      btnWhere.ImageEdgeInsets = new UIEdgeInsets(0, 255, 0, 0);
      btnWhere.TitleEdgeInsets = new UIEdgeInsets(0, -20, 0, 40);
      btnWhen.ImageEdgeInsets = new UIEdgeInsets(0, 255, 0, 0);
      btnWhen.TitleEdgeInsets = new UIEdgeInsets(0, -20, 0, 0);
      NavigationController.NavigationBar.SetBackgroundImage(UIImage.FromBundle("navbar.jpg"), UIBarMetrics.Default);
      AddSettingsButton();

    }

    void SearchResultChanged(SearchResultView selectedSearchResultView)
    {
      SelectedSearchResult = selectedSearchResultView;

      var title = AppConfig.Bold(selectedSearchResultView.Title).Concatenate(string.Format("{0}", selectedSearchResultView.TitlelessDetail));
      btnWhere.SetAttributedTitle(title, UIControlState.Normal);
    }

    void CalendarDatesSelected(DateRange selectedDates)
    {
      SelectedDates = selectedDates;
      var dayView = new CalendayDayView(SelectedDates.StartDate);
      var nights = SelectedDates.TotalDays;

      var description = "Check-in "
        .Concatenate(AppConfig.Bold(dayView.CheckInDateDescription()))
        .Concatenate(" for ")
        .Concatenate(AppConfig.Bold(string.Format("{0} {1}", nights, nights == 1 ?"night" : "nights")));

      btnWhen.SetAttributedTitle(description, UIControlState.Normal);
    }

    public void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
    {
      // Handle foreground updates
      CurrentLocation = e.Location;

    }

  }
}
