// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("NoResultsView")]
	partial class UINoResultsView
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView NoResultsImage { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel NoResultsLabel { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (NoResultsImage != null) {
				NoResultsImage.Dispose ();
				NoResultsImage = null;
			}
			if (NoResultsLabel != null) {
				NoResultsLabel.Dispose ();
				NoResultsLabel = null;
			}
		}
	}
}
