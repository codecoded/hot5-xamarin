using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using LeadedSky.UI.iOS;
using LeadedSky.Shared;

namespace Hot5
{
  public partial class UICalendarButton : UIButton
  {
    public UICalendarButton(IntPtr handle) : base(handle)
    {
      Initialize();
    }

    void Initialize()
    {
      ImageEdgeInsets = new UIEdgeInsets(0, 280, 0, 0);
      TitleEdgeInsets = new UIEdgeInsets(0, -20, 0, 0);
      this.AddBorders(UIViewExtensions.BorderFlags.Bottom, UIColor.Black.ColorWithAlpha(0.5f), 0, 0, 0.5f);
      Font = AppConfig.Font();
      Layer.ZPosition = -100f;

    }

    public void SetDates(DateRange dates)
    {
      var dayView = new CalendayDayView(dates.StartDate);
      var nights = dates.TotalDays;

      var description = "Check-in "
        .Concatenate(AppConfig.Bold(dayView.CheckInDateDescription()))
        .Concatenate(" for ")
        .Concatenate(AppConfig.Bold(string.Format("{0} {1}", nights, nights == 1 ?"night" : "nights")));

      var c = new UIStringAttributes{ ForegroundColor = UIColor.White };

      description.AddAttributes(c, new NSRange(0, description.Length));
      SetAttributedTitle(description, UIControlState.Normal);
    }
  }
}
