﻿using System;

using PubSub;
using Hot5.Core;
using LeadedSky.UI.iOS;
using MonoTouch.UIKit;
using Pusher;
using LeadedSky.Shared;


namespace Hot5
{
  public class HotelsEventCoordinator
  {

    public UIHotelsViewController Controller { get; private set; }

    public HotelsEventCoordinator(UIHotelsViewController controller)
    {
      Controller = controller;
      AppSession.LocManager.LocationUpdated += Controller.HandleLocationChanged;
      Controller.Subscribe<Currency>(Controller.OnCurrencySelected);
    }

    public void Attach()
    {
      "Attaching HotelsViewControllers Events: Handle={0}".Log(Controller.Handle);

      this.Subscribe<UIXHotelResultCell>(Controller.PresentHotel);
      Controller.PriceSlider.ValueChanged += Controller.PriceValueChanged;
      Controller.PriceSlider.TouchUpInside += Controller.PriceTouched;

      Controller.OptionsView.FilterButton.TouchUpInside += Controller.ShowFilterView;
      Controller.OptionsView.SortButton.TouchUpInside += Controller.ShowSortView;
      Controller.OptionsView.MapButton.TouchUpInside += Controller.ShowMapView;
      Controller.ScrollView.Scrolled += Controller.OptionsView.ControllerScrolled;
      Controller.SearchService.SearchErrored += Controller.OnSearchErrored;
      Reachability.ReachabilityChanged += Controller.ReachabilityChanged;
      //PusherClient.Pusher.GetEventSubscription<ResultsUpdateContract>().EventEmitted += Controller.PushMessageReceived;

    }

    public void Detach()
    {
      "Detaching HotelsViewControllers Events: Handle={0}".Log(Controller.Handle);

      this.Unsubscribe<HotelsResponse>();
      this.Unsubscribe<UIXHotelResultCell>();

      Controller.PriceSlider.ValueChanged -= Controller.PriceValueChanged;
      Controller.PriceSlider.TouchUpInside -= Controller.PriceTouched;

      Controller.OptionsView.FilterButton.TouchUpInside -= Controller.ShowFilterView;
      Controller.OptionsView.SortButton.TouchUpInside -= Controller.ShowSortView;
      Controller.OptionsView.MapButton.TouchUpInside -= Controller.ShowMapView;
      Controller.ScrollView.Scrolled -= Controller.OptionsView.ControllerScrolled;
      Controller.SearchService.SearchErrored -= Controller.OnSearchErrored;
      Reachability.ReachabilityChanged -= Controller.ReachabilityChanged;

      // PusherClient.Pusher.GetEventSubscription<ResultsUpdateContract>().EventEmitted -= Controller.PushMessageReceived;

    }
      
  }
}

