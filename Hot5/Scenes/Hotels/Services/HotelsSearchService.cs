﻿using System;
using Hot5.Core;
using System.Threading.Tasks;
using LeadedSky.UI.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using PubSub;
using RestSharp;

namespace Hot5
{
  public class HotelsSearchService
  {
    public RestSharp.RestRequestAsyncHandle RequestHandle;
    int searchCount;

    public int RetryCount { get; private set; }

    public EventHandler SearchErrored { get; set; }

    public HotelsSearchService(int retryCount = 10)
    {
      RetryCount = retryCount;
    }

    public void ResetCount()
    {
      searchCount = 0;
    }

    public  void StartHotelsSearch(HotelsRequest request)
    {
      searchCount++;
      RequestHandle = ApiClient.StartHotelsSearch(request, ProcessResponse);
    }

    public  void Search(HotelsRequest request)
    {
      //searchCount++;
      RequestHandle = ApiClient.HotelsResults(request, ProcessResponse);
    }

    public void ProcessResponse(HotelsRequest request, IRestResponse response)
    {
      if(!ValidResponse(response))
      {
        LogAndRetry(request, response);
        return;
      }
      else
      {
        var hotelsResponse = HotelResponseParser.Parse<HotelsResponse>(response.Content);
        if(hotelsResponse != null && hotelsResponse.Hotels != null)
        {
          hotelsResponse.SetHotelIndices();
          hotelsResponse.SetCurrency();
        }
        this.Publish<HotelsResponse>(hotelsResponse);
      }
    }

    void LogAndRetry(HotelsRequest request, IRestResponse response)
    {
      if(response != null)
      {
        if(response.ResponseUri != null)
          "API Response invalid: status={0}, uri={1}".Log(response.StatusCode, response.ResponseUri.AbsoluteUri);
        else
          "API Response invalid: status={0}".Log(response.StatusCode);

        if(response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
        {
          if(SearchErrored != null)
            SearchErrored(this, EventArgs.Empty);
          return;
        }
      }
      else
        "API Response invalid: No response given".Log();



      Retry(request);
    }

    bool ValidResponse(IRestResponse response)
    {
      return response.StatusCode == System.Net.HttpStatusCode.OK;
    }

    public void Abort()
    {
      if(RequestHandle != null)
        RequestHandle.Abort();
    }

    void Retry(HotelsRequest request)
    {
      searchCount++;
      Console.WriteLine("Retrying search");
      if(searchCount < RetryCount)
        Search(request);
      else
      {
        Console.WriteLine(string.Format("Exceeded retry count of {0}", RetryCount));

        Abort();
        if(SearchErrored != null)
          SearchErrored(this, EventArgs.Empty);
//        using (var pool = new NSAutoreleasePool())
//        {
//          pool.BeginInvokeOnMainThread(delegate {
//            new UIAlertView("Uh-oh!", "There seems to be a problem retrieving the latest prices. Best try starting the search again!", null, "Ok").Show();
//          });
//        }
      }

    }

  }
}

