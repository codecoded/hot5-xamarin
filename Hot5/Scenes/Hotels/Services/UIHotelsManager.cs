﻿using System;
using Hot5.Core;
using LeadedSky.UI.iOS;
using System.Drawing;
using MonoTouch.UIKit;

namespace Hot5
{
  public class UIHotelsManager
  {

    readonly UIHotelsViewController Controller;

    static HotelsRequest Request { get { return AppRequestResponse.Request; } }

    UINoResultsView NoResultsView;
    LoadingOverlay LoadingOverlay;

    public string LoadingTitle
    { 
      get { return LoadingOverlay.Title; } 
      set { LoadingOverlay.Title = value; }
    }

    protected  SidebarController SidebarController
    { 
      get
      {
        return AppSession.RootController.SidebarController;
      } 
    }

    public UIHotelsManager(UIHotelsViewController controller)
    {
      Controller = controller;
      DoLayout();
    }


    public void DoLayout()
    {
      Controller.NavigationItem.Title = "Current Location";
      Controller.HotelTableView = new UIHotelTableView(UIScreen.MainScreen.Bounds);
      Controller.HotelTableView.Hidden = true;
      Controller.Add(Controller.HotelTableView);
      Controller.ScrollView.Add(Controller.HotelTableView);

      AddOptionsPanelView();
      AddSettingsButton();
      AddSearchButton();


      Controller.ScrollView.Layer.ZPosition = 5;

      SetCalendarDescription();

      var scrollViewFrame = Controller.ScrollView.Frame;

      /// var overlayTop = Controller.PriceSlider.Frame.Bottom + 20;
      var overlayTop = Controller.ScrollView.Frame.Y;
      var overlayHeight = Controller.OptionsView.Frame.Top - overlayTop;

      NoResultsView = new UINoResultsView(
        new RectangleF(0, overlayTop, UIScreen.MainScreen.Bounds.Width, overlayHeight)
      );

      LoadingOverlay = new LoadingOverlay(new RectangleF(0, overlayTop, UIScreen.MainScreen.Bounds.Width, overlayHeight));
      //LoadingOverlay.BackgroundColor = UIColor.Red;
      LoadingOverlay.Layer.ZPosition = 1000;
      LoadingTitle = "Comparing Hotel Prices";
      NoResultsView.Hidden = true;
      LoadingOverlay.Hidden = true;

      Controller.View.AddSubview(NoResultsView);
      Controller.View.Add(LoadingOverlay);

    }

    public void SetCalendarDescription()
    {
      Controller.BtnWhen.SetDates(Request.CalendarDates());
    }


    void AddOptionsPanelView()
    {
      const int optionsHeight = 50;
      Controller.OptionsView = NibLoader.Create<UIXOptionsPanelView>(new RectangleF(0, UIScreen.MainScreen.Bounds.Bottom - 108, Controller.View.Frame.Width, optionsHeight));
      Controller.OptionsView.Setup();
      Controller.View.AddSubview(Controller.OptionsView);
    }

    void AddSettingsButton()
    {
      var btnCurrency = new UIBarButtonItem(UIImage.FromFile("settings.png")
        , UIBarButtonItemStyle.Plain
        , (sender, args) => SidebarController.ToggleMenu());
      btnCurrency.TintColor = AppConfig.SecondayColor;
      Controller.NavigationItem.SetLeftBarButtonItem(btnCurrency, false);
    }

    void AddSearchButton()
    {
      var btnSearch = new UIBarButtonItem(UIBarButtonSystemItem.Search, Controller.ShowSearchView);
      btnSearch.TintColor = AppConfig.SecondayColor;
      Controller.NavigationItem.SetRightBarButtonItem(btnSearch, false);
    }


    public void DisplayNoConnectivity()
    {
      EnableDisableActions(false);
      NoResultsView.NoConnectivity();
      DisplayDialog();    
    }


    public void DisplayDialog()
    {
      AppSession.RunOnMainThread(delegate {
        HideLoadingOverlay();
        NoResultsView.Hidden = false;
        Controller.HotelTableView.Hidden = true;
      });
    }

    public void DisplayNoResults()
    {
//      if(!LoadingOverlay.Hidden)
//        return;
      NoResultsView.NoResults();
      DisplayDialog();
    }

    public void DisplayNoLocation()
    {
      NoResultsView.NoLocation();
      EnableDisableActions(false);
      Controller.NavigationController.NavigationBar.UserInteractionEnabled = true;

      DisplayDialog();    
    }

    public void DisplayError()
    {
      NoResultsView.Error();
      DisplayDialog();    
    }

    public void DisplayHotelResults()
    {
      Controller.ScrollView.ContentSize = Controller.HotelTableView.ContentSize;
      EnableDisableActions(true);
      HideLoadingOverlay();
      HideDialog();
      Controller.HotelTableView.Hidden = false;

    }

    public void HideDialog()
    {
      NoResultsView.Hidden = true;
    }

    public void HideLoadingOverlay()
    {
      LoadingOverlay.Hidden = true;
    }

    public void DisplayLoading()
    {
      HideDialog();
      EnableDisableActions(true);
      LoadingOverlay.Hidden = false;
    }

    public void EnableDisableActions(bool enabled)
    {
      Controller.OptionsView.UserInteractionEnabled = enabled;
      Controller.PriceSlider.UserInteractionEnabled = enabled;
      Controller.NavigationController.NavigationBar.UserInteractionEnabled = enabled;
      Controller.BtnWhen.UserInteractionEnabled = enabled;
    }

    public void UpdatePriceLabel(float? maxPrice)
    {
      AppSession.RunOnMainThread(delegate {

        if(Request.MaxPrice == null || maxPrice == null)
        {
          Controller.PriceLabel.Text = "MAX";
          Controller.PriceSlider.MaxValue = maxPrice.HasValue ?maxPrice.Value : 1000;
          Controller.PriceSlider.Value = Controller.PriceSlider.MaxValue;
          return;
        }
        Controller.PriceSlider.MaxValue = maxPrice.Value;
        Controller.PriceSlider.Value = Request.MaxPrice.Value;
        Controller.PriceLabel.Text = string.Format(SessionStore.CurrencyFormat, "{0:C0}", Controller.PriceSlider.Value);
      });

    }
  }
}
