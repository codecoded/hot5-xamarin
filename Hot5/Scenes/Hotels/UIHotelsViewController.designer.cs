// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("HotelsViewController")]
	partial class UIHotelsViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblDates { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblTitle { get; set; }

		[Outlet]
		Hot5.UIHotelsScrollView scrollHotels { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISlider slidePrice { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UICalendarButton btnWhen { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnWhen != null) {
				btnWhen.Dispose ();
				btnWhen = null;
			}
		}
	}
}
