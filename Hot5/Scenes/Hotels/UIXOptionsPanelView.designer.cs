// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXOptionsPanelView")]
	partial class UIXOptionsPanelView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnFilter { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnMap { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnSort { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnFilter != null) {
				btnFilter.Dispose ();
				btnFilter = null;
			}
			if (btnMap != null) {
				btnMap.Dispose ();
				btnMap = null;
			}
			if (btnSort != null) {
				btnSort.Dispose ();
				btnSort = null;
			}
		}
	}
}
