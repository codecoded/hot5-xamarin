﻿using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.Shared;
using Hot5.Core;
using LeadedSky.UI.iOS;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Pusher;
using MonoTouch.CoreLocation;
using PubSub;
using Xamarin;
using System.Threading;

namespace Hot5
{
  public partial class UIHotelsViewController : UIViewController
  {
    HotelsEventCoordinator EventCoordinator;
    UIHotelsManager UIManager;

    public UICalendarButton BtnWhen { get { return btnWhen; } }

    public UILabel PriceLabel { get { return lblPrice; } }

    static HotelsResponse Response { get { return AppRequestResponse.Response; } set { AppRequestResponse.Response = value; } }

    static HotelsRequest Request { get { return AppRequestResponse.Request; } }

    public UIHotelTableView HotelTableView;

    public UIXOptionsPanelView OptionsView;

    public UISlider PriceSlider { get { return slidePrice; } }

    public UIScrollView ScrollView { get { return scrollHotels; } }

    //    public bool Finished { get { return (Response != null && Response.State == SearchState.Finished) || CancelRequest; } }
    //
    //    bool CancelRequest { get { return AppSession.CancelRequest; } set { AppSession.CancelRequest = value; } }
    //
    //    public bool SameRequest
    //    {
    //      get
    //      {
    //        if(Response == null || Request == null || CancelRequest)
    //          return false;
    //        return Response.Info.Slug == Request.Slug
    //        && Response.Criteria.StartDate == Request.StartDate
    //        && Response.Criteria.EndDate == Request.EndDate
    //        && Response.Criteria.CurrencyCode == Request.Currency;
    //      }
    //    }

    public string LocationTitle;

    FilterViewController filterController;
    SortViewController sortController;
    UIMapViewController mapController;

    public HotelsSearchService SearchService;

    bool DoneInitialSearch;
    const int MaxPollCount = 15;
    const int POLL_INTERVAL_MS = 2500;
    bool Errored = false;

    public UIHotelsViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      UIManager = new UIHotelsManager(this);
      EventCoordinator = new HotelsEventCoordinator(this);
      SearchService = new HotelsSearchService();

      EventCoordinator.Attach();

      if(!AppSession.HasConnectivity)
        UIManager.DisplayNoConnectivity();
      else
      {
        bool currentLocation = SetCurrentLocation();
        if(currentLocation)
          UIManager.DisplayLoading();
      }
      Insights.Track("AppStarted");
      "UIHotelsViewController Loaded. Handle={0}".Log(Handle);
    }

    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      this.Subscribe<HotelsResponse>(Refresh);

      if(AppRequestResponse.ReloadRequest)
        CalendarDatesSelected(Request.CalendarDates());
      else if(AppRequestResponse.ReloadResponse)
        UpdateHotels(Response);
      AppRequestResponse.ReloadResponse = false;

    }

    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
      this.Unsubscribe<HotelsResponse>();
      //CancelRequest = true;

    }

    public void PresentHotel(UIXHotelResultCell hotelCell)
    {
      var hotel = hotelCell.Hotel;
      "Hotel selected: Handle: {0}, Hotel={1}".Log(Handle, hotel.Name);
      var HotelController = (UIHotelController)Storyboard.InstantiateViewController("UIHotelController");
      HotelController.Hotel = hotel;
      HotelController.Request = Request.ToHotelRequest(hotel);

      Insights.Track("HotelSelected", new Dictionary<string, string> {
        { "HotelId", hotel.Id.ToString() }
      });

      NavigationController.PushViewController(HotelController, true);
    }


    public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
    {
      base.PrepareForSegue(segue, sender);

      switch (segue.Identifier)
      {
      case "CalendarSegue":
        PrepareCalendarController(segue.DestinationViewController as CalendarViewController);
        break;
      }
    }

    void PrepareCalendarController(CalendarViewController calendarViewController)
    {
      if(calendarViewController == null)
        return;    
      calendarViewController.SelectedDates = Request.CalendarDates();
      calendarViewController.DatesSelected = CalendarDatesSelected;
    }

    public void PriceValueChanged(object sender, EventArgs e)
    {
      var newStep = (int)Math.Round(slidePrice.Value / 5);
      slidePrice.Value = (newStep * 5.0f);

      if(slidePrice.Value == slidePrice.MaxValue)
      {
        lblPrice.Text = "MAX";
        Request.MaxPrice = null;
        return;
      }
      lblPrice.Text = string.Format(SessionStore.CurrencyFormat, "{0:C0}", slidePrice.Value);
      Request.MaxPrice = slidePrice.Value;
    }

    public void PriceTouched(object sender, EventArgs e)
    {
      UIManager.LoadingTitle = "Filtering Hotel Prices";
      Search();
    }

    void Search()
    {
      if(!AppSession.HasConnectivity && Response == null)
        UIManager.DisplayNoConnectivity();
      else
      {
        UIManager.DisplayLoading();
        ScrollView.SetContentOffset(PointF.Empty, false);

        SearchService.Search(Request);
      }
    }

    public void Refresh(HotelsResponse response)
    {
      AppSession.RunOnMainThread(() => UpdateHotels(response));
    }

    void UpdateHotels(HotelsResponse response)
    {
      if(response == null)
      {
        UIManager.DisplayNoResults();
        return;
      }

      UIManager.EnableDisableActions(true);

      Response = response;

      "UIHotelsViewController::UpdateHotels: responseState={0} timestamp={1:G} hotelCount={2}".Log(Response.State, Response.Info.Timestamp.Value, (Response.Hotels == null ?0 : Response.Hotels.Count()));

      if(AppRequestResponse.IgnoreResponse(response.Info))
        return;

      HotelTableView.Hotels = Response.Hotels;

      Request.Key = Response.Info.Key;

      HideShowResults();
      UpdateInfo(Response.Info);
    }

    async void StartPollingTimer()
    {
      int pollCount = 0;
      while(!AppRequestResponse.Finished && pollCount <= MaxPollCount && !AppRequestResponse.Errored)
      {
        "Polling for results. count={0}".Log(pollCount++);
        SearchService.Search(Request);
        if(pollCount == 1)
          await Task.Delay(2000);
        else
          await Task.Delay(POLL_INTERVAL_MS);

      }
    }


    void HideShowResults()
    {
      if(HotelTableView.Hidden)
        UIManager.DisplayNoResults();
      else
        UIManager.DisplayHotelResults();
    }

    void UpdateInfo(SearchResultsInfo hotelInfo)
    {      
      Request.Sort = hotelInfo.Sort;

      if(hotelInfo.Amenities != null)
        Request.Amenities = hotelInfo.Amenities;

      if(hotelInfo.StarRatings != null)
        Request.StarRatings = hotelInfo.StarRatings.ConvertAll<string>(e => e.ToString());

      if(hotelInfo.MaxPrice.Value <= 0)
        return;

      UIManager.UpdatePriceLabel(hotelInfo.MaxPrice.Value);
    }

    public void ShowFilterView(object sender, EventArgs e)
    {
      filterController = (FilterViewController)Storyboard.InstantiateViewController("Hot5HotelFilter");
      filterController.SelectedStarRatings = Request.StarRatings;
      filterController.SelectedAmenities = Request.Amenities;
      filterController.HandleFilterHotels = ApplyFilter;
      PresentController(filterController);
    }

    public void ShowSortView(object sender, EventArgs e)
    {
      sortController = (SortViewController)Storyboard.InstantiateViewController("Hot5HotelSort");
      sortController.HandleSortHotels = ApplySort;
      sortController.SelectedSort = Request.Sort;
      PresentController(sortController);
    }

    public void ShowMapView(object sender, EventArgs e)
    {
//      if(Response == null || Response.Hotels == null || Response.Hotels.Count == 0)
//        return;
      mapController = (UIMapViewController)Storyboard.InstantiateViewController("Hot5HotelMap");
      AppRequestResponse.Response = Response;

      PresentController(mapController);
    }

    public void ShowSearchView(object sender, EventArgs e)
    {
      var searchViewController = (UISearchViewController)Storyboard.InstantiateViewController("SearchViewController");
      searchViewController.SearchComplete = SearchResultChanged;

      AppSession.RootController.NavController.PresentViewController(searchViewController, true, null);
    }


    public void SearchResultChanged(SearchResultView selectedSearchResultView)
    {
      NavigationItem.Title = selectedSearchResultView.Title;

      if(selectedSearchResultView.MyLocation)
      {
        if(!SetCurrentLocation())
        {
          ResetRequest();
          AppRequestResponse.CancelRequest = true;
          return;
        }
      }
      else
        Request.UpdateSelectedLocation(selectedSearchResultView);

      NewSearchRequest();
    }

    void PresentController(UIViewController controller)
    {
      var navController = new UINavigationController(controller);
      navController.NavigationBar.BackgroundColor = UIColor.White;
      PresentViewController(navController, true, null);
    }

    bool SetCurrentLocation()
    {
      if(AppSession.CurrentLocation == null || !AppSession.HasLocationServices)
      {
        UIManager.DisplayNoLocation();
        AppRequestResponse.CancelRequest = true;
        return false;
      }
      Request.SetMyLocation(AppSession.CurrentLocation.Coordinate.Latitude, AppSession.CurrentLocation.Coordinate.Longitude);
      return true;
    }

    public void CalendarDatesSelected(DateRange selectedDates)
    {
      Request.UpdateRequestDates(selectedDates);
      UIManager.SetCalendarDescription();
      NewSearchRequest();
    }

    void ApplySort(TableRow tableRowItem)
    {
      if(tableRowItem == null)
        return;
      Request.Sort = tableRowItem.Value;
      SearchService.ResetCount();
      UIManager.LoadingTitle = "Sorting Hotels";
      Search();
    }

    void ApplyFilter(IEnumerable tableRows)
    {
      if(tableRows == null)
        return;

      Request.StarRatings = new List<string>();
      Request.Amenities = new List<string>();

      foreach(var item in tableRows.Cast<TableRowItem>())
      {
        var header = (FilterOptions.Headers)Enum.Parse(typeof(FilterOptions.Headers), item.HeaderValue);

        switch (header)
        {
        case FilterOptions.Headers.StarRatings:
          Request.StarRatings.Add(item.Value);
          break;
        case FilterOptions.Headers.Amenities:
          Request.Amenities.Add(item.Value);
          break;
        }
       
      }
      SearchService.ResetCount();
      UIManager.LoadingTitle = "Filtering Hotels";
      Search();
    }

    void PresentSearchController(UISearchViewController searchViewController)
    {
      if(searchViewController != null)
        searchViewController.SearchComplete = SearchResultChanged;
    }

    public void OnCurrencySelected(Currency currency)
    {
      Request.UpdateCurrency(currency);
      UIManager.UpdatePriceLabel(null);
      NewSearchRequest();
    }

    public void OnSearchErrored(object sender, EventArgs e)
    {
      UIManager.DisplayError();
      Errored = true;
    }

    void NewSearchRequest()
    {

      ResetRequest();

      if(!AppSession.HasConnectivity)
        UIManager.DisplayNoConnectivity();
      else
      {
        UIManager.EnableDisableActions(true);
        UIManager.LoadingTitle = "Comparing Hotel Prices";
        UIManager.DisplayLoading();
        StartPollingTimer();
      }
    }

    void ResetRequest()
    {
      AppRequestResponse.ResetRequest();

      UIManager.UpdatePriceLabel(null);
      HotelTableView.Hotels = null;
      ScrollView.SetContentOffset(PointF.Empty, false);
      SearchService.ResetCount();

    }

    public void HandleLocationChanged(object sender, LocationUpdatedEventArgs e)
    {
      if(DoneInitialSearch)
        return;

      AppSession.CurrentLocation = e.Location;

      if(!SetCurrentLocation())
        return;

      AppSession.LocManager.LocationUpdated -= HandleLocationChanged;
      "Location Updated first time. Initiating Search".Log();
      DoneInitialSearch = true;
      NewSearchRequest();      
    }


    public void ReachabilityChanged(object sender, EventArgs e)
    {
      if(!AppSession.HasConnectivity)
        UIManager.DisplayNoConnectivity();
      else
        UIManager.DisplayHotelResults();
    }
  }
}

