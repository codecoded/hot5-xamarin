﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIXOptionsPanelView : UIParallaxView
  {

    public UIButton FilterButton { get { return btnFilter; } }

    public UIButton SortButton { get { return btnSort; } }

    public UIButton MapButton { get { return btnMap; } }

    public UIXOptionsPanelView(IntPtr handle) : base(handle)
    {
    }

    public UIXOptionsPanelView(RectangleF frame) : base(frame)
    {

    }

    public UIXOptionsPanelView()
    {

    }

    public void Setup()
    {
//      btnFilter.SetImage(UIImage.FromBundle("filter.png"), UIControlState.Normal);
//      btnSort.SetImage(UIImage.FromBundle("generic_sorting2.png"), UIControlState.Normal);
//      btnMap.SetImage(UIImage.FromBundle("map_icon_color.png"), UIControlState.Normal);

      this.AddBorders(UIViewExtensions.BorderFlags.Top, UIColor.LightGray);
      OriginalCenterY = Center.Y;
      Layer.ZPosition = 10;
    }
  }
}

