﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;

namespace Hot5
{
  public partial class UINoResultsView : UIView
  {
    public UIImageView Logo { get { return NoResultsImage; } }

    public UINoResultsView(IntPtr h) : base(h)
    {
      LoadNib();
    }

    public UINoResultsView() : base()
    {
      LoadNib();
    }

    public UINoResultsView(RectangleF frame) : base(frame)
    {
      LoadNib();
    }

    void LoadNib()
    {
      NSArray array = NSBundle.MainBundle.LoadNib("NoResultsView", this, null);
      UIView view = Runtime.GetNSObject(array.ValueAt(0)) as UIView;
      view.Frame = new RectangleF(PointF.Empty, new SizeF(this.Frame.Size));
      AddSubview(view);
    }

   
    public void NoResults()
    {
      var message = "Oh, it seems like there are no available hotels with the dates and options  you have selected. \u0011\u2028\u2028Try changing your filters or price range to broaden your search. ";
      SetMessage(message);
    }

    public void NoLocation()
    {
      var message = "It looks like we're unable to determine your current location. Please select a location to start searching.";
      SetMessage(message);
    }

    public void NoConnectivity()
    {
      var message = "There appears to be no internet connectivity. Please enable 3G or connect via Wifi to continue";
      SetMessage(message);
    }

    public void Error()
    {
      var message = "Uh-oh, there was an unexpected problem with your search!\u0011\u2028\u2028Please try again or an alternative location. If the problem persists, you can contact us on support@hot5.com. ";
      SetMessage(message);

    }

    public void ConnectionError()
    {
      var message = "Sorry,there is a problem communicating with the Hot5 server. \u0011\u2028\u2028Please try again, or if the problem persists, you can contact us at support@hot5.com. ";
      SetMessage(message);

    }

    public void SetMessage(string message)
    {
      AppSession.RunOnMainThread(delegate {
        NoResultsLabel.Text = message;
      });
     
    }
  }
}

