﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Runtime.CompilerServices;

namespace Hot5
{
  [Register("UIHotelsScrollView")]
  public class UIHotelsScrollView : UIScrollView
  {
    public UIHotelsScrollView(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelsScrollView(RectangleF frame) : base(frame)
    {
    }


    public UIHotelsScrollView()
    {
    }

    void initialize()
    {
      BackgroundColor = UIColor.Clear;
      ClipsToBounds = false;
    }

    public override UIView HitTest(PointF point, UIEvent uievent)
    {
      var view = base.HitTest(point, uievent);

      foreach(UIView subview in Subviews)
      {
        if(view != null && view.UserInteractionEnabled)
          break;
        var newPoint = ConvertPointToView(point, subview);
        view = subview.HitTest(newPoint, uievent);

      }

      return view;
    }
  }

}