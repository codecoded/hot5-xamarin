﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using LeadedSky.UI.iOS;
using PubSub;

namespace Hot5
{
  public partial class UIXHotelResultCell : UITableViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelResultCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelResultCell");

    Hotel hotel;
    readonly UICollectionView HotelCollection;
    UITapGestureRecognizer TapGesture;

    public Hotel Hotel
    {
      get{ return hotel; }
      set
      {
        var sameHotel = hotel != null && hotel.Slug == value.Slug;
        hotel = value;
        Refresh(sameHotel);
      }
    }


    public UIXHotelResultCell(IntPtr handle) : base(handle)
    {
      HotelCollection = new UIHotelCollectionView(new Rectangle(0, 0, 320, 200));
      Add(HotelCollection);
      HotelCollection.Layer.ZPosition = Layer.ZPosition - 1;
      UserInteractionEnabled = true;
    }

    void Refresh(bool sameHotel)
    {

      HotelCollection.Source = new HotelCollectionSource(Hotel);
      HotelCollection.ReloadData();
      if(!sameHotel)
        HotelCollection.SetContentOffset(PointF.Empty, false);

      AddTapGesture();

      SetPriceLabel();

      lblIndex.Text = string.Format("{0}.", hotel.Index);
      lblName.Text = hotel.Name;
      //      lblAddress.Text = hotel.Address;
      SetAddressLabel();

      DisplayRating();
      DisplaySaving();
      DisplayStarRating();

      if(hotel.Index == 1)
        this.AddShadow();
      //      imgSaving.Image = UIImage.FromFile("saving_4.png");
      //      imgRating.Image = UIImage.FromFile("like_2.png");
      //      this.AddBorders(UIViewExtensions.BorderFlags.Bottom, UIColor.White, 0, 0, 0.5f);
    }

    void SetPriceLabel()
    {
      if(hotel.Offer == null || hotel.Offer.MaxPrice == 0)
      {
        lblPrice.AttributedText = "N/A".ToMutAttrib();
        lblPrice.Hidden = true;
        lblPrice.Alpha = 0;
        return;
      }

      FadeInView(lblPrice);

      // lblPrice.Hidden = false;
      var firstAttributes = new UIStringAttributes { Font = AppConfig.FontBold(16)  };

      var price = string.Format(SessionStore.CurrencyFormat, "{0:C0}", hotel.Offer.MinPrice).ToMutAttrib();
      price.SetAttributes(firstAttributes.Dictionary, new NSRange(0, SessionStore.CurrencyFormat.CurrencySymbol.Length));
      lblPrice.AttributedText = price;
    }

    void SetAddressLabel()
    {
      var s = string.Format("{0:0.#} mi - {1}", hotel.DistanceInMiles(), hotel.Address);
      lblAddress.Text = s;
    }

    void AddTapGesture()
    {
      if(TapGesture != null)
        RemoveGestureRecognizer(TapGesture);
      TapGesture = new UITapGestureRecognizer();
      AddGestureRecognizer(TapGesture);
      TapGesture.AddTarget(() => this.Publish<UIXHotelResultCell>(this));
    }

    void DisplayRating()
    {

      lblRating.Hidden = !Hotel.ShowRating;
      if(lblRating.Hidden)
        return;
      
      float rating = Hotel.Ratings.Overall.Value;
//      lblRating.Font = AppConfig.Font();
      lblRating.Text = string.Format("{0}% Overall Rating", rating);
    }

    void DisplaySaving()
    {

      float saving = Hotel.Offer.Saving;
      lblSaving.Hidden = !Hotel.ShowSaving;

      if(lblSaving.Hidden)
        return;
      FadeInView(lblSaving);
      lblSaving.Font = AppConfig.Font();
      lblSaving.Text = string.Format("{0}% saving", Convert.ToInt16(saving));

    }

    void DisplayStarRating()
    {
      lblStarRating.Hidden = Hotel.StarRating.GetValueOrDefault() == 0;
      if(lblStarRating.Hidden)
        return;
      lblStarRating.Text = Hotel.DisplayStarRating;
      lblStarRating.Font = AppConfig.FontBold(20);
    }

    void FadeInView(UIView view)
    {
      view.Hidden = false;
      UIView.AnimateAsync(0.2, () => {
        view.Alpha = 1;
      });
    }
  }
}

