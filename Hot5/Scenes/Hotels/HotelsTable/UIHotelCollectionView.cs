﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class UIHotelCollectionView : UICollectionView
  {
    public UIHotelCollectionView(RectangleF frame) : base(frame, Layout())
    {
      UserInteractionEnabled = true;
      ScrollEnabled = true;
      PagingEnabled = true;
      AllowsSelection = false;
      ShowsVerticalScrollIndicator = false;
      DirectionalLockEnabled = true;
      ShowsHorizontalScrollIndicator = false;
      BackgroundColor = AppConfig.SecondayColor;
      RegisterClassForCell(typeof(HotelImageCollectionCell), HotelCollectionSource.ReusableTableCellId);
    }


    static UICollectionViewFlowLayout Layout()
    {
      //layout.SectionInset = UIEdgeInsets.Zero;
      var layout = new UICollectionViewFlowLayout();
      layout.ItemSize = new SizeF(320f, 200f);
      layout.MinimumLineSpacing = 0f;
      layout.MinimumInteritemSpacing = 0f;
      layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
      return layout;
    }
      
  }
}

