﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;

namespace Hot5
{
  public partial class UIXHotelsFooterView : UIView
  {
    public UIXHotelsFooterView(IntPtr h) : base(h)
    {
      LoadNib();
    }

    public UIXHotelsFooterView() : base()
    {
      LoadNib();
    }

    public UIXHotelsFooterView(RectangleF frame) : base(frame)
    {
      LoadNib();
    }

    void LoadNib()
    {
      NSArray array = NSBundle.MainBundle.LoadNib("UIXHotelsFooterView", this, null);
      UIView view = Runtime.GetNSObject(array.ValueAt(0)) as UIView;
      view.Frame = new RectangleF(PointF.Empty, new SizeF(this.Frame.Size));
      AddSubview(view);
    }
  }
}

