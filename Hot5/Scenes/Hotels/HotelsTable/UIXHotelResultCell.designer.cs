// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelResultCell")]
	partial class UIXHotelResultCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblAddress { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblIndex { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblRating { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblSaving { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblStarRating { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (lblAddress != null) {
				lblAddress.Dispose ();
				lblAddress = null;
			}
			if (lblIndex != null) {
				lblIndex.Dispose ();
				lblIndex = null;
			}
			if (lblName != null) {
				lblName.Dispose ();
				lblName = null;
			}
			if (lblPrice != null) {
				lblPrice.Dispose ();
				lblPrice = null;
			}
			if (lblRating != null) {
				lblRating.Dispose ();
				lblRating = null;
			}
			if (lblSaving != null) {
				lblSaving.Dispose ();
				lblSaving = null;
			}
			if (lblStarRating != null) {
				lblStarRating.Dispose ();
				lblStarRating = null;
			}
		}
	}
}
