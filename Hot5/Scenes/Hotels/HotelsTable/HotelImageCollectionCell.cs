using System;
using MonoTouch.UIKit;
using Hot5.Core;
using SDWebImage;
using MonoTouch.Foundation;
using System.Drawing;
using PubSub;
using LeadedSky.UI.iOS;
using Xamarin;

namespace Hot5
{
  partial class HotelImageCollectionCell : UICollectionViewCell
  {
    public UIImageView ImageView { get; set; }

    UIImage Placeholder = UIImage.FromBundle("image_placeholder_small.png");
    UIActivityIndicatorView activityIndicator;

    public Hotel Hotel;

    public HotelImageCollectionCell(IntPtr handle) : base(handle)
    {
      ImageView = new UIImageView(ContentView.Frame);
      createActivityIndicator();
      ContentView.AddSubview(ImageView);
      UserInteractionEnabled = true;
    }

    public void SetImage(Hotel hotel, int index)
    {
      Hotel = hotel;
      string imageUrl = null;// = Hotel.MainImage.ImageUrl;
  
      if(Hotel.Images != null && Hotel.Images.Count > index)
        imageUrl = Hotel.Images[index].Url;

//      if(activityIndicator == null)
//      {
      //createActivityIndicator(Center);
       
//      }


      if(string.IsNullOrEmpty(imageUrl))
      {
        activityIndicator.StopAnimating();
        ImageView.Image = Placeholder;
      }
      else
      {
        try
        {
         
          activityIndicator.StartAnimating();
          ImageView.SetImage(new NSUrl(imageUrl), null, SDWebImageOptions.ContinueInBackground, (UIImage image, NSError error, SDImageCacheType cache) => {

            if(image != null && cache == SDImageCacheType.None)
            {
              ImageView.Alpha = 0.0f;
              UIView.Animate(1, () => {
                ImageView.Alpha = 1.0f;
              });

            }
            activityIndicator.StopAnimating();
            if(error != null)
            {
              ImageView.Image = Placeholder;
              "Error loading image: {0}".Log(error);
            }
          });
        }
        catch(Exception ex)
        {
          Insights.Report(ex);
        }
      }
    }

    void createActivityIndicator()
    {
      activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
      activityIndicator.HidesWhenStopped = true;
      activityIndicator.Hidden = false;
      activityIndicator.StartAnimating();
      activityIndicator.Center = ContentView.Center;
      ContentView.AddSubview(activityIndicator);
      // return activityIndicator;
    }
      
  }
}
