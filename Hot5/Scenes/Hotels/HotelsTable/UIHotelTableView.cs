using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.Drawing;
using System.Linq;
using System.Collections.Generic;
using Hot5.Core;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIHotelTableView : UITableView
  {
    public Hotel SelectedHotel { get; set; }

    List<Hotel> hotels;

    public List<Hotel> Hotels
    {
      get{ return hotels; }
      set
      {
        hotels = value;
        Refresh();
      }
    }

    public UIHotelTableView(IntPtr handle) : base(handle)
    {
      RegisterNibForCellReuse(UIXHotelResultCell.Nib, UIXHotelResultCell.Key);
    }

    public UIHotelTableView(RectangleF frame) : base(frame)
    {
      RegisterNibForCellReuse(UIXHotelResultCell.Nib, UIXHotelResultCell.Key);
      ClipsToBounds = false;
      BackgroundColor = UIColor.White;
      Layer.MasksToBounds = false;
      ScrollEnabled = false;
      AllowsSelection = false;
      UserInteractionEnabled = true;
      Layer.ShadowRadius = 5f;
      Layer.ShadowOffset = new SizeF(0, -10f);
      Layer.ShadowColor = UIColor.Black.CGColor;
    }

    public void Refresh()
    {
      Source = new HotelTableDataSource(Hotels);
      ReloadData();
      Hidden = Hotels == null || Hotels.Count == 0;
//      if(!Hidden)
//        ScrollIndicatorInsets = new UIEdgeInsets(VisibleCells[0].Frame.Top, 0, 0, 0);
      if(ContentSize.Height > Frame.Height)
        Frame = new RectangleF(Frame.Location, ContentSize);
    }
   
  }
}
