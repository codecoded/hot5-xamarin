﻿using System;
using System.Collections.Generic;
using System.Drawing;

using MonoTouch.Foundation;

using MonoTouch.UIKit;
using Hot5.Core;
using SDWebImage;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class HotelCollectionSource : UICollectionViewSource
  {
    public static NSString ReusableTableCellId = new NSString("HotelImageCell");

    public Hotel Hotel { get; private set; }


    public HotelCollectionSource(Hotel hotel)
    {
      Hotel = hotel;
    }

    public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
    {
      "Item Selected: {0}".Log(Hotel.Name);
      collectionView.Superview.Log();
    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {    
      var cell = (HotelImageCollectionCell)collectionView.DequeueReusableCell(ReusableTableCellId, indexPath);
      cell.SetImage(Hotel, indexPath.Row);
      return cell;
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return Hotel.Images.Count > 0 ?Hotel.Images.Count : 1;
    }
      
  }
}

