﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using Hot5.Core;
using System.Collections.Generic;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class HotelTableDataSource : UITableViewSource
  {
    public static NSString ReusableTableCellId = new NSString("HotelTableCellId");

    public List<Hotel> Hotels;
    const float ROW_HEIGHT = 201;

    public HotelTableDataSource(List<Hotel> hotels)
    {
      Hotels = hotels;
    }

    public override int NumberOfSections(UITableView tableView)
    {
      return 1;
    }

    public override int RowsInSection(UITableView tableview, int sectionIndex)
    {
      return Hotels == null ?0 : Hotels.Count;
    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelResultCell)tableView.DequeueReusableCell(UIXHotelResultCell.Key, indexPath);
      cell.Hotel = Hotels[indexPath.Row];
      return cell;
    }

    public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
    {
      return ROW_HEIGHT;
//      Hotel hotel = Hotels[indexPath.Row];
//      if(hotel.ShowSaving && hotel.ShowRating)
//        return ROW_HEIGHT;
//      if(hotel.ShowSaving || hotel.ShowRating)
//        return ROW_HEIGHT - 25;
//      return ROW_HEIGHT - 50;
    }

    public override float GetHeightForFooter(UITableView tableView, int section)
    {
      return 178f;
    }

    public override UIView GetViewForFooter(UITableView tableView, int section)
    {
      return new UIXHotelsFooterView();
    }

  }
}

