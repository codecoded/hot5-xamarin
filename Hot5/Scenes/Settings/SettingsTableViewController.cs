using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.Collections.Generic;

using LeadedSky.UI.iOS;
using Hot5.Core;
using PubSub;
using MonoTouch.MessageUI;

namespace Hot5
{
  partial class SettingsTableViewController : UIViewController
  {
    List<string> Options;
    SettingsTableSource source;


    CurrencyTableViewController CurrencyController;

    SidebarController SidebarController  { get { return AppSession.RootController.SidebarController; } }

    UINavigationController NavController  { get { return AppSession.RootController.NavController; } }

    static UserCache UserCache { get { return AppSession.UserCache; } }

    MFMailComposeViewController mailController;


    string CurrentCurrencyDesc()
    {
      var selectedCurrency = UserCache.Currency;
      if(selectedCurrency == null)
        return "Currency";
      return string.Format("Currency ({0})", selectedCurrency.Code);
    }

    public SettingsTableViewController(IntPtr handle) : base(handle)
    {
    }


    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      ReloadData();
      UISettingsTableView.AllowsSelection = true;

      mailController = new MFMailComposeViewController();
      mailController.SetToRecipients(new []{ "support@hot5.com" });
      mailController.SetSubject("Hot5 Support");
      mailController.Finished += OnFinishedEmail;
      UISettingsTableView.RegisterNibForCellReuse(UIXSettingsTableCell.Nib, UIXSettingsTableCell.Key);

      this.Subscribe<CurrencyTableViewSource>(OnCurrencySelected);
    }


    public void OnRowSelected(object sender, EventArgs e)
    {
      var option = source.SelectedOption;
      if(option.ToLowerInvariant().StartsWith("currency"))
        PresentCurrencyController();
      else if(option.ToLowerInvariant().Equals("contact us"))
        SendEmail();
    }

    void PresentCurrencyController()
    {
      if(CurrencyController == null)
        CurrencyController = (CurrencyTableViewController)Storyboard.InstantiateViewController("CurrencyTableViewController");

      var navController = new UINavigationController(CurrencyController);

      NavController.PresentViewController(navController, true, null);
//      NavController.PresentViewController(CurrencyController, true, null);
    }

    public void OnCurrencySelected(CurrencyTableViewSource selectedCurrency)
    {
      UserCache.Currency = selectedCurrency.SelectedCurrency;
      UserCache.Save();
      CurrencyController.DismissViewController(true, null);
      ReloadData();
      SidebarController.CloseMenu();

    }

    void ReloadData()
    {
      Options = new List<string>{ CurrentCurrencyDesc(), "Contact Us" };
      source = new SettingsTableSource(Options);
      source.OnRowSelected += OnRowSelected;
      //UISettingsTableView.AdjustFrame(width: 180);
      UISettingsTableView.Source = source;
      UISettingsTableView.ReloadData();

    }

    void SendEmail()
    {

      var versionNo = NSBundle.MainBundle.InfoDictionary[new NSString("CFBundleVersion")].ToString();

      mailController = new MFMailComposeViewController();
      mailController.SetToRecipients(new []{ "support@hot5.com" });
      mailController.SetSubject(string.Format("Hot5 Support (v{0})", versionNo));
      mailController.Finished += OnFinishedEmail;
      NavController.PresentViewController(mailController, true, null);

    }

    void OnFinishedEmail(object sender, MFComposeResultEventArgs args)
    {
      Console.WriteLine(args.Result.ToString());
      args.Controller.DismissViewController(true, null);

    }

  }
}
