﻿
using System;
using System.Drawing;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using System.Collections.Generic;
using PubSub;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class CurrencyTableViewSource : UITableViewSource
  {
    public IEnumerable<IGrouping<string, Currency>> Currencies { get; set; }

    public Currency SelectedCurrency = null;

    public CurrencyTableViewSource(List<Currency> currenciesList)
    {
      Currencies = currenciesList.OrderBy(e => e.Country).GroupBy(e => e.Country[0].ToString());
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      SelectedCurrency = Currencies.ElementAt(indexPath.Section).ElementAt(indexPath.Row);
      this.Publish<CurrencyTableViewSource>(this);
    }


    public override int NumberOfSections(UITableView tableView)
    {
      // TODO: return the actual number of sections
      return Currencies.Count();
    }

    public override int RowsInSection(UITableView tableview, int section)
    {
      // TODO: return the actual number of items in the section
      return Currencies.ElementAt(section).Count();
    }

    public override string TitleForHeader(UITableView tableView, int section)
    {
      return Currencies.ElementAt(section).Key;
    }

    public override string TitleForFooter(UITableView tableView, int section)
    {
      return "";
    }

    //    public override UIView GetViewForHeader(UITableView tableView, int section)
    //    {
    //      var view = new UILabel {
    //        Text = TitleForHeader(tableView, section),
    //        BackgroundColor = UIColor.White,
    //        Font = AppConfig.FontBold(20),
    //        TextColor = AppConfig.PrimaryColor
    //      };
    //
    //
    //      return view;
    //    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {
      var cell = tableView.DequeueReusableCell(CurrencyTableViewCell.Key) as CurrencyTableViewCell;
      if(cell == null)
        cell = new CurrencyTableViewCell();
      cell.Currency = Currencies.ElementAt(indexPath.Section).ElementAt(indexPath.Row);

      return cell;
    }

  }
}

