// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("CurrencyTableViewController")]
	partial class CurrencyTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem navItemCancel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView TableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
