﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Hot5
{
  public partial class UIXSettingsTableCell : UITableViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXSettingsTableCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXSettingsTableCell");

    public UILabel LblSetting { get { return lblSetting; } }

    public UIXSettingsTableCell(IntPtr handle) : base(handle)
    {
    }

    public static UIXSettingsTableCell Create()
    {
      return (UIXSettingsTableCell)Nib.Instantiate(null, null)[0];
    }
  }
}

