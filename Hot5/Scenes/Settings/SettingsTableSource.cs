﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using Hot5.Core;
using System.Collections.Generic;
using LeadedSky.UI.iOS;
using PubSub;

namespace Hot5
{

  public class SettingsTableSource : UITableViewSource
  {

    public EventHandler OnRowSelected;
    public string SelectedOption = null;

    public List<string> Options;
    //    const float ROW_HEIGHT = 280;

    public SettingsTableSource(List<string> options)
    {
      Options = options;
//      Options.TrimExcess();
    }

    public override int NumberOfSections(UITableView tableView)
    {
      return 1;
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      SelectedOption = Options[indexPath.Row];
      if(OnRowSelected != null)
        OnRowSelected(this, EventArgs.Empty);
    }

    public override int RowsInSection(UITableView tableview, int sectionIndex)
    {
      if(Options == null)
        return 0;
      return Options.Count;
    }

    public override UITableViewCell GetCell(UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
    {
      var cell = tableView.DequeueReusableCell(UIXSettingsTableCell.Key) as UIXSettingsTableCell;
     

      cell.LblSetting.Text = Options[indexPath.Row];
//      cell.TextLabel.BackgroundColor = UIColor.Red;
//      // cell.BackgroundColor = UIColor.Blue;
//
//      cell.TextLabel.TextColor = UIColor.Yellow;//.ColorWithAlpha(0.8f);
//      cell.TextLabel.Font = AppConfig.FontBold(16);
//      //cell.AdjustFrame(width: UIStatusBarFrameChangeEventArgs.)
      cell.SelectionStyle = UITableViewCellSelectionStyle.None;
      cell.TextLabel.HighlightedTextColor = UIColor.LightGray;
////      cell.Highlighted = true;
//      //cell.AdjustFrame(width: 60);
//      if(indexPath.Row != Options.Count - 1)
//        cell.AddBorders(UIViewExtensions.BorderFlags.Bottom, UIColor.Black.ColorWithAlpha(0.2f), 0, 0, 1.5f);
      return cell;
    }

    public override float GetHeightForHeader(UITableView tableView, int section)
    {
      return 64;
    }



    //    public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
    //    {
    //      return 50f;
    //    }
    //    public override float GetHeightForHeader(UITableView tableView, int section)
    //    {
    //      return 44f;
    //    }
  }
}

