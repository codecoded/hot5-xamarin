﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.UI.iOS;
using Hot5.Core;
using System.Collections.Generic;

namespace Hot5
{
  public partial class CurrencyTableViewController : UIViewController
  {
    CurrencyTableViewSource source;
    public EventHandler CurrencyChanged;



    public CurrencyTableViewController(IntPtr handle) : base(handle)
    {
    }

    public override void DidReceiveMemoryWarning()
    {
      // Releases the view if it doesn't have a superview.
      base.DidReceiveMemoryWarning();
      
      // Release any cached data, images, etc that aren't in use.
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      var currencyList = AppSession.Currencies.CurrenciesList;

      if(currencyList == null)
        currencyList = CurrencyJsonFactory.LoadCurrencies();

//      View.SetBackgroundImage(AppConfig.ModalBackgroundImage);
      source = new CurrencyTableViewSource(currencyList);
      TableView.Frame = UIScreen.MainScreen.Bounds;

      TableView.Source = source;
      navItemCancel.Clicked += (sender, e) => DismissViewController(true, null);
    }
     

  }
}

