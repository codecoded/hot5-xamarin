﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;

namespace Hot5
{
  public class CurrencyTableViewCell : UITableViewCell
  {
    public static readonly NSString Key = new NSString("CurrencyTableViewCell");
    Currency currency;

    public Currency Currency
    {
      get
      {
        return currency;
      }
      set
      {
        currency = value;
        Refresh();
      }
    }

    public CurrencyTableViewCell() : base(UITableViewCellStyle.Value1, Key)
    {

    }

    void Refresh()
    {
      TextLabel.Text = Currency.Country;
      DetailTextLabel.Text = Currency.Symbol;
      DetailTextLabel.TextColor = UIColor.DarkGray;
      BackgroundColor = UIColor.Clear;
    }
  }
}

