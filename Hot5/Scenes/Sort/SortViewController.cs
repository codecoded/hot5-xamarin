using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using Hot5.Core;
using LeadedSky.Shared;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  partial class SortViewController : UIViewController
  {
  
    public TableRowSelectedDelegate HandleSortHotels { get; set; }

    public string SelectedSort { get; set; }

    public SortViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      View.SetBackgroundImage(AppConfig.ModalBackgroundImage);
      SortTable.Initialize(SortOptions.CreateOptionsList(SelectedSort), AppConfig.SEPERATOR_COLOR);
      SortTable.TableSelectionComplete = TableSelectionComplete;
      CancelItem.Clicked += (sender, e) => DismissViewController(true, null);
      NavigationController.Title = "Sort Hotels";
      Title = "Sort Hotels";
    }


    public void TableSelectionComplete(TableRow tableRow)
    {
      DismissViewController(true, null);
      if(HandleSortHotels != null)
        HandleSortHotels(tableRow);
    }

  }
}
