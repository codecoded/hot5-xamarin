// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("SortViewController")]
	partial class SortViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CancelItem { get; set; }

		[Outlet]
		LeadedSky.UI.iOS.SingleTableView SortTable { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
