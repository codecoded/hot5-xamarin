using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.IO;

using Hot5.Core;
using LeadedSky.Shared;
using System.Collections.Generic;
using System.Linq;

namespace Hot5
{
  partial class UISearchViewController : UIViewController
  {
    public UISearchTableViewSource TableSource { get; set; }

    public UserCache UserCache { get { return AppSession.UserCache; } }

    public string SearchTerm { get { return SearchText.Text; } }

    public UISearchTableViewSource.SearchResultSelectedDelegate SearchComplete { get; set; }

    public UISearchViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      DoHooks();
      LoadData(null);
    }

    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      SearchText.EdgeInsets = new UIEdgeInsets(0, 15, 0, 0);
      SearchText.BecomeFirstResponder();
    }


    void LoadData(SearchResponse autocompleteResponse)
    {
      SearchResponseView responseView;
      if(autocompleteResponse == null || string.IsNullOrEmpty(SearchTerm))
      {
        if(AppSession.HasLocationServices)
          responseView = SearchResponseView.Default(MyLocationSection());
        else
          responseView = new SearchResponseView();
        responseView.AddRecentSection(UserCache.RecentSearches);
      }
      else
      {
        var recents = UserCache.RecentSearches.Where(e => e.Detail.IndexOf(SearchTerm, StringComparison.InvariantCultureIgnoreCase) > -1).ToList();
        responseView = new SearchResponseView(autocompleteResponse, recents);
      }

      TableSource = new UISearchTableViewSource(responseView);
      TableSource.SearchTerm = SearchTerm;
      TableSource.SearchResultSelected = SearchResultSelected;

      SearchTableView.Source = TableSource;
      SearchTableView.ReloadData();

    }

    void SearchResultSelected(SearchResultView searchResultView)
    {
      SearchText.ResignFirstResponder();
      SearchComplete(searchResultView);
      UserCache.AddSearchAndSave(searchResultView);
      DismissViewController(true, null);
    }

    void DoSearch()
    {
      ApiClient.AutocompleteSearch(SearchTerm, response => {
        using (var pool = new NSAutoreleasePool())
        {
          pool.BeginInvokeOnMainThread(() => LoadData(response));
        }
      });
    }

    void DoHooks()
    {
      CancelButton.Clicked += delegate {
        DismissViewController(true, null);
      };

      SearchText.EditingChanged += delegate {
        DoSearch();
      };
    }

    List<SearchResultView> MyLocationSection()
    {
      return new List<SearchResultView> {
        new SearchResultView {
          Detail = "Current Location",
          SearchType = "System",
          Slug = "my-location",
          Title = "Current Location"
        }
      };
    }
  }
}
