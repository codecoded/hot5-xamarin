// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("SearchViewController")]
	partial class UISearchViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem CancelButton { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView SearchTableView { get; set; }

		[Outlet]
		Hot5.UIPaddedTextField SearchText { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
