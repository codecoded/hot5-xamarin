﻿using System;
using MonoTouch.UIKit;
using Hot5.Core;
using MonoTouch.Foundation;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

using LeadedSky.Shared;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class UISearchTableViewSource : UITableViewSource
  {
    public SearchResponseView TableViewData { get; set; }

    public string SearchTerm { get; set; }

    public SearchResultSelectedDelegate SearchResultSelected { get; set; }

    public delegate void SearchResultSelectedDelegate(SearchResultView searchResultView);

    public static NSString ReusableTableCellId = new NSString("SearchResultTableCell");
    public static NSString NoResultsTableCellId = new NSString("NoResultsFoundCell");
    public static NSString SearchSimpleCellId = new NSString("SearchSimpleCell");

    public UISearchTableViewSource(SearchResponseView tableViewData)
    {
      this.TableViewData = tableViewData;
    }

    public override int NumberOfSections(UITableView tableView)
    {
      return TableViewData.Sections.Count;
    }

    public override int RowsInSection(UITableView tableview, int sectionIndex)
    {
      var section = TableViewData.GetSectionAt(sectionIndex);
      if(section != null && section.Count == 0)
        return 1;
      return section != null ?section.Count : 0;
    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {
      var searchResult = FindSearchResult(indexPath);

      if(searchResult != null)
        return setCellFormatting(tableView, indexPath, searchResult);
      else
        return noResultsCellView(tableView, indexPath);

    }

    UITableViewCell setCellFormatting(UITableView tableView, NSIndexPath indexPath, SearchResultView searchResultView)
    {
      if(TableViewData.IsSimpleView(indexPath.Section))
        return setSimpleCellFormatting(tableView, indexPath, searchResultView);

      var cell = (UITableViewCell)tableView.DequeueReusableCell(ReusableTableCellId, indexPath);

      cell.TextLabel.Font = AppConfig.Font();
      cell.TextLabel.Text = searchResultView.Title;
      cell.TextLabel.Hidden = false;

      cell.TextLabel.HighlightText(SearchTerm, new UIStringAttributes { 
        Font = AppConfig.FontBold() 
      }, StringComparison.InvariantCultureIgnoreCase);
      cell.DetailTextLabel.Hidden = false;

      try
      {
        cell.DetailTextLabel.Text = searchResultView.Detail.Remove(0, searchResultView.Title.Length + 2);
      }
      catch
      {
        cell.DetailTextLabel.Text = searchResultView.Detail;

      }
      cell.DetailTextLabel.HighlightText(SearchTerm, new UIStringAttributes {
        Font = AppConfig.FontBold(cell.DetailTextLabel.Font.PointSize) 
      },
        StringComparison.InvariantCultureIgnoreCase);
      return cell;
    }

    UITableViewCell setSimpleCellFormatting(UITableView tableView, NSIndexPath indexPath, SearchResultView searchResultView)
    {
      var cell = (UITableViewCell)tableView.DequeueReusableCell(SearchSimpleCellId, indexPath);
      cell.TextLabel.Text = searchResultView.Title;
      cell.TextLabel.Font = AppConfig.Font();
      cell.TextLabel.HighlightText(SearchTerm, new UIStringAttributes { 
        Font = AppConfig.FontBold() 
      }, StringComparison.InvariantCultureIgnoreCase);
          
      return cell;
    }

    UITableViewCell noResultsCellView(UITableView tableView, NSIndexPath indexPath)
    {
      var cell = (UITableViewCell)tableView.DequeueReusableCell(NoResultsTableCellId, indexPath);
      cell.TextLabel.Text = string.Format("No results for \"{0}\"", SearchTerm);
      return cell;
    }

    public override string TitleForHeader(UITableView tableView, int sectionIndex)
    {
      var headerTitle = TableViewData.GetSearchTermAt(sectionIndex);
      if(headerTitle.Equals("system", StringComparison.InvariantCultureIgnoreCase))
        return null;
      return headerTitle.ToUpper();
    }

    public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, int section)
    {
      var header = (UITableViewHeaderFooterView)headerView;
      header.TextLabel.TextColor = AppConfig.PrimaryColor;
      header.ContentView.BackgroundColor = UIColor.White;
      header.Alpha = 0.9f;
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      var searchResult = FindSearchResult(indexPath);
      SearchResultSelected(searchResult);
      tableView.DeselectRow(indexPath, true); 
    }

    SearchResultView FindSearchResult(NSIndexPath indexPath)
    {
      var curSection = TableViewData.GetSectionAt(indexPath.Section);

      if(curSection == null || curSection.Count == 0)
        return null;

      return curSection[indexPath.Row];
    }
  }
}

