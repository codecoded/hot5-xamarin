﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;

namespace Hot5
{
  [Register("UIHotelImagesCollectionView")]
  public partial class UIHotelImagesCollectionView : UICollectionView, IPagingEventsListener
  {
    RectangleF startingFrame;

    public int CurrentVisiblePage
    {
      get{ return (int)Math.Round(ContentOffset.X / Frame.Width); }
    }

    int currentPage = 0;

    public int CurrentPage
    { 
      get { return currentPage; } 
      private set
      {
        if(currentPage == value)
          return;
        currentPage = value;
        GotoCurrentPage();
      }
    }

    public UIHotelImagesCollectionView(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelImagesCollectionView(RectangleF frame, UICollectionViewLayout layout) : base(frame, layout)
    {
      initialize();
    }

    void initialize()
    {
      UserInteractionEnabled = true;
      PagingEnabled = true;
      ClipsToBounds = true;
      ShowsHorizontalScrollIndicator = false;
      RegisterNibForCell(UIHotelImageCollectionCell.Nib, UIHotelImageCollectionCell.Key);
      ContentMode = UIViewContentMode.ScaleAspectFill;
      startingFrame = Frame;
      BackgroundColor = AppConfig.SecondayColor;
      DirectionalLockEnabled = true;
      Console.WriteLine("HotelImagesCollectionView Initialized");
    }

    public void CheckVisiblePage()
    {

      if(CurrentPage == CurrentVisiblePage)
        return;

      CurrentPage = CurrentVisiblePage;
      HotelEventCoordinator.PagingEventsCoordinator.NotifyPageChanged(this, CurrentPage);
    }

    public void HandlePageChanged(int newPage)
    {
      CurrentPage = newPage;
    }

    public void GotoCurrentPage()
    {
      var hotelPath = NSIndexPath.FromRowSection(currentPage, 0);
      ScrollToItem(hotelPath, UICollectionViewScrollPosition.Left, true);
    }

    public void ResizeFrameHeight(PointF offset)
    {

      if(offset == PointF.Empty)
        return;
      var height = newHeight(offset);
      if(height < startingFrame.Height)
      {
        if(Frame.Y == startingFrame.Y)
          return;
        height = startingFrame.Height;
      }
      var frame = Frame;
      frame.Height = height; 
      ResizeAndInvalidate(frame);
    }


    float newHeight(PointF offset)
    {
      return startingFrame.Height - offset.Y;
    }

    public void ResizeAndInvalidate(RectangleF frame)
    {
      if(frame.Height > 250)
        CollectionViewLayout.InvalidateLayout();

      Frame = frame;

    }

  }
}
  