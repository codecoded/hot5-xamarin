﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;

namespace Hot5
{
  [Register("UIHotelImagesPager")]
  public partial class UIHotelImagesPager : UIPageControl, IPagingEventsListener
  {
  
    public UIHotelImagesPager(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelImagesPager(RectangleF frame) : base(frame)
    {
      initialize();
    }

    public UIHotelImagesPager()
    {
      initialize();
    }

    void initialize()
    {

      UserInteractionEnabled = true;
      ValueChanged += pageChangedEvent;
      HidesForSinglePage = true;
      Console.WriteLine("HotelIamgesPager Initialized");
    }

    protected override void Dispose(bool disposing)
    {
      base.Dispose(disposing);
      "UIHotelImagesPager - Disposing".Log();
      ValueChanged -= pageChangedEvent;
    }

    void pageChangedEvent(object sender, EventArgs e)
    {
      HotelEventCoordinator.PagingEventsCoordinator.NotifyPageChanged(this, CurrentPage);
    }

    public void SetPagesAndRefit(int pageCount)
    {
      Pages = pageCount;
//      SizeToFit();
    }

    public void HandlePageChanged(int currentPage)
    {
      CurrentPage = currentPage;
      UpdateCurrentPageDisplay();
      Console.WriteLine(string.Format("Paging: {0}", CurrentPage));
    }
  }
}

