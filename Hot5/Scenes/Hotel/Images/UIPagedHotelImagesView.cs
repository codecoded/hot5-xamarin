﻿using System;
using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.ObjCRuntime;
using System.Collections.Generic;
using LeadedSky.UI.iOS;
using LeadedSky.Shared;

namespace Hot5
{
  [Register("UIPagedHotelImagesView")]
  public partial class UIPagedHotelImagesView : UIView,IScrollingEventsListener
  {

    public Hotel.HotelChangedDelegate HandleHotelChanged { get; set; }

    public UIHotelImagesCollectionView HotelImagesCollection;
    public UIHotelImagesPager ImagesPager;
    UIXHotelDetailsOverlay HotelDetailsOverlay;

    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public UIPagedHotelImagesView(IntPtr handle) : base(handle)
    {
    }

    public UIPagedHotelImagesView(RectangleF frame) : base(frame)
    {
      Initialize();
    }

    public UIPagedHotelImagesView()
    {
      Initialize();
    }

    public void Initialize()
    {
      HotelImagesCollection = new UIHotelImagesCollectionView(Frame, new UIHotelImagesFlowLayout(new SizeF(320, Frame.Height)));
      HotelImagesCollection.ContentMode = UIViewContentMode.ScaleAspectFill;

      ImagesPager = new UIHotelImagesPager(new RectangleF(0, 0, 320, 38));
      HotelDetailsOverlay = NibLoader.Create<UIXHotelDetailsOverlay>();


      Add(HotelImagesCollection);
      Add(ImagesPager);
      Add(HotelDetailsOverlay);
      BringSubviewToFront(HotelDetailsOverlay);
      SetupConstraints();
    }


    #region IScrollingEventsListener implementation

    public void Refresh()
    {
      HotelImagesCollection.Source = new UIHotelImagesCollectionSource(Hotel);
      HotelDetailsOverlay.Hotel = Hotel;
      ImagesPager.SetPagesAndRefit(Hotel.ImageCountLimited());
    }

    public void HandleScrolled(object sender)
    {
      UIScrollView scrollView = sender as UIScrollView;
      var offset = scrollView.ContentOffset;
      HotelImagesCollection.ResizeFrameHeight(offset);

      var offsetY = offset.Y;
      if(offsetY <= 0)
        HotelDetailsOverlay.Frame = new RectangleF(0, offsetY, 320, 150);
      else
        HotelDetailsOverlay.Frame = new RectangleF(0, 0, 320, 150);
    }

    #endregion

    public void SetupConstraints()
    {
      ImagesPager.TranslatesAutoresizingMaskIntoConstraints = false;

      List<NSLayoutConstraint> constraints = new List<NSLayoutConstraint>();

      constraints.Add(NSLayoutConstraint.Create(
        ImagesPager,
        NSLayoutAttribute.Bottom,
        NSLayoutRelation.GreaterThanOrEqual,
        HotelImagesCollection,
        NSLayoutAttribute.Bottom,
        1,
        0
      ));

      constraints.Add(NSLayoutConstraint.Create(
        ImagesPager,
        NSLayoutAttribute.CenterX,
        NSLayoutRelation.Equal,
        this,
        NSLayoutAttribute.CenterX,
        1,
        0
      ));


      AddConstraints(constraints.ToArray());
    }


  }
}

