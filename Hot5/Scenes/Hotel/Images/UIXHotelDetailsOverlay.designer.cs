// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelDetailsOverlay")]
	partial class UIXHotelDetailsOverlay
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView imgOverlay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblHotelAddress { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblHotelName { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblStarRating { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (imgOverlay != null) {
				imgOverlay.Dispose ();
				imgOverlay = null;
			}
			if (lblHotelAddress != null) {
				lblHotelAddress.Dispose ();
				lblHotelAddress = null;
			}
			if (lblHotelName != null) {
				lblHotelName.Dispose ();
				lblHotelName = null;
			}
			if (lblStarRating != null) {
				lblStarRating.Dispose ();
				lblStarRating = null;
			}
		}
	}
}
