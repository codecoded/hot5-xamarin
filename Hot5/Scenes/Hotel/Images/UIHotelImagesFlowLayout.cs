﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;

namespace Hot5
{
  [Register("HotelImagesFlowLayout")]
  public partial class UIHotelImagesFlowLayout : UICollectionViewFlowLayout
  {
    public UIHotelImagesFlowLayout(IntPtr h) : base(h)
    {
      Initialize();
      ItemSize = new SizeF(320, 250);
    }

    public UIHotelImagesFlowLayout(SizeF itemSize)
    {
      Initialize();
      ItemSize = itemSize;
    }

    public void Initialize()
    {
     
      FooterReferenceSize = SizeF.Empty;
      HeaderReferenceSize = SizeF.Empty;
      MinimumInteritemSpacing = 0f;
      MinimumLineSpacing = 0f;
      ScrollDirection = UICollectionViewScrollDirection.Horizontal;
    }
  }
}

