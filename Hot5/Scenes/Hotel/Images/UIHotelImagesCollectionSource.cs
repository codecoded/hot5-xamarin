﻿using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public class UIHotelImagesCollectionSource : UICollectionViewSource
  {
    public Hotel Hotel;


    public UIHotelImagesCollectionSource(Hotel hotel)
    {
      Hotel = hotel;

    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (UIHotelImageCollectionCell)collectionView.DequeueReusableCell(UIHotelImageCollectionCell.Key, indexPath);
      if(Hotel.Images.Count == 0)
        cell.SetPlaceholder();
      else
        cell.SetHotelImage(Hotel.Images[indexPath.Row]);

      cell.ContentView.Frame = cell.Bounds;
      cell.ContentView.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleBottomMargin;

      return cell;
    }

    [Export("collectionView:layout:sizeForItemAtIndexPath:")]
    public virtual SizeF SizeForItemAtIndexPath(UICollectionView collectionView, UICollectionViewLayout layout, NSIndexPath indexPath)
    {
      return new SizeF(UIScreen.MainScreen.Bounds.Width, collectionView.Frame.Height);
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return Hotel.ImageCountLimited();
    }

    public override void DecelerationEnded(UIScrollView scrollView)
    {
      var view = scrollView as UIHotelImagesCollectionView;
      view.CheckVisiblePage();
    }
  }
    
}

