﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using SDWebImage;
using LeadedSky.UI.iOS;
using Xamarin;

namespace Hot5
{
  public partial class UIHotelImageCollectionCell : UICollectionViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIHotelImageCollectionCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIHotelImageCollectionCell");

    UIActivityIndicatorView activityIndicator;
    UIImage Placeholder = UIImage.FromBundle("image_placeholder_small.png");

    public UIHotelImageCollectionCell(IntPtr handle) : base(handle)
    {
      createActivityIndicator();

    }
    //
    //    public void SetHotelImage(HotelImage hotelImage)
    //    {
    //      try
    //      {
    //        HotelImageView.SetImage(new NSUrl(hotelImage.Url));
    //      }
    //      catch
    //      {
    //
    //      }
    //    }

    public void SetPlaceholder()
    {
      HotelImageView.Image = Placeholder;
      activityIndicator.StopAnimating();
    }

    public void SetHotelImage(HotelImage hotelImage)
    {
      var imageUrl = hotelImage.Url;

      //activityIndicator = createActivityIndicator(Center);

 
      try
      {
        activityIndicator.StartAnimating();
        HotelImageView.SetImage(new NSUrl(imageUrl), null, SDWebImageOptions.ContinueInBackground, (UIImage image, NSError error, SDImageCacheType cache) => {

          if(image != null && cache == SDImageCacheType.None)
          {
            HotelImageView.Alpha = 0.0f;
            UIView.Animate(1, () => {
              HotelImageView.Alpha = 1.0f;
            });

          }

         
          activityIndicator.StopAnimating();
          if(error != null)
          {
            HotelImageView.Image = Placeholder;
            "Error loading image: {0}".Log(error);
          }
        });
      }
      catch(Exception ex)
      {
        Insights.Report(ex);
      }

    }

    void createActivityIndicator()
    {
      activityIndicator = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
      activityIndicator.HidesWhenStopped = true;
      activityIndicator.Hidden = false;
      activityIndicator.StartAnimating();
      activityIndicator.Center = ContentView.Center;
      ContentView.AddSubview(activityIndicator);
      // return activityIndicator;
    }

    public override void LayoutSubviews()
    {
      base.LayoutSubviews();

      var contentViewIsAutoresized = Frame.Size == ContentView.Frame.Size;

      if(!contentViewIsAutoresized)
      {
        var contentViewFrame = ContentView.Frame;
        contentViewFrame.Size = Frame.Size;
        ContentView.Frame = contentViewFrame;
      }

    }

  }
}

