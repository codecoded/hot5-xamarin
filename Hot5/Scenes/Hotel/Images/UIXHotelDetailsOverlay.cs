﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using LeadedSky.UI.iOS;
using System.Drawing;
using Hot5.Core;

namespace Hot5
{
  [Register("UIXHotelDetailsOverlay")]
  public partial class UIXHotelDetailsOverlay : UIView
  {
    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public UIXHotelDetailsOverlay(IntPtr pntr) : base(pntr)
    {

    }

    public UIXHotelDetailsOverlay(RectangleF frame) : base(frame)
    {
    }

    public UIXHotelDetailsOverlay()
    {
    }

    public override void AwakeFromNib()
    {
      base.AwakeFromNib();
      UserInteractionEnabled = false;
    }


    public void Refresh()
    {

      imgOverlay.Image = UIImage.FromBundle("HotelDetailsMask.png");
//      lblHotelAddress.Text = hotel.Address;
      SetAddressLabel();
      lblHotelName.Text = hotel.Name;
      lblStarRating.Text = hotel.DisplayStarRating;
    }

    void SetAddressLabel()
    {
      var s = string.Format("{0:0.#} mi - {1}", hotel.DistanceInMiles(), hotel.Address);
      lblHotelAddress.Text = s;
    }

    public override UIView HitTest(PointF point, UIEvent uievent)
    {

//      var view = base.HitTest(point, uievent);
//
//      foreach(UIView subview in Subviews)
//      {
//        if(view != null && view.UserInteractionEnabled)
//          break;
//        var newPoint = ConvertPointToView(point, subview);
//        view = subview.HitTest(newPoint, uievent);
//      }
      return null;
    }
  }
}

