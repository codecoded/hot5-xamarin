﻿using System;
using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.ObjCRuntime;
using System.Collections.Generic;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIHotelAboutView : UIView
  {
    public UIHotelDescriptionCollectionView HotelDescriptionCollection;
    UIHotelDescriptionPager HotelDescriptionPager;
    UIHotelAmenitiesCollectionView HotelAmenitiesView;
    UIHotelRatingCollectionView HotelRatingCollection;

    static NSNotificationCenter EventsCoordinator { get { return NSNotificationCenter.DefaultCenter; } }

    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public UIHotelAboutView(IntPtr handle) : base(handle)
    {
    }

    public UIHotelAboutView(RectangleF frame) : base(frame)
    {
      initialize();
    }

    public UIHotelAboutView()
    {
      initialize();
    }

    public void ChangePage(NSNotification notice)
    {
      var pageNo = (notice.Object as NSNumber).IntValue;

      HotelDescriptionCollection.CurrentPage = pageNo;
      HotelDescriptionPager.CurrentPage = pageNo;
    }

    void initialize()
    {
      HotelDescriptionCollection = new UIHotelDescriptionCollectionView(new RectangleF(PointF.Empty, new SizeF(320, 160)), new UIHotelImagesFlowLayout(new SizeF(320, 150)));
      HotelDescriptionCollection.ContentMode = UIViewContentMode.ScaleAspectFill;

      HotelRatingCollection = new UIHotelRatingCollectionView(new RectangleF(PointF.Empty, new SizeF(320, 160)), new UIHotelImagesFlowLayout(new SizeF(120, 120)));


      HotelAmenitiesView = new UIHotelAmenitiesCollectionView(new RectangleF(0, 160, 320, Frame.Height - 188), new UIHotelAmenitiesFlowLayout(new Size(160, 35)));
      BackgroundColor = UIColor.White;

      Add(HotelDescriptionCollection);
      if(Hotel.ShowRating)
        Add(HotelRatingCollection);

      Add(HotelAmenitiesView);

      HotelDescriptionPager = new UIHotelDescriptionPager(new RectangleF(0, 150, 320, 38));
      Add(HotelDescriptionPager);
//      EventsCoordinator.AddObserver(XIBTestController.DESCRIPTION_PAGED_EVENT_ID, ChangePage);

    }

    void Refresh()
    {
      HotelDescriptionCollection.Source = new UIHotelDescriptionCollectionSource(Hotel);
      HotelDescriptionCollection.ReloadData();
      HotelAmenitiesView.LoadSource(Hotel);
      HotelDescriptionPager.SetPagesAndRefit(hotel.ImageCountLimited());
    }

  }
}

