﻿using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Linq;
using System.Collections.Generic;

namespace Hot5
{
  public class UIHotelRatingCollectionSource : UICollectionViewSource
  {
    public Hotel Hotel;

    public List<HotelRating> Ratings;

    public UIHotelRatingCollectionSource(Hotel hotel)
    {
      Hotel = hotel;
      Ratings = Hotel.Ratings.ToList();
    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelRatingCell)collectionView.DequeueReusableCell(UIXHotelRatingCell.Key, indexPath);
      var rating = Ratings[indexPath.Row];

      cell.HotelRating = new HotelRating(rating.Provider, rating.Rating);
      return cell;
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return Ratings.Count;
    }

   
  }

}

