// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelRatingCell")]
	partial class UIXHotelRatingCell
	{
		[Outlet]
		MonoTouch.UIKit.UIImageView ImgProvider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel LblRating { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ImgProvider != null) {
				ImgProvider.Dispose ();
				ImgProvider = null;
			}
			if (LblRating != null) {
				LblRating.Dispose ();
				LblRating = null;
			}
		}
	}
}
