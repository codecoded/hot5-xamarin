﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIHotelRatingCollectionView : UICollectionView
  {
  

    public UIHotelRatingCollectionView(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelRatingCollectionView(RectangleF frame, UICollectionViewLayout layout) : base(frame, layout)
    {
      initialize();
    }

    void initialize()
    {
      UserInteractionEnabled = true;
      PagingEnabled = false;
      ClipsToBounds = true;
      ShowsHorizontalScrollIndicator = false;
      RegisterNibForCell(UIXHotelRatingCell.Nib, UIXHotelRatingCell.Key);
      BackgroundColor = UIColor.White;
      Console.WriteLine("UIHotelRatingCollectionView Initialized");
    }
  }
}

