﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Hot5
{
  public class UIXRatingCollectionCell : UICollectionViewCell
  {
    public static readonly NSString Key = new NSString("UIXRatingCollectionCell");

    [Export("initWithFrame:")]
    public UIXRatingCollectionCell(RectangleF frame) : base(frame)
    {
      // TODO: add subviews to the ContentView, set various colors, etc.
      BackgroundColor = UIColor.Cyan;
    }
  }
}

