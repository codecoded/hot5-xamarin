﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using Hot5.Core;

namespace Hot5
{
  public class UIRatingView : UIView
  {
    HotelRating HotelRating;

    public UIRatingView(HotelRating hotelRating, RectangleF frame) : base(frame)
    {
      BackgroundColor = UIColor.White;
      HotelRating = hotelRating;
      //AddRating();
    }

    void AddRating()
    {
      var lbl = new UILabel(new RectangleF(0, 0, Frame.Width, 20));
      lbl.TextAlignment = UITextAlignment.Center;
      lbl.Center = Center;
      lbl.Font = AppConfig.Font(23f);
      lbl.TextColor = AppConfig.SecondayColor;
      lbl.Text = string.Format("{0}%", HotelRating.Rating);
      Add(lbl);
    }

    public override void Draw(RectangleF rect)
    {
      base.Draw(rect);
      DrawCircle();
      //DrawText(rect);
    }

    void DrawCircle()
    { 

      CGContext ctx = UIGraphics.GetCurrentContext();
      ctx.BeginPath();
      ctx.SetStrokeColor(AppConfig.PrimaryColor.ColorWithAlpha(
        HotelRating.IsOverall ?1f : 0.3f).CGColor);
      ctx.SetShouldAntialias(true);
      ctx.StrokeEllipseInRect(CenteredRect((Frame.Height - 5) / 2));
      ctx.SetLineWidth(0.5f);
      ctx.FillPath();
      //DrawSpot();
    }

    void DrawSpot()
    { 

      CGContext ctx = UIGraphics.GetCurrentContext();
      ctx.BeginPath();
      ctx.AddEllipseInRect(CenteredRect(5f));
      ctx.SetFillColor(UIColor.Red.CGColor);
      ctx.FillPath();
    }

    RectangleF CenteredRect(float radius)
    {
      var x = Center.X - radius;
      var y = Center.Y - radius;
      return new RectangleF(x, y, radius * 2, radius * 2);
    }
    //    - (void) drawString: (NSString*) s
    //    withFont: (UIFont*) font
    //    inRect: (CGRect) contextRect {
    //
    //      /// Make a copy of the default paragraph style
    //      /// Set line break mode
    //      paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    //      /// Set text alignment
    //      paragraphStyle.alignment = NSTextAlignmentCenter;
    //
    //      NSDictionary *attributes = @{ NSFontAttributeName: font,
    //        NSForegroundColorAttributeName: [UIColor whiteColor],
    //        NSParagraphStyleAttributeName: paragraphStyle };
    //
    //      CGSize size = [s sizeWithAttributes:attributes];
    //
    //
    //
    //      [s drawInRect:textRect withAttributes:attributes];

  }
}

