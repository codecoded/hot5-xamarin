﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;

namespace Hot5
{
  public partial class UIXHotelRatingCell : UICollectionViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelRatingCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelRatingCell");
    UILabel lblOverall;

    HotelRating hotelRating;

    public HotelRating HotelRating
    {
      get
      {
        return hotelRating;
      }
      set
      {
        hotelRating = value;
        update();
      }
    }


    public UIXHotelRatingCell(IntPtr handle) : base(handle)
    {
    }

    public static UIXHotelRatingCell Create()
    {
      return (UIXHotelRatingCell)Nib.Instantiate(null, null)[0];
    }


    void update()
    {
      //LblRating.Hidden = true;
      LblRating.Text = string.Format("{0}%", HotelRating.Rating);
      if(lblOverall == null)
      {
        lblOverall = new UILabel(new RectangleF(0, Frame.Height - 27, Frame.Width, 20));
        lblOverall.Text = "Overall Rating";
        lblOverall.Font = AppConfig.Font(14);
        lblOverall.TextAlignment = UITextAlignment.Center;
        lblOverall.TextColor = AppConfig.SecondayColor;
        Add(lblOverall);
      }
      //LblRating.SizeToFit();
      //LblRating.Center = new PointF(40, 40);

      Add(CreateRatingView());

      if(HotelRating.IsOverall)
      {
        ImgProvider.Hidden = true;
        lblOverall.Hidden = false;
      }
      else
      {
        ImgProvider.Hidden = false;
        lblOverall.Hidden = true;

        ImgProvider.Image = UIImage.FromBundle(string.Format("{0}_small.png", HotelRating.Provider.ToLower()));
      }

//      lblProvider.Text = hotelProvider.EnglishProviderName();
    }

    UIRatingView CreateRatingView()
    {
      var view = new UIRatingView(HotelRating, new RectangleF(0, 0, Frame.Width, 80));
      //view.Center = this.Center;
      view.Layer.ZPosition = -1;
      return view;
    }

  }
}

