﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using Hot5.Core;
using LeadedSky.UI.iOS;
using PubSub;
using MonoTouch.Foundation;
using MonoTouch.CoreAnimation;

namespace Hot5
{
  public class UiHotelAboutTableSource : UITableViewSource
  {
    public UIHotelDescriptionCollectionView HotelDescriptionCollection;
    //UIHotelDescriptionPager HotelDescriptionPager;
    //    UIHotelAmenitiesCollectionView HotelAmenitiesView;
    UIHotelRatingCollectionView HotelRatingCollection;


    const float lblHeight = 60f;
    const string cellIdentifier = "AboutTableCell";

    public Hotel Hotel;
    bool DescriptionExpanded = false;

    float DescriptionHeight = 120f;


    public UiHotelAboutTableSource(Hotel hotel)
    {
      Hotel = hotel;
    }


    public override int NumberOfSections(UITableView tableView)
    {
      // NOTE: Don't call the base implementation on a Model class
      // see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
      return 1;
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      if(indexPath.Row != 0)
        return;

      DescriptionSelected(tableView as UIHotelAboutTable, indexPath);


    }

    void DescriptionSelected(UIHotelAboutTable tableView, NSIndexPath indexPath)
    {
      var cell = tableView.CellAt(indexPath) as UIXHotelDescriptionTableCell;
      DescriptionExpanded = !DescriptionExpanded;
      DescriptionHeight = DescriptionExpanded ?cell.DescriptionHeight : 120f;
     
      CATransaction.Begin();
      CATransaction.CompletionBlock = () => InvokeOnMainThread(() => this.Publish<UIHotelAboutTable>(tableView));
      tableView.BeginUpdates();
      tableView.ReloadRows(new []{ indexPath }, UITableViewRowAnimation.Fade);
      tableView.EndUpdates();
      CATransaction.Commit();

      //cell.SetDescriptionHeight(DescriptionExpanded);
    }

    public override int RowsInSection(UITableView tableview, int section)
    {
      return 3;
    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {

      UITableViewCell cell;

      switch (indexPath.Row)
      {
      case 0:
        cell = tableView.DequeueReusableCell(UIXHotelDescriptionTableCell.Key);
        return AddDescription((UIXHotelDescriptionTableCell)cell);
      case 1:
        cell = tableView.DequeueReusableCell(cellIdentifier) ?? new UITableViewCell(UITableViewCellStyle.Default, cellIdentifier);
        return AddRatings(cell);
      case 2:
        cell = tableView.DequeueReusableCell(UIAboutAmenitiesCell.Key) ?? new UIAboutAmenitiesCell();
        return  AddAmenities((UIAboutAmenitiesCell)cell);
      default:
//        cell.Hidden = true;
        return null;
      }
    }

    UITableViewCell AddDescription(UIXHotelDescriptionTableCell cell)
    {
      cell.Hotel = Hotel;
      cell.SetDescriptionHeight(DescriptionExpanded);
      return cell;
    }
      
    //    UITableViewCell AddDescription(UITableViewCell cell)
    //    {
    //      HotelDescriptionCollection = new UIHotelDescriptionCollectionView(new RectangleF(PointF.Empty, new SizeF(UIScreen.MainScreen.Bounds.Width, DescriptionHeight)),
    //        new UIHotelImagesFlowLayout(new SizeF(320, DescriptionHeight)));
    //      HotelDescriptionCollection.ContentMode = UIViewContentMode.ScaleAspectFill;
    //      HotelDescriptionCollection.Source = new UIHotelDescriptionCollectionSource(Hotel);
    //      HotelDescriptionCollection.ReloadData();
    //
    //      if(TapGesture != null)
    //        cell.RemoveGestureRecognizer(TapGesture);
    //      //      TapGesture = new UITapGestureRecognizer();
    //      //      cell.AddGestureRecognizer(TapGesture);
    //      //      TapGesture.AddTarget(ExpandRow);
    //
    //      cell.SelectionStyle = UITableViewCellSelectionStyle.None;
    //
    //      cell.Add(HotelDescriptionCollection);
    //
    //      return cell;
    //    }

   
    UITableViewCell AddRatings(UITableViewCell cell)
    {
      if(!Hotel.ShowRating)
      {
        cell.Hidden = true;
        return cell;
      }

      cell.Add(SectionTitle("Hotel Ratings"));
      cell.SelectionStyle = UITableViewCellSelectionStyle.None;
      var ratingSize = new SizeF(110f, 110f);
      HotelRatingCollection = new UIHotelRatingCollectionView(
        new RectangleF(0, lblHeight, UIScreen.MainScreen.Bounds.Width, ratingSize.Width), 
        new UIHotelImagesFlowLayout(ratingSize));
      HotelRatingCollection.ContentMode = UIViewContentMode.ScaleAspectFill;
      HotelRatingCollection.Source = new UIHotelRatingCollectionSource(Hotel);
      HotelRatingCollection.ReloadData();

      cell.Add(HotelRatingCollection);
      return cell;

    }


    UITableViewCell AddAmenities(UIAboutAmenitiesCell cell)
    {
      cell.Hotel = Hotel;

      return cell;
//      var amenitiesLayout = new UIHotelAmenitiesFlowLayout(new Size(160, 35));
//      HotelAmenitiesView = new UIHotelAmenitiesCollectionView(
//        new RectangleF(0, lblHeight, 320, AmenityHeight()), amenitiesLayout);
//      HotelAmenitiesView.LoadSource(Hotel);
//
//      cell.SelectionStyle = UITableViewCellSelectionStyle.None;
//
//      cell.Add(HotelAmenitiesView);
//      cell.Add(SectionTitle("Hotel Amenities"));
//
//      return cell;

    }

     

    public override float GetHeightForHeader(UITableView tableView, int section)
    {
      return 0;
    }

    public override float GetHeightForFooter(UITableView tableView, int section)
    {
      return 0;
    }


    public override float GetHeightForRow(UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
    {
      switch (indexPath.Row)
      {
      case 0:
        return DescriptionHeight;
      case 1:
        return 120 + lblHeight;
      case 2:
        //return 500;
        return AmenityHeight() + lblHeight;
      default:
        return 160f;
      }
    }


    float AmenityHeight()
    {
      var height = (float)(Math.Ceiling(Hotel.AmenitiesList().Count / 2.0)) * 70f;
      return height;
    
    }

    UILabel SectionTitle(string title)
    {
      var lblTitle = new UILabel(new RectangleF(10, 0, 160, lblHeight));
      lblTitle.Font = AppConfig.FontBold(12);
      lblTitle.Text = title;     
      lblTitle.TextColor = AppConfig.SecondayColor;
      lblTitle.BackgroundColor = UIColor.Clear;
      return  lblTitle;
    }
       
  }
}

