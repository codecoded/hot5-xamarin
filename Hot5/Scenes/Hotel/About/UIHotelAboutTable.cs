﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace Hot5
{
  public class UIHotelAboutTable : UITableView
  {
    public UIHotelAboutTable(RectangleF frame) : base(frame, UITableViewStyle.Plain)
    {
      Initialize();
    }

    void Initialize()
    {
      SectionFooterHeight = 0;
      SectionHeaderHeight = 0;
      SeparatorStyle = UITableViewCellSeparatorStyle.None;
      ScrollEnabled = false;
      ClipsToBounds = true;
      Layer.MasksToBounds = false;    
      RegisterNibForCellReuse(UIXHotelDescriptionTableCell.Nib, UIXHotelDescriptionTableCell.Key);
      UserInteractionEnabled = true;
    }


    public SizeF CurrentHSize()
    {
      return  SizeThatFits(new SizeF(Frame.Size.Width, int.MaxValue));
    }
  }
}

