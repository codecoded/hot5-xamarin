// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelDescriptionCell")]
	partial class UIXHotelDescriptionCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblMore { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblProvider { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}
			if (lblMore != null) {
				lblMore.Dispose ();
				lblMore = null;
			}
			if (lblProvider != null) {
				lblProvider.Dispose ();
				lblProvider = null;
			}
		}
	}
}
