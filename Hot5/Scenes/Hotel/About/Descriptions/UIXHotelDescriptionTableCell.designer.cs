// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelDescriptionTableCell")]
	partial class UIXHotelDescriptionTableCell
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblDescription { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblProvider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblReadMore { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}
			if (lblProvider != null) {
				lblProvider.Dispose ();
				lblProvider = null;
			}
			if (lblReadMore != null) {
				lblReadMore.Dispose ();
				lblReadMore = null;
			}
		}
	}
}
