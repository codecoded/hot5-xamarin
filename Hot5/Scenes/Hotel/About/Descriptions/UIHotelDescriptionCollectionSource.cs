﻿using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Linq;

namespace Hot5
{
  public class UIHotelDescriptionCollectionSource : UICollectionViewSource
  {
    public Hotel Hotel;

    public bool NoDescriptions { get { return Hotel.Providers == null || Hotel.Providers.Count(e => !string.IsNullOrEmpty(e.Description)) == 0; } }

    public UIHotelDescriptionCollectionSource(Hotel hotel)
    {
      Hotel = hotel;

    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelDescriptionCell)collectionView.DequeueReusableCell(UIXHotelDescriptionCell.Key, indexPath);
      if(!NoDescriptions)
        cell.HotelProvider = Hotel.Providers[indexPath.Row];
      else
        cell.HotelProvider = new HotelProvider {
          Description = Hotel.Description,
          Provider = ""
        };
      return cell;
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return NoDescriptions ?1 : Hotel.Providers.Count;
    }

    public override void DecelerationEnded(UIScrollView scrollView)
    {
      var view = scrollView as UIHotelDescriptionCollectionView;
      view.CheckVisiblePage();
    }
  }
    
}

