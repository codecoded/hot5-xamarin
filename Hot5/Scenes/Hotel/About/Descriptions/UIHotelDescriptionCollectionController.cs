﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using System.Linq;

namespace Hot5
{
  public class UIHotelDescriptionCollectionController : UICollectionViewController
  {

    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        CollectionView.ReloadData();
      }
    }

    public bool NoDescriptions { get { return Hotel.Providers == null || Hotel.Providers.Count(e => !string.IsNullOrEmpty(e.Description)) == 0; } }

    public int CurrentVisiblePage
    {
      get{ return (int)Math.Round(CollectionView.ContentOffset.X / CollectionView.Frame.Width); }
    }

    int currentPage = 0;

    public int CurrentPage
    { 
      get { return currentPage; } 
      set
      {
        if(currentPage == value)
          return;
        currentPage = value;
        GotoCurrentPage();
      }
    }

    public UIHotelDescriptionCollectionController(UICollectionViewLayout layout) : base(layout)
    {
    }

    public override void DidReceiveMemoryWarning()
    {
      // Releases the view if it doesn't have a superview.
      base.DidReceiveMemoryWarning();
      
      // Release any cached data, images, etc that aren't in use.
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      
      // Register any custom UICollectionViewCell classes
      //CollectionView.RegisterClassForCell(typeof(UIFilmstripCell), UIFilmstripCell.Key);
      // Note: If you use one of the Collection View Cell templates to create a new cell type,
      // you can register it using the RegisterNibForCell() method like this:
      //
      // CollectionView.RegisterNibForCell (MyCollectionViewCell.Nib, MyCollectionViewCell.Key);
      CollectionView.RegisterNibForCell(UIXHotelDescriptionCell.Nib, UIXHotelDescriptionCell.Key);

      CollectionView.UserInteractionEnabled = true;
      CollectionView.PagingEnabled = true;
      CollectionView.ClipsToBounds = true;
      CollectionView.ShowsHorizontalScrollIndicator = false;
      CollectionView.BackgroundColor = UIColor.White;
      Console.WriteLine("UIFilmstripController Initialized");

    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      // TODO: return the actual number of sections
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return NoDescriptions ?1 : Hotel.Providers.Count;

    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelDescriptionCell)collectionView.DequeueReusableCell(UIXHotelDescriptionCell.Key, indexPath);
      if(!NoDescriptions)
        cell.HotelProvider = Hotel.Providers[indexPath.Row];
      else
        cell.HotelProvider = new HotelProvider {
          Description = Hotel.Description,
          Provider = ""
        };
      return cell;
    }

    public void CheckVisiblePage()
    {
      if(CurrentPage == CurrentVisiblePage)
        return;
      CurrentPage = CurrentVisiblePage;

      NSNotificationCenter.DefaultCenter.PostNotificationName(HotelEventCoordinator.DESCRIPTION_PAGED_EVENT_ID, new NSNumber(currentPage));
    }
    //
    //    public void HandlePageChanged(NSNotification notification)
    //    {
    //      if (notification == null) return;
    //      notification.Object
    //      CurrentPage = newPage;
    //    }

    public void GotoCurrentPage()
    {
      var hotelPath = NSIndexPath.FromRowSection(currentPage, 0);
      CollectionView.ScrollToItem(hotelPath, UICollectionViewScrollPosition.Left, true);
    }

  }
}

