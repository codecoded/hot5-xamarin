﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using PubSub;

namespace Hot5
{
  public partial class UIXHotelDescriptionCell : UICollectionViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelDescriptionCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelDescriptionCell");


    HotelProvider hotelProvider;

    public HotelProvider HotelProvider
    {
      get
      {
        return hotelProvider;
      }
      set
      {
        hotelProvider = value;
        update();
      }
    }

    public UIXHotelDescriptionCell(IntPtr handle) : base(handle)
    {
    }

    public static UIXHotelDescriptionCell Create()
    {
      return (UIXHotelDescriptionCell)Nib.Instantiate(null, null)[0];
    }

    void update()
    {

      lblDescription.Text = hotelProvider.Description;
      lblDescription.Lines = 0;
//      lblDescription.SizeToFit();
      lblProvider.Text = hotelProvider.EnglishProviderName();
     
    }

  }
}

