﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIXHotelDescriptionTableCell : UITableViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelDescriptionTableCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelDescriptionTableCell");

    Hotel hotel;

    public UILabel Description { get { return lblDescription; } }

    public float DescriptionHeight;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        update();
      }
    }

    public UIXHotelDescriptionTableCell(IntPtr handle) : base(handle)
    {
    }

    public static UIXHotelDescriptionTableCell Create()
    {
      return (UIXHotelDescriptionTableCell)Nib.Instantiate(null, null)[0];
    }

    void update()
    {
      lblProvider.Hidden = true;
      SelectionStyle = UITableViewCellSelectionStyle.None;
      lblDescription.Text = hotel.Description;
      DescriptionHeight = lblDescription.SizeThatFits(Frame.Size).Height + 40f;
      //lblProvider.Text = hotelProvider.EnglishProviderName();

    }

    public void SetDescriptionHeight(bool expanded)
    {
      var height = expanded ?DescriptionHeight - 40 : 80;
      var topPos = expanded ?height + 20 : 100;

      Description.AdjustFrame(height: height);
      lblProvider.AdjustFrame(y: topPos);
      lblReadMore.Hidden = expanded;
    }
            
  }
}

