﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIHotelDescriptionCollectionView : UICollectionView
  {

    public int CurrentVisiblePage
    {
      get{ return (int)Math.Round(ContentOffset.X / Frame.Width); }
    }

    int currentPage = 0;

    public int CurrentPage
    { 
      get { return currentPage; } 
      set
      {
        if(currentPage == value)
          return;
        currentPage = value;
        GotoCurrentPage();
      }
    }

    public UIHotelDescriptionCollectionView(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelDescriptionCollectionView(RectangleF frame, UICollectionViewLayout layout) : base(frame, layout)
    {
      initialize();
    }

    void initialize()
    {
      UserInteractionEnabled = true;
      PagingEnabled = true;
      ClipsToBounds = true;
      ShowsHorizontalScrollIndicator = false;
      RegisterNibForCell(UIXHotelDescriptionCell.Nib, UIXHotelDescriptionCell.Key);
      BackgroundColor = UIColor.White;
      Console.WriteLine("UIHotelDescriptionCollectionView Initialized");
    }

    public void CheckVisiblePage()
    {
      if(CurrentPage == CurrentVisiblePage)
        return;
      CurrentPage = CurrentVisiblePage;

      NSNotificationCenter.DefaultCenter.PostNotificationName(HotelEventCoordinator.DESCRIPTION_PAGED_EVENT_ID, new NSNumber(currentPage));
    }
    //
    //    public void HandlePageChanged(NSNotification notification)
    //    {
    //      if (notification == null) return;
    //      notification.Object
    //      CurrentPage = newPage;
    //    }

    public void GotoCurrentPage()
    {
      var hotelPath = NSIndexPath.FromRowSection(currentPage, 0);
      ScrollToItem(hotelPath, UICollectionViewScrollPosition.Left, true);
    }

  }
}

