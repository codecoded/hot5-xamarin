﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;

namespace Hot5
{
  public partial class UIHotelDescriptionPager : UIPageControl
  {

    public UIHotelDescriptionPager(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIHotelDescriptionPager(RectangleF frame) : base(frame)
    {
      initialize();
    }

    public UIHotelDescriptionPager()
    {
      initialize();
    }

    void initialize()
    {
      UserInteractionEnabled = true;
      ValueChanged += (sender, e) => NSNotificationCenter.DefaultCenter.PostNotificationName(
        HotelEventCoordinator.DESCRIPTION_PAGED_EVENT_ID, new NSNumber(CurrentPage));

      Console.WriteLine("HotelDescriptionPager Initialized");
    }

    public void SetPagesAndRefit(int pageCount)
    {
      Pages = pageCount;
//      SizeToFit();
    }

    public void HandlePageChanged(int currentPage)
    {
      CurrentPage = currentPage;
      UpdateCurrentPageDisplay();
      Console.WriteLine(string.Format("Paging: {0}", CurrentPage));
    }
  }
}

