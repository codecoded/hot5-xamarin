﻿using Hot5.Core;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Collections.Generic;

namespace Hot5
{
  public class UIHotelAmenitiesCollectionSource : UICollectionViewSource
  {
    public List<HotelAmenity> HotelAmenities;


    public UIHotelAmenitiesCollectionSource(List<HotelAmenity> hotelAmenities)
    {
      HotelAmenities = hotelAmenities;

    }

    public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelAmenityCollectionCell)collectionView.DequeueReusableCell(UIXHotelAmenityCollectionCell.Key, indexPath);
      cell.HotelAmenity = HotelAmenities[indexPath.Row];
      return cell;
    }

    public override int NumberOfSections(UICollectionView collectionView)
    {
      return 1;
    }

    public override int GetItemsCount(UICollectionView collectionView, int section)
    {
      return HotelAmenities.Count;
    }
      
    //    public override void DecelerationEnded(UIScrollView scrollView)
    //    {
    //      var view = scrollView as UIHotelDescriptionCollectionView;
    //      view.CheckVisiblePage();
    //    }

  }

}

