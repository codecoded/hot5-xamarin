﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using PubSub;

using LeadedSky.UI.iOS;

namespace Hot5
{
  public partial class UIAboutAmenitiesCell : UITableViewCell
  {
    public static readonly NSString Key = new NSString("UIAboutAmenitiesCell");

    UIHotelAmenitiesCollectionView HotelAmenitiesView;


    Hotel hotel;


    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        update();
      }
    }


    public UIAboutAmenitiesCell() : base(UITableViewCellStyle.Default, Key)
    {

    }



    void update()
    {
      var amenitiesLayout = new UIHotelAmenitiesFlowLayout(new Size(160, 35));
      HotelAmenitiesView = new UIHotelAmenitiesCollectionView(
        new RectangleF(0, 60f, 320, AmenityHeight()), amenitiesLayout);
      HotelAmenitiesView.LoadSource(Hotel);

      SelectionStyle = UITableViewCellSelectionStyle.None;

      Add(HotelAmenitiesView);
      Add(SectionTitle("Hotel Amenities"));

    }


    float AmenityHeight()
    {
      var height = (float)(Math.Ceiling(Hotel.AmenitiesList().Count / 2.0)) * 70f;
      return height;

    }

    UILabel SectionTitle(string title)
    {
      var lblTitle = new UILabel(new RectangleF(10, 0, 160, 60f));
      lblTitle.Font = AppConfig.FontBold(12);
      lblTitle.Text = title;     
      lblTitle.TextColor = AppConfig.SecondayColor;
      lblTitle.BackgroundColor = UIColor.Clear;
      return  lblTitle;
    }

  }
}

