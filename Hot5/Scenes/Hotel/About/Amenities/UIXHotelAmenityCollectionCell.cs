﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;

namespace Hot5
{
  public partial class UIXHotelAmenityCollectionCell : UICollectionViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelAmenityCollectionCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelAmenityCollectionCell");

    public HotelAmenity hotelAmenity;

    public HotelAmenity HotelAmenity
    {
      get
      {
        return hotelAmenity;
      }
      set
      {
        if(value == hotelAmenity)
          return;
        hotelAmenity = value;
        UpdateCell();
      }
    }

    public UIXHotelAmenityCollectionCell(IntPtr handle) : base(handle)
    {
    }

    public static UIXHotelAmenityCollectionCell Create()
    {
      return (UIXHotelAmenityCollectionCell)Nib.Instantiate(null, null)[0];
    }

    void UpdateCell()
    {
      lblDescription.TextColor = AppConfig.SecondayColor;
      lblDescription.Text = HotelAmenity.Description;


      lblIcon.Image = UIImage.FromBundle(string.Format("amenities/{0}_colored.png", HotelAmenity.Flag.ToString().ToLower()));
    }
      
  }
}

