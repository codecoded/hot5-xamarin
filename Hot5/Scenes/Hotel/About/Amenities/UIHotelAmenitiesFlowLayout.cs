﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;

namespace Hot5
{
  public partial class UIHotelAmenitiesFlowLayout : UICollectionViewFlowLayout
  {
    public UIHotelAmenitiesFlowLayout(IntPtr h) : base(h)
    {
      Initialize();
      ItemSize = new SizeF(160, 35);
    }

    public UIHotelAmenitiesFlowLayout(SizeF itemSize)
    {
      Initialize();
      ItemSize = itemSize;
    }

    public void Initialize()
    {
      FooterReferenceSize = SizeF.Empty;
      HeaderReferenceSize = SizeF.Empty;
      MinimumInteritemSpacing = 0f;
      MinimumLineSpacing = 10f;
      ScrollDirection = UICollectionViewScrollDirection.Vertical;
    }
  }
}

