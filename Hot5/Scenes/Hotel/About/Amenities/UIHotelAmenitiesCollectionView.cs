﻿using System;
using Hot5.Core;
using MonoTouch.UIKit;
using System.Drawing;

namespace Hot5
{
  public class UIHotelAmenitiesCollectionView : UICollectionView
  {
    public UIHotelAmenitiesCollectionView(IntPtr h) : base(h)
    {
      Initialize();
    }

    public UIHotelAmenitiesCollectionView(RectangleF frame, UICollectionViewLayout layout) : base(frame, layout)
    {
      Initialize();
    }

    void Initialize()
    {
      BackgroundColor = UIColor.White;
      UserInteractionEnabled = false;
      PagingEnabled = false;
      ClipsToBounds = true;
      ShowsHorizontalScrollIndicator = false;
      ScrollEnabled = false;
      RegisterNibForCell(UIXHotelAmenityCollectionCell.Nib, UIXHotelAmenityCollectionCell.Key);
      Console.WriteLine("UIHotelAmenitiesCollectionView Initialized");
    }

    public void LoadSource(Hotel hotel)
    {
      var amenities = HotelAmenity.ConvertToList(hotel.Amenities.GetValueOrDefault());
      Source = new UIHotelAmenitiesCollectionSource(amenities);
      ReloadData();
//      Frame = new RectangleF(Frame.Location, new SizeF(Frame.Width, amenities.Count / 2 * 45));
    }

  }
}

