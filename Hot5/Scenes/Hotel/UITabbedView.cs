﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using LeadedSky.UI.iOS;
using Hot5.Core;
using LeadedSky.Shared;
using MonoTouch.Foundation;

namespace Hot5
{

  public class UITabbedView : UIView, IScrollingEventsListener
  {
    const float TabsHeight = 50;

    UIView currentView;
    public HotelTabsPanel Tabs;
    public UIHotelRoomsTableView HotelRoomsTable;
    public UIHotelMapView HotelMap;
    public UIHotelAboutTable HotelAboutTable;

    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public static float SCREEN_WIDTH = UIScreen.MainScreen.Bounds.Width;

    public bool ScrollableContent
    {

      get
      {
        return Tabs.ScrollableContent;
      }

    }

    public SizeF ContentSize
    {
      get
      { 
        if(currentView == null)
          return SizeF.Empty;
        var uIScrollView = currentView as UIScrollView;

        var size = uIScrollView != null ?uIScrollView.ContentSize : currentView.Frame.Size;
        "ContentSize={0}".Log(size);
        return size;
      }
    }

    public UITabbedView(RectangleF frame) : base(frame)
    {
      Initialize();
    }

    public UITabbedView()
    {
      Initialize();
    }

    public void Initialize()
    {
      addTabs();   
      addRoomsTable();
      //addDescriptionsCollection();
      addAbout();
      addMap();
    }

    public void DealsClicked()
    {
      switchView(HotelRoomsTable, Tabs.DealButton);
    }

    public void AboutClicked()
    {
      switchView(HotelAboutTable, Tabs.AboutButton);
    }

    public void PicsClicked()
    {
      SwitchButtons(Tabs.PicsButton);

    }

    public void MapClicked()
    {
      switchView(HotelMap, Tabs.MapButton);
    }


    void switchView(UIView newView, UIButton buttonClicked)
    {
      SwitchButtons(buttonClicked);

      if(currentView != null)
        currentView.RemoveFromSuperview();

      if(newView != null)
        Add(newView);

      currentView = newView;
      NSNotificationCenter.DefaultCenter.PostNotificationName("TabbedViewChanged", newView);
    }

    void SwitchButtons(UIButton buttonClicked)
    {
      if(buttonClicked == Tabs.CurrentButton)
        return;
      if(Tabs.CurrentButton != null)
      {
        UIView.Animate(
          0.150,
          () => {
            Tabs.CurrentButton.BackgroundColor = UIColor.Clear;
            Tabs.CurrentButton.SetTitleColor(AppConfig.SecondayColor, UIControlState.Normal);
          }
        );
      }
      Tabs.CurrentButton = buttonClicked;

      UIView.Animate(
        0.150,
        () => {
          Tabs.CurrentButton.BackgroundColor = AppConfig.PrimaryColor;
          Tabs.CurrentButton.SetTitleColor(UIColor.White, UIControlState.Normal);
        }
      );


    }

    void addTabs()
    {
      if(Tabs != null)
        return;
      Tabs = NibLoader.Create<HotelTabsPanel>(new RectangleF(0, 0, SCREEN_WIDTH, TabsHeight));
      Tabs.DealButton.TouchUpInside += (sender, e) => DealsClicked();
      Tabs.AboutButton.TouchUpInside += (sender, e) => AboutClicked();
      Tabs.PicsButton.TouchUpInside += (sender, e) => PicsClicked();
      Tabs.MapButton.TouchUpInside += (sender, e) => MapClicked();

      Add(Tabs);
    }

    void addRoomsTable()
    {
      HotelRoomsTable = new UIHotelRoomsTableView(
        new RectangleF(0, Tabs.Frame.Bottom, SCREEN_WIDTH, 300));
    }

    //    void addDescriptionsCollection()
    //    {
    //      HotelDescriptions = new UIHotelAboutView(
    //        new RectangleF(0, Tabs.Frame.Bottom, SCREEN_WIDTH, UIScreen.MainScreen.Bounds.Height - 42));
    //    }
    //
    void addAbout()
    {
      HotelAboutTable = new UIHotelAboutTable(
        new RectangleF(0, Tabs.Frame.Bottom, SCREEN_WIDTH, UIScreen.MainScreen.Bounds.Height - 42));

    }

    void addMap()
    {
      HotelMap = new UIHotelMapView(new Hot5MapDelegate(), 
        new RectangleF(0, Tabs.Frame.Bottom, SCREEN_WIDTH, UIScreen.MainScreen.Bounds.Height - 42));
    }

    void Refresh()
    {
      HotelRoomsTable.Hotel = Hotel;
      HotelMap.Hotel = Hotel;
      //HotelDescriptions.Hotel = Hotel;
      HotelAboutTable.Source = new UiHotelAboutTableSource(Hotel);
      HotelAboutTable.ReloadData();
      HotelAboutTable.SetNeedsLayout();
    }

    public void HandleScrolled(object sender)
    {
      var scrollView = sender as UIScrollView;
      if(scrollView.ContentOffset.Y >= 250)
      {
        TryStickToView(scrollView.Superview);
      }
      else
      {
        TryUnstickTabs();
      }
    }

    public void TryStickToView(UIView view)
    {
      if(!Tabs.StuckToView)
        Tabs.MoveToView(view);
    }

    public void TryUnstickTabs(float topPosition = 0)
    {
      if(!Tabs.StuckToView)
        return;
      Tabs.Frame = new RectangleF(0, topPosition, SCREEN_WIDTH, TabsHeight);
      Tabs.MoveToView(this);
    }


    public override UIView HitTest(PointF point, UIEvent uievent)
    {

      if(!UserInteractionEnabled)
        return null;
//      "UITabbedView HitTest: point={0}".Log(point);

      var view = base.HitTest(point, uievent);
      foreach(UIView subview in Subviews)
      {
        if(view != null && view.UserInteractionEnabled)
          break;
        var newPoint = ConvertPointToView(point, subview);
        view = subview.HitTest(newPoint, uievent);
      }
      return view;
    }
  }
}

