﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using LeadedSky.UI.iOS;
using System.Runtime.CompilerServices;

namespace Hot5
{
  [Register("UIOverflowingScrollView")]
  public class UIOverflowingScrollView : UIScrollView, IUIScrollViewDelegate
  {
    public bool Killed = false;

    public UIOverflowingScrollView(IntPtr h) : base(h)
    {
      initialize();
    }

    public UIOverflowingScrollView(RectangleF frame) : base(frame)
    {
    }

    public UIOverflowingScrollView()
    {
    }

    void initialize()
    {
      BackgroundColor = UIColor.Clear;
      ClipsToBounds = false;
      Layer.MasksToBounds = false;     

//      Delegate = new UIOverflowingScrollViewDelegate();
      WeakDelegate = this;
    }



    public void SetContentSize(SizeF size)
    {
      ContentSize = new SizeF(size.Width, size.Height + 75);
    }

    public void KillScroll()
    {
      ScrollEnabled = false;
      ScrollEnabled = true;
    }

    public void ReviveScroll()
    {
      UserInteractionEnabled = true;
      ScrollEnabled = true;
    }

    [Export("scrollViewDidScroll:")]
    public new void Scrolled(UIScrollView scrollView)
    {
      HotelEventCoordinator.ScrollCoordinator.NotifyScrolled(scrollView);
    }

    [Export("scrollViewDidEndScrollingAnimation:")]
    public new void ScrollAnimationEnded(UIScrollView scrollView)
    {
      NSNotificationCenter.DefaultCenter.PostNotificationName("ScrollAnimationEnded", this);
      "Animation ended: Enabled={0}, Offset={1}".Log(scrollView.ScrollEnabled, scrollView.ContentOffset);
    }


    [Export("scrollViewWillEndDragging:withVelocity:targetContentOffset:"), CompilerGenerated]
    public new void WillEndDragging(UIScrollView scrollView, PointF velocity, ref PointF targetContentOffset)
    {
      var offsetY = -218;
      if(!scrollView.ScrollEnabled)
      {
        "Will end dragging, scroll not enabled: {0}".Log(scrollView);
        scrollView.SetContentOffset(new PointF(0, offsetY), true);
      }
    }

    [Export("scrollViewDidEndDragging:willDecelerate:")]
    public new void DraggingEnded(UIScrollView scrollView, bool willDecelerate)
    {
      if(!scrollView.ScrollEnabled)
      {
        "Dragging Ended, scroll not enabled: {0}".Log(scrollView);
        scrollView.ContentOffset = new PointF(0, -75);
      }
    }

    public override UIView HitTest(PointF point, UIEvent uievent)
    {

//      "UIOverflowingScrollView View HitTest: point={0}".Log(point);

      var view = base.HitTest(point, uievent);

      foreach(UIView subview in Subviews)
      {
        if(view != null && view.UserInteractionEnabled)
          break;
        var newPoint = ConvertPointToView(point, subview);
        view = subview.HitTest(newPoint, uievent);
      }
      if(view == null && point.Y > 50 && UserInteractionEnabled)
        view = this;

      return view;
    }
  }

}