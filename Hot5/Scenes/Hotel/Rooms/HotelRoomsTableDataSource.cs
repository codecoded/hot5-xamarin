﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;
using Hot5.Core;
using System.Collections.Generic;
using LeadedSky.UI.iOS;
using PubSub;

namespace Hot5
{
  public class HotelRoomsTableDataSource : UITableViewSource
  {

    public List<HotelRoom> Rooms;

    public HotelRoomsTableDataSource(List<HotelRoom> rooms)
    {
      Rooms = rooms;
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      this.Publish<HotelRoom>(Rooms[indexPath.Row]);
    }

    public override int NumberOfSections(UITableView tableView)
    {
      return 1;
    }


    public override int RowsInSection(UITableView tableview, int sectionIndex)
    {
      if(Rooms == null || Rooms.Count == 0)
        return 1;
      return Rooms.Count;
    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {
      var cell = (UIXHotelRoomCell)tableView.DequeueReusableCell(UIXHotelRoomCell.Key);

      if(Rooms == null)
      {
        var noRoomsCell = tableView.DequeueReusableCell(NoRoomsCell.Key) as NoRoomsCell;
        noRoomsCell.Play();
        noRoomsCell.UserInteractionEnabled = false;
        return noRoomsCell;
      }
      if(Rooms.Count == 0)
      {
        var noRoomsCell = tableView.DequeueReusableCell(NoRoomsCell.Key) as NoRoomsCell;
        noRoomsCell.SetNoRooms();
        noRoomsCell.UserInteractionEnabled = false;

        return noRoomsCell;
      }
      cell.UserInteractionEnabled = true;
      cell.Room = Rooms[indexPath.Row];
      return cell;
    }

    public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
    {
      if(Rooms == null || Rooms.Count == 0)
        return 220;
      var room = Rooms[indexPath.Row];
      if(room.MainAmenities)
        return 100f; 
      return 75f;
    }


    UIView NoRooms()
    {
      var view = new UILabel(new RectangleF(0, 0, 320, 220));
      view.Text = "No rooms found";
      return view;
    }
  }
}

