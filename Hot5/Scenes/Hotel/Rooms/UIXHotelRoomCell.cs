﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using LeadedSky.UI.iOS;
using PubSub;

namespace Hot5
{
  public partial class UIXHotelRoomCell : UITableViewCell
  {
    public static readonly UINib Nib = UINib.FromName("UIXHotelRoomCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("UIXHotelRoomCell");

    HotelRoom room;

    public HotelRoom Room
    {
      get
      {
        return room;
      }
      set
      {
        room = value;
        Refresh();
      }
    }

    public UIXHotelRoomCell(IntPtr handle) : base(handle)
    {
    }

    public override void AwakeFromNib()
    {
      base.AwakeFromNib();
      lblBookWith.TextColor = AppConfig.PrimaryColor;
      SelectionStyle = UITableViewCellSelectionStyle.None;
      btnBookNow.TouchUpInside += (object sender, EventArgs e) => this.Publish<HotelRoom>(Room);
    }

    void SetPriceLabel()
    {
      var firstAttributes = new UIStringAttributes { Font = AppConfig.FontBold(16)  };

      var price = string.Format(SessionStore.CurrencyFormat, "{0:C0}", room.Price).ToMutAttrib();
      price.SetAttributes(firstAttributes.Dictionary, new NSRange(0, SessionStore.CurrencyFormat.CurrencySymbol.Length));
      lblPrice.AttributedText = price;
    }

    public static UIXHotelRoomCell Create()
    {
      return (UIXHotelRoomCell)Nib.Instantiate(null, null)[0];
    }

    void Refresh()
    {
      SetPriceLabel();
      imgWifi.Hidden = true;
      imgRefundable.Hidden = true;

      roomDescription.Text = room.Description;

      imgProvider.Image = UIImage.FromBundle(string.Format("{0}_small.png", room.Provider.ToLower()));

      if(!room.MainAmenities)
      {
        //cnstNameProviderVS.Constant = 10f;
        return;
      }
      //cnstNameProviderVS.Constant = 36f;
      if(!room.Wifi.GetValueOrDefault())
        cnstImgB.Constant = 15;
      else
      {
        imgWifi.Image = UIImage.FromFile("WifiLabel.png");
        imgWifi.Hidden = false;
      }

//      imgRefundable.Image = UIImage.FromFile("refundable_label.png");
      if(room.Breakfast.GetValueOrDefault())
      {
        imgRefundable.Image = UIImage.FromFile("breakfast_label.png");
//          imgRefundable.Frame = imgRefundable.Frame.ModX(imgWifi.Frame.X);
        imgRefundable.Hidden = false;
      }
//      var f = imgProvider.Frame;
//      f.Y = 76;
      //imgProvider.Frame = f;
      AutoresizingMask = UIViewAutoresizing.All;
      SetNeedsLayout();
    }



  }
}

