// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelRoomCell")]
	partial class UIXHotelRoomCell
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnBookNow { get; set; }

		[Outlet]
		MonoTouch.UIKit.NSLayoutConstraint cnstImgB { get; set; }

		[Outlet]
		MonoTouch.UIKit.NSLayoutConstraint cnstNameProviderVS { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgProvider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgRefundable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgWifi { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblBookWith { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblPrice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel roomDescription { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnBookNow != null) {
				btnBookNow.Dispose ();
				btnBookNow = null;
			}
			if (cnstImgB != null) {
				cnstImgB.Dispose ();
				cnstImgB = null;
			}
			if (cnstNameProviderVS != null) {
				cnstNameProviderVS.Dispose ();
				cnstNameProviderVS = null;
			}
			if (imgProvider != null) {
				imgProvider.Dispose ();
				imgProvider = null;
			}
			if (imgRefundable != null) {
				imgRefundable.Dispose ();
				imgRefundable = null;
			}
			if (imgWifi != null) {
				imgWifi.Dispose ();
				imgWifi = null;
			}
			if (lblBookWith != null) {
				lblBookWith.Dispose ();
				lblBookWith = null;
			}
			if (lblPrice != null) {
				lblPrice.Dispose ();
				lblPrice = null;
			}
			if (roomDescription != null) {
				roomDescription.Dispose ();
				roomDescription = null;
			}
		}
	}
}
