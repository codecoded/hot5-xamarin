﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Hot5
{
  public partial class NoRoomsCell : UITableViewCell
  {
    public static readonly UINib Nib = UINib.FromName("NoRoomsCell", NSBundle.MainBundle);
    public static readonly NSString Key = new NSString("NoRoomsCell");

    public NoRoomsCell(IntPtr handle) : base(handle)
    {
    }

    public static NoRoomsCell Create()
    {
      return (NoRoomsCell)Nib.Instantiate(null, null)[0];
    }

    public void Play()
    {
      lblDescription.Text = "Finding best room prices right now";
      activityLoading.Hidden = false;
       
      activityLoading.StartAnimating();
    }

    public void SetNoRooms()
    {
      lblDescription.Text = "Sorry, there are no rooms available for those dates";
      activityLoading.Hidden = true;
      activityLoading.StopAnimating();

    }
  }
}

