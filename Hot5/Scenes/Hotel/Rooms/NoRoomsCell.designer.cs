// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("NoRoomsCell")]
	partial class NoRoomsCell
	{
		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView activityLoading { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblDescription { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (activityLoading != null) {
				activityLoading.Dispose ();
				activityLoading = null;
			}
			if (lblDescription != null) {
				lblDescription.Dispose ();
				lblDescription = null;
			}
		}
	}
}
