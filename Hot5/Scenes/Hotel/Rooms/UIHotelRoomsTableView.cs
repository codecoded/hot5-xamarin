using MonoTouch.UIKit;
using System.Drawing;
using Hot5.Core;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;
using MonoTouch.Foundation;
using PubSub;

namespace Hot5
{
  public partial class UIHotelRoomsTableView : UITableView
  {
    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public bool Animating { get; set; }

    public PointF CurrentOffset { get; set; }

    public bool CellsVisible{ get { return VisibleCells != null && VisibleCells.Length > 0; } }

    UIButton btnCalendar;

    public RectangleF FirstVisbleCellFrame
    {
      get
      {
        if(!CellsVisible)
          return RectangleF.Empty;
        return VisibleCells[0].Frame;
      }
    }

    public UIHotelRoomsTableView(RectangleF frame) : base(frame, UITableViewStyle.Grouped)
    {
      initialize();
    }

    void initialize()
    {
      TableHeaderView = CalendarButton();

      RegisterNibForCellReuse(UIXHotelRoomCell.Nib, UIXHotelRoomCell.Key);
      RegisterNibForCellReuse(NoRoomsCell.Nib, NoRoomsCell.Key);

      UserInteractionEnabled = true;
//      ContentInset = new UIEdgeInsets(-37, 0, 0, 0);
      ClipsToBounds = false;
      BackgroundColor = UIColor.White;
      Layer.MasksToBounds = false;     
      SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;
      SeparatorColor = AppConfig.SecondayColor;
      SeparatorInset = new UIEdgeInsets(0, 10, 0, 10);
    }

    UIButton CalendarButton()
    {
      btnCalendar = new UIButton(UIButtonType.Custom);
      btnCalendar.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
      btnCalendar.Font = AppConfig.Font();
      btnCalendar.Frame = new RectangleF(0, 0, 320, 50);
      btnCalendar.BackgroundColor = UIColor.White;
      btnCalendar.TitleEdgeInsets = new UIEdgeInsets(0, -20, 0, 0);
      btnCalendar.ImageEdgeInsets = new UIEdgeInsets(0, 275, 0, 0);
      btnCalendar.SetImage(UIImage.FromBundle("calendar_colour.png"), UIControlState.Normal);
      SetCalendarText(DateRange.DaysFromNow(3));

      btnCalendar.TouchUpInside += (sender, e) => this.Publish<CalendarViewController>(null);
      return btnCalendar;
    }

   

    void Refresh()
    {
      Source = new HotelRoomsTableDataSource(hotel.Rooms);
      ReloadData();
      Resize();
    }

    public void Resize()
    {
      if(ContentSize.Height > Frame.Height)
        Frame = new RectangleF(Frame.Location, ContentSize);
    }


    public void SetCalendarText(DateRange selectedDates)
    {
      var dayView = new CalendayDayView(selectedDates.StartDate);
      var nights = selectedDates.TotalDays;

      var text = "Check-in "
        .Concatenate(AppConfig.Bold(dayView.CheckInDateDescription()))
        .Concatenate(" for ")
        .Concatenate(AppConfig.Bold(string.Format("{0} {1}", nights, nights == 1 ?"night" : "nights")));
      btnCalendar.SetAttributedTitle(text, UIControlState.Normal);
    }
      
  }
}
