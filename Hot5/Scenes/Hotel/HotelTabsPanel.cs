﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.ObjCRuntime;
using LeadedSky.UI.iOS;
using LeadedSky.Shared;

namespace Hot5
{
  [Register("HotelTabsPanel")]
  public partial class HotelTabsPanel : UIView
  {
    public bool StuckToView = false;
    RectangleF resetFrame;

    public bool Animating { get; set; }

    public PointF CurrentOffset { get; set; }

    public float OptionsViewY;

    const float scrollOffset = 0;

    public UIView ViewToStickTo { get; private set; }

    public UIButton DealButton { get { return btnDeal; } }

    public UIButton AboutButton { get { return btnAbout; } }

    public UIButton PicsButton { get { return btnPics; } }

    public UIButton MapButton { get { return btnMap; } }


    public UIButton CurrentButton { get; set; }

    public bool ScrollableContent
    {

      get
      {
        return CurrentButton == DealButton || CurrentButton == AboutButton; 
      }
    }

    public HotelTabsPanel(IntPtr h) : base(h)
    {

    }

    public HotelTabsPanel()
    {
    }

    public override void AwakeFromNib()
    {
      base.AwakeFromNib();
      this.AddBorders(UIViewExtensions.BorderFlags.Bottom, AppConfig.SecondayColor, 0, 0, 0.5f);

    }

    public void ConfigureToStickTo(UIView viewToStickTo)
    {
      ViewToStickTo = viewToStickTo;
    }

    #region IScrollingEventsListener implementation

    public void HandleScrolled(object sender)
    {
      if(resetFrame == RectangleF.Empty)
        resetFrame = ViewToStickTo.ConvertRectFromView(ViewToStickTo.Bounds, this);

      UIScrollView scrollView = sender as UIScrollView;
      if(StuckToView)
      {
        if(scrollView.ContentOffset.Y <= resetFrame.Y)
          UnStickToView(scrollView);
      }
      else
      {
        var canvasLocation = ViewToStickTo.ConvertRectFromView(ViewToStickTo.Bounds, this);
        if(canvasLocation.Top <= 0)
          StickToView();
      }
    }


    #endregion

    public void Reset()
    {
      Frame = resetFrame;
    }

    public void Move(PointF offset)
    {
      if(StuckToView)
        return;
      if(resetFrame == RectangleF.Empty)
        resetFrame = Frame;

      var top = newTop(offset);
      if(top < 0)
      {
        if(Frame.Y == 0)
          return;
        top = 0;
      }
      var frame = Frame;
      frame.Y = top; 
      Frame = frame;
    }

    float newTop(PointF offset)
    {
      return resetFrame.Y - offset.Y;
    }

    public void StickToView()
    {
      resetFrame = ViewToStickTo.ConvertRectFromView(ViewToStickTo.Bounds, this);
      Frame = new RectangleF(PointF.Empty, Frame.Size);
      MoveToView(ViewToStickTo);
    }


    public void UnStickToView(UIView view)
    {
      Frame = resetFrame;
      MoveToView(view);
    }

    public void MoveToView(UIView view)
    {
      RemoveFromSuperview();
      view.AddSubview(this);
      StuckToView = !StuckToView;
    }

    public RectangleF CurrentPosition()
    {
      if(StuckToView)
      {
        return ConvertRectFromView(Frame, Superview);
      }

      return Frame;
    }
  }
}

