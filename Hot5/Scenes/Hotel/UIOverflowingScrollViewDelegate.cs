﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using LeadedSky.UI.iOS;

namespace Hot5
{

  public class UIOverflowingScrollViewDelegate : UIScrollViewDelegate
  {
    public override void WillEndDragging(UIScrollView scrollView, PointF velocity, ref PointF targetContentOffset)
    {
      "Will End Dragging: {0}".Log(scrollView);

      var offsetY = -218;
      if(!scrollView.ScrollEnabled)
      {
        scrollView.SetContentOffset(new PointF(0, offsetY), true);
      }
    }

    public override void DraggingEnded(UIScrollView scrollView, bool willDecelerate)
    {
      "Dragging Ended: {0}".Log(scrollView);
      if(!scrollView.ScrollEnabled)
        scrollView.ContentOffset = new PointF(0, -50);
    }

    public override void Scrolled(UIScrollView scrollView)
    {
      if(scrollView.ContentOffset.Y < -50)
        scrollView.ScrollEnabled = false;

      HotelEventCoordinator.ScrollCoordinator.NotifyScrolled(scrollView);
    }
  }

}

