using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using LeadedSky.UI.iOS;
using System.Drawing;
using LeadedSky.Shared;
using Hot5.Core;
using Hot5;

using PubSub;
using Pusher;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin;

namespace Hot5
{
  public partial class UIHotelController : UIViewController, IScrollingEventsListener
  {

    public Hotel Hotel;
    HotelEventCoordinator EventCoordinator;

    public string Slug;
    public HotelRequest Request;
    public string Key;
    public UITabbedView TabbedView;
    public UIPagedHotelImagesView HotelImages;

    bool IsInScrollView{ get { return TabbedView.IsDescendantOfView(ScrollView); } }

    UINoResultsView NoResultsView;
    LoadingOverlay loadingOverlay;

    //y  = SCREEN_HEIGHT - 568-
    float BOTTOM_OFFSET { get { return -ScrollView.Frame.Height + 50; } }

    const float SNAP_POINT = -55;

    public static float SCREEN_WIDTH = UIScreen.MainScreen.Bounds.Width;
    public static float SCREEN_HEIGHT = UIScreen.MainScreen.Bounds.Height;

    public UIHotelController(IntPtr handle) : base(handle)
    {
    }

    public override void LoadView()
    {
      base.LoadView();
      "UIHotelController Loading".Log();
      if(EventCoordinator == null)
      {
        EventCoordinator = new HotelEventCoordinator(this);
      }

//      Hotel = HotelJsonFactory.LoadHotel().Hotel;
//      PusherClient.SubscribeAsync(Hotel.Channel);

      loadData();
      Request.Slug = Hotel.Slug;
      TabbedView.HotelRoomsTable.SetCalendarText(Request.CalendarDates());

      if(Hotel.Rooms != null && Hotel.Rooms.Count == 0)
        Hotel.Rooms = null;

      loadHotel();
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      NavigationItem.Title = Hotel.Name;
      NavigationController.NavigationBar.Alpha = 1f;
    }

    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      EventCoordinator.Attach();
      RefreshData();
    }

    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
      EventCoordinator.Detach();
    }

    void loadHotel()
    {
      ApiClient.HotelSearch(Request, response => {
        var hotelsResponse = HotelResponseParser.Parse<HotelResponse>(response);
        if(hotelsResponse == null)
          return;

        Hotel.Rooms = hotelsResponse.Hotel.Rooms;
        Hotel.Providers = hotelsResponse.Hotel.Providers;
//          Request.Key = Hotel.Key;

        AppSession.RunOnMainThread(() => RefreshData());
      });    
    }

    void loadData()
    {
      addImagesCollection();
      addTabbedView();
      ScrollView.AddSubview(TabbedView);

      RefreshData(true);
//      ScrollView.ContentSize = TabbedView.ContentSize;
//      TabbedView.DealsClicked();
    }

    public void RefreshContentSize(UIHotelAboutTable aboutTable)
    {
      ScrollView.SetContentSize(aboutTable.CurrentHSize());

      aboutTable.AdjustFrame(height: aboutTable.CurrentHSize().Height);

    }

    void RefreshData(bool clickDeals = false)
    {
//      PusherClient.SubscribeAsync(Hotel.Channel);

      TabbedView.Hotel = Hotel;

      var offset = ScrollView.ContentOffset.Y;
      ScrollView.SetContentSize(TabbedView.ContentSize);
      ScrollView.SetContentOffset(new PointF(0, offset), false);
      if(clickDeals)
        TabbedView.DealsClicked();
    }
    //
    //    public void PushMessageReceived(object sender, IIncomingEvent<AvailabilityUpdateContract> incomingEvent)
    //    {
    //      "PushMessageReceived<AvailabilityUpdateContract>: eventName={0}, channel={1}, data={2}".Log(incomingEvent.EventName, incomingEvent.Channel, incomingEvent.Data);
    //
    ////      Request.Key = incomingEvent.DataObject.Key;
    //
    //      ApiClient.HotelRooms(Request, response => {
    //        var hotelRooms = HotelResponseParser.Parse<List<HotelRoom>>(response);
    //       
    //        using (var pool = new NSAutoreleasePool())
    //          pool.BeginInvokeOnMainThread(() => {
    //            if(hotelRooms != null)
    //            {
    //              Hotel.Rooms = hotelRooms;
    //            }
    //            else
    //            {
    //              "No rooms available".Log();
    //            }
    //            RefreshData();
    //          });
    //      });
    //    }
    //
    //
    public void PresentCalendar(object blank)
    {
      var calendarViewController = (CalendarViewController)Storyboard.InstantiateViewController("CalendarViewController");
      calendarViewController.SelectedDates = Request.CalendarDates();
      calendarViewController.DatesSelected = CalendarDatesSelected;    
      PresentViewController(calendarViewController, true, null);
    }

    public void ScrollAnimationEnded(NSNotification notification = null)
    {
      TabbedView.UserInteractionEnabled = true;
      ScrollView.ScrollEnabled = ScrollView.ContentOffset.Y >= 0;
      ScrollView.UserInteractionEnabled = TabbedView.ScrollableContent;

      ScrollView.SetContentSize(TabbedView.ContentSize);
    }

    void addImagesCollection()
    {
      HotelImages = new UIPagedHotelImagesView(new RectangleF(0, 0, SCREEN_WIDTH, ScrollView.Frame.Y));
      Add(HotelImages);
      View.SendSubviewToBack(HotelImages);
      HotelImages.Hotel = Hotel;

    }

    void addTabbedView()
    {
      TabbedView = new UITabbedView(UIScreen.MainScreen.Bounds);
      TabbedView.Tabs.PicsButton.TouchUpInside += (sender, e) => PicsView();
    }

    public void TabbedViewChanged(NSNotification notification)
    {
      if(notification.Object is UIHotelAboutTable || notification.Object is UIHotelRoomsTableView)
      //if(TabbedView.ScrollableContent)
        SlideView();
      else if(notification.Object is UIHotelMapView)
        MapView();
      else
        PicsView();
    }

    public void HandleScrolled(object sender)
    {
      if(ScrollView.ContentOffset.Y < SNAP_POINT && ScrollView.Dragging)
        PicsView();
    }

    public void SlideView()
    {
      if(ScrollView.ContentOffset == PointF.Empty)
      {
        ScrollView.UserInteractionEnabled = true;
        ScrollView.SetContentSize(TabbedView.ContentSize);
        return;
      }

      var animate = ScrollView.ContentOffset.Y < 300;
      ScrollView.SetContentOffset(new PointF(0, 0), animate);

      TabbedView.UserInteractionEnabled = !animate;

      if(!animate)
        ScrollAnimationEnded();
    }

    public void MapView()
    {
      ScrollView.UserInteractionEnabled = false;
      TabbedView.UserInteractionEnabled = false;
      bool animate = ScrollView.ContentOffset.Y < 250;
      ScrollView.SetContentOffset(new PointF(0, 250), animate);
      if(!animate)
        ScrollAnimationEnded();
    }

    public void PicsView()
    {
      TabbedView.PicsClicked();
      if(ScrollView.ContentOffset.Y == BOTTOM_OFFSET)
        return;

      ScrollView.KillScroll();

      TabbedView.UserInteractionEnabled = false;

      ScrollView.SetContentOffset(new PointF(0, BOTTOM_OFFSET), true);
      ScrollView.UserInteractionEnabled = false;
      HotelImages.Frame = new RectangleF(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - TabbedView.Tabs.Frame.Height);
    }

    public void HotelRoomSelected(HotelRoom hotelRoom)
    {
      var bookingController = new UIXHotelBookingController(Hotel, hotelRoom);
      bookingController.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;

      Insights.Track("OfferSelected", new Dictionary<string, string> {
        { "HotelId", Hotel.Id.ToString() },
        { "Provider", hotelRoom.Provider },
        { "Price", hotelRoom.OfferPrice },
        { "StartDate",Request.StartDate.ToShortDateString() },
        { "EndDate", Request.EndDate.ToShortDateString() }
      });

//      NavigationController.PresentViewController(bookingController, true, null);
      NavigationController.PushViewController(bookingController, true);
//      PresentController(bookingController);
    }

    void PresentController(UIViewController controller)
    {
      var navController = new UINavigationController(controller);
      PresentViewController(navController, true, null);
    }

    void CalendarDatesSelected(DateRange selectedDates)
    {
      AppRequestResponse.Request.StartDate = selectedDates.StartDate;
      AppRequestResponse.Request.EndDate = selectedDates.EndDate;
      Request = AppRequestResponse.Request.ToHotelRequest(Hotel);
      Request.Key = null;
      TabbedView.HotelRoomsTable.SetCalendarText(selectedDates);
      //PusherClient.UnsubscribeFromChannel(Hotel.Channel);
      Hotel.Rooms = null;
      //RefreshData();
      loadHotel();
      AppRequestResponse.ReloadRequest = true;
//      DisplayLoading();
    }


    void DisplayNoResults()
    {
//      HideLoadingOverlay();

      if(loadingOverlay != null && !loadingOverlay.Hidden)
        return;
//
      if(NoResultsView == null)
        NoResultsView = new UINoResultsView(
          new RectangleF(0, UIScreen.MainScreen.Bounds.Height - 252, UIScreen.MainScreen.Bounds.Width, 252)
        );

//      View.AddSubview(NoResultsView);
//      HotelTableView.Hidden = true;
    }

    void HideLoadingOverlay()
    {
      if(loadingOverlay == null)
        return;
      loadingOverlay.Hidden = true;
    }

    void DisplayLoading()
    {
      if(loadingOverlay == null)
      {
        loadingOverlay = new LoadingOverlay(new RectangleF(0, UIScreen.MainScreen.Bounds.Bottom - 358, UIScreen.MainScreen.Bounds.Width, 360));
        loadingOverlay.Layer.ZPosition = 1000;
        View.Add(loadingOverlay);
      }
      else
      {
        loadingOverlay.Hidden = false;
      }

    }

    void HideNoResults()
    {
      if(NoResultsView != null && NoResultsView.IsDescendantOfView(View))
        NoResultsView.RemoveFromSuperview();
      HideLoadingOverlay();
    }

  }
}
