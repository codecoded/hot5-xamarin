﻿using System;
using PubSub;
using Hot5.Core;
using LeadedSky.Shared;
using LeadedSky.UI.iOS;
using MonoTouch.Foundation;

namespace Hot5
{
  public class HotelEventCoordinator
  {
    public static readonly ScrollingCoordinator ScrollCoordinator = new ScrollingCoordinator();
    public static PagingCoordinator PagingEventsCoordinator = new PagingCoordinator();
    public static NSObject TAB_CHANGED_OBSERVER;
    public static NSObject SCROLL_ANIM_ENDED_OBSERVER;
    public static NSString TAB_CHANGED = new NSString("TabbedViewChanged");
    public static NSString SCROLL_ANIM_ENDED = new NSString("ScrollAnimationEnded");
    public const string DESCRIPTION_PAGED_EVENT_ID = "HotelDescriptionPagedEventId";

    public UIHotelController Controller { get; private set; }


    public HotelEventCoordinator(UIHotelController controller)
    {
      Controller = controller;
    }

    public void Attach()
    {
      "Attaching UIHotelController Events: Handle={0}".Log(Controller.Handle);

      this.Subscribe<HotelRoom>(Controller.HotelRoomSelected);
      this.Subscribe<CalendarViewController>(Controller.PresentCalendar);
      this.Subscribe<UIHotelAboutTable>(Controller.RefreshContentSize);

      TAB_CHANGED_OBSERVER = NSNotificationCenter.DefaultCenter.AddObserver(
        TAB_CHANGED, Controller.TabbedViewChanged);
      SCROLL_ANIM_ENDED_OBSERVER = NSNotificationCenter.DefaultCenter.AddObserver(
        SCROLL_ANIM_ENDED, Controller.ScrollAnimationEnded);

      ScrollCoordinator.AddListener(Controller);
      ScrollCoordinator.AddListener(Controller.HotelImages);
      ScrollCoordinator.AddListener(Controller.TabbedView);
      PagingEventsCoordinator.AddListener(Controller.HotelImages.HotelImagesCollection);
      PagingEventsCoordinator.AddListener(Controller.HotelImages.ImagesPager);

      //PusherClient.Pusher.GetEventSubscription<AvailabilityUpdateContract>().EventEmitted += Controller.PushMessageReceived;


    }

    public void Detach()
    {
      "Detaching UIHotelController Events: Handle={0}".Log(Controller.Handle);

      this.Unsubscribe<HotelRoom>();
      this.Unsubscribe<CalendarViewController>();

      ScrollCoordinator.RemoveListener(Controller);
      ScrollCoordinator.RemoveListener(Controller.HotelImages);
      ScrollCoordinator.RemoveListener(Controller.TabbedView);
      PagingEventsCoordinator.RemoveListener(Controller.HotelImages.HotelImagesCollection);
      PagingEventsCoordinator.RemoveListener(Controller.HotelImages.ImagesPager);

      NSNotificationCenter.DefaultCenter.RemoveObserver(TAB_CHANGED_OBSERVER);
      NSNotificationCenter.DefaultCenter.RemoveObserver(SCROLL_ANIM_ENDED_OBSERVER);
//      PusherClient.UnsubscribeFromChannel(Controller.Hotel.Channel);
//      PusherClient.Pusher.GetEventSubscription<AvailabilityUpdateContract>().EventEmitted -= Controller.PushMessageReceived;

    }
  }
}

