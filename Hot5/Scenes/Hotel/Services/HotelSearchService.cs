﻿using System;
using Hot5.Core;
using System.Threading.Tasks;
using LeadedSky.UI.iOS;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using PubSub;

namespace Hot5
{
  public class HotelSearchService
  {
    public RestSharp.RestRequestAsyncHandle RequestHandle;
    int searchCount;

    public int RetryCount { get; private set; }

    public HotelSearchService(int retryCount = 8)
    {
      RetryCount = retryCount;
    }

    public void StartHotelsSearch(HotelRequest request)
    {
      searchCount++;
      RequestHandle = ApiClient.StartHotelsSearch(request, ProcessResponse);
    }

    public  void Search(HotelsRequest request)
    {
      searchCount++;
      RequestHandle = ApiClient.HotelsResults(request, ProcessResponse);
    }

    public void ProcessResponse(HotelsRequest request, string response)
    {
      if(response == null)
        Retry(request);
      else
      {
        var hotelsResponse = HotelResponseParser.Parse<HotelsResponse>(response);
        if(hotelsResponse != null && hotelsResponse.Hotels != null)
          hotelsResponse.SetHotelIndices();
        hotelsResponse.SetCurrency();
        this.Publish<HotelsResponse>(hotelsResponse);
      }
    }

    public void Abort()
    {
      if(RequestHandle != null)
        RequestHandle.Abort();
    }

    void Retry(HotelsRequest request)
    {
      Console.WriteLine("HTTP errored out");
      if(searchCount < RetryCount)
        Search(request);
      else
      {
        using (var pool = new NSAutoreleasePool())
        {
          pool.BeginInvokeOnMainThread(delegate {
            new UIAlertView("Uh-oh!", "There seems to be a problem retrieving the latest prices. Best try starting the search again!", null, "Ok").Show();
          });
        }
      }

    }

  }
}

