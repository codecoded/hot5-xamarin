// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("UIXHotelBookingController")]
	partial class UIXHotelBookingController
	{
		[Outlet]
		MonoTouch.UIKit.UIActivityIndicatorView activityMain { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem barBtnBack { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem barBtnForward { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem barBtnRefresh { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem barBtnShare { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imgProvider { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIToolbar toolMain { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView vwOverlay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIWebView webContent { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (activityMain != null) {
				activityMain.Dispose ();
				activityMain = null;
			}
			if (barBtnBack != null) {
				barBtnBack.Dispose ();
				barBtnBack = null;
			}
			if (barBtnForward != null) {
				barBtnForward.Dispose ();
				barBtnForward = null;
			}
			if (barBtnRefresh != null) {
				barBtnRefresh.Dispose ();
				barBtnRefresh = null;
			}
			if (barBtnShare != null) {
				barBtnShare.Dispose ();
				barBtnShare = null;
			}
			if (imgProvider != null) {
				imgProvider.Dispose ();
				imgProvider = null;
			}
			if (toolMain != null) {
				toolMain.Dispose ();
				toolMain = null;
			}
			if (vwOverlay != null) {
				vwOverlay.Dispose ();
				vwOverlay = null;
			}
			if (webContent != null) {
				webContent.Dispose ();
				webContent = null;
			}
		}
	}
}
