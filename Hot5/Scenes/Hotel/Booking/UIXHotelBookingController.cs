﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Hot5.Core;
using LeadedSky.UI.iOS;
using Xamarin;
using System.Net.Http;
using System.Collections.Generic;

namespace Hot5
{
  public partial class UIXHotelBookingController : UIViewController, IUIWebViewDelegate
  {
    public Hotel Hotel { get; private set; }

    public HotelRoom Room { get; private set; }

    public UIXHotelBookingController(Hotel hotel, HotelRoom room) : base("UIXHotelBookingController", null)
    {
      Hotel = hotel;
      Room = room;
    }

    public override void DidReceiveMemoryWarning()
    {
      base.DidReceiveMemoryWarning();
      
    }

    public override void AwakeFromNib()
    {
      base.AwakeFromNib();

    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      imgProvider.Image = UIImage.FromBundle(string.Format("{0}_small.png", Room.Provider.ToLower()));
      vwOverlay.Hidden = false;
      webContent.WeakDelegate = this;
      webContent.LoadRequest(new NSUrlRequest(new NSUrl(CreateLink())));
    }

    [Export("webViewDidFinishLoad:")]
    public void LoadingFinished(UIWebView webView)
    {
      HideDialog(webView);
    }

    void HideDialog(UIWebView webView)
    {     
      AppSession.RunOnMainThread(() => {
        vwOverlay.Hidden = true;
        webView.Hidden = false;
//        if(NavigationController != null)
//          NavigationController.SetToolbarHidden(false, false);
        barBtnBack.Enabled = webContent.CanGoBack;
        barBtnForward.Enabled = webContent.CanGoForward;
        barBtnRefresh.Enabled = true;
      });

    }
    //
    //    [Export("webView:didFailLoadWithError:")]
    //    public void LoadFailed(MonoTouch.UIKit.UIWebView webView, MonoTouch.Foundation.NSError error)
    //    {
    //      "Webview Error: {0}".Log(error);
    //      new UIAlertView("Problem", "It seems as we're unable to load the providers website. Please try again or contact us at support@hot5.com", null, "Ok").Show();
    //    }


    public override void ViewWillAppear(bool animated)
    {
      base.ViewWillAppear(animated);
      try
      {

        activityMain.StartAnimating();
        if(NavigationController != null)
          NavigationController.SetToolbarHidden(true, false);
        SetToolbarItems(toolMain.Items, false);

//      NavigationController.SetToolbarItems(toolMain.Items, false);
        barBtnRefresh.Clicked += (sender, e) => webContent.Reload();
        barBtnForward.Clicked += (sender, e) => webContent.GoForward();
        barBtnBack.Clicked += (sender, e) => webContent.GoBack();
        barBtnShare.Clicked += (sender, e) => ShareHotelOffer();

      }
      catch(Exception ex)
      {
        Insights.Report(ex);
        new UIAlertView("Problem", "It seems as we're unable to load the providers website. Please try again or contact us at support@hot5.com", null, "Ok").Show();
      }
    }


    public void ShareHotelOffer()
    {
      //This constructor will give us the option to share the _imageView.Image via AirDrop
      var a = new UIActivityViewController(new NSObject[] { new NSUrl(CreateLink(false)) }, null);
      PresentViewController(a, true, null);

    }

    public override void ViewWillDisappear(bool animated)
    {
      base.ViewWillDisappear(animated);
      activityMain.StopAnimating();
      NavigationController.SetToolbarHidden(true, false);
    }


    string CreateLink(bool mobile = true)
    {
      var uri = new UriBuilder("http", ApiClient.DOMAIN, 80, string.Format("offer/{0}{1}", (mobile ?"mobile/" : ""), Room.Provider));

      var queryParams = new Dictionary<string,string>() {
        { "start_date", AppRequestResponse.Request.StartDate.ToString("yyyy-MM-dd") },
        { "end_date", AppRequestResponse.Request.EndDate.ToString("yyyy-MM-dd") },
        { "currency", AppRequestResponse.Request.Currency },
        { "hotel_id", Hotel.Id.ToString() },
        { "provider", Room.Provider },
        { "price", Math.Round(Room.Price).ToString() },
        { "saving", Math.Round(Hotel.Offer.Saving).ToString() },
        { "max_price", Math.Round(Hotel.Offer.MaxPrice).ToString() }
      };

      if(Room.ProviderId != null)
        queryParams.Add("provider_id", Room.ProviderId.Value.ToString());

      uri.Query = new FormUrlEncodedContent(queryParams).ReadAsStringAsync().Result;

      return uri.ToString();
    }
  }
}

