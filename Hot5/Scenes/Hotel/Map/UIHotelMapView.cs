using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using Hot5.Core;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;

namespace Hot5
{
  public partial class UIHotelMapView : MonoTouch.MapKit.MKMapView
  {
    List<HotelAnnotation> HotelAnnotations;

    Hotel hotel;

    public Hotel Hotel
    {
      get
      {
        return hotel;
      }
      set
      {
        hotel = value;
        Refresh();
      }
    }

    public UIHotelMapView(MKMapViewDelegate mapDelegate, RectangleF frame) : base(frame)
    {
      Delegate = mapDelegate;
      initialize();
    }

    void initialize()
    {
      ShowsUserLocation = true;
    }

    public void HandleHotelChanged(Hotel hotel)
    {
      var annotation = HotelAnnotations.Find(e => e.Hotel.Index == hotel.Index);
      if(SelectedAnnotations != null)
        SelectedAnnotations.ToList().ForEach(e => DeselectAnnotation(e, false));
      SelectAnnotation(annotation, true);
    }

    HotelAnnotation createAnnotation()
    {
      return new HotelAnnotation(hotel, new CLLocationCoordinate2D(Hotel.Latitude, Hotel.Longitude));
    }

    MKMapCamera createCamera()
    {
      var camera = new MKMapCamera();
      camera.CenterCoordinate = new CLLocationCoordinate2D(Hotel.Latitude, Hotel.Longitude);
      camera.Altitude = 17000;
      return camera;
    }

    void Refresh()
    {
      SetCamera(createCamera(), false);
      SetCenterCoordinate(new CLLocationCoordinate2D(Hotel.Latitude, Hotel.Longitude), true);
      AddAnnotation(createAnnotation());
    }
  }
}
