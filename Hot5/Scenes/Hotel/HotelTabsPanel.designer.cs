// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("HotelTabsPanel")]
	partial class HotelTabsPanel
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnAbout { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnDeal { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnMap { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnPics { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (btnAbout != null) {
				btnAbout.Dispose ();
				btnAbout = null;
			}
			if (btnDeal != null) {
				btnDeal.Dispose ();
				btnDeal = null;
			}
			if (btnMap != null) {
				btnMap.Dispose ();
				btnMap = null;
			}
			if (btnPics != null) {
				btnPics.Dispose ();
				btnPics = null;
			}
		}
	}
}
