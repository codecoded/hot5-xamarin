using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using Hot5.Core;
using System.Collections.Generic;
using LeadedSky.Shared;
using System.Linq;
using System.Drawing;

using LeadedSky.UI.iOS;

namespace Hot5
{
  partial class FilterViewController : UIViewController
  {

    public TableRowsSelectedDelegate HandleFilterHotels { get; set; }

    public List<string> SelectedStarRatings;
    public List<string> SelectedAmenities;
    public List<TableRow> SelectedRows = new List<TableRow>();

    public FilterViewController()
    {
    }

    public FilterViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();
      DoLayout();
      DoHooks();
      DoLoad();
    }

    public void DoLoad()
    {      
      var options = FilterOptions.CreateOptionsList(SelectedStarRatings, SelectedAmenities);
      FilterTable.Initialize(options, AppConfig.SEPERATOR_COLOR);
      SelectedRows = options.Select(e => e as TableRowItem).Where(e => e != null && e.Selected).Cast<TableRow>().ToList();
    }

    void DoLayout()
    {
      View.SetBackgroundImage(AppConfig.ModalBackgroundImage);
    }

    void DoHooks()
    {
      FilterTable.TableSelectionComplete = TableSelectionComplete;

      navItemCancel.Clicked += (sender, e) => DismissViewController(true, null);

      navItemApply.Clicked += (sender, e) => {
        if(HandleFilterHotels != null)
          HandleFilterHotels(SelectedRows);
        DismissViewController(true, null);
      };
    }

    public void TableSelectionComplete(TableRow tableRow)
    {
      var item = tableRow as TableRowItem;
      if(item == null)
        return;

      if(item.Selected)
        SelectedRows.Add(item);
      else
        SelectedRows.Remove(item);

    }
  }


}
