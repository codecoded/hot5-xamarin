// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System;
using System.CodeDom.Compiler;

namespace Hot5
{
	[Register ("FilterViewController")]
	partial class FilterViewController
	{
		[Outlet]
		LeadedSky.UI.iOS.SingleTableView FilterTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem navItemApply { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIBarButtonItem navItemCancel { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
