using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using LeadedSky.UI.iOS;
using LeadedSky.Shared;
using Pusher;
using Hot5.Core;

namespace Hot5
{
  public  partial class RootViewController : UIViewController
  {
    public LeadedSky.UI.iOS.SidebarController SidebarController { get; private set; }

    public UINavigationController NavController { get; private set; }


    public RootViewController(IntPtr handle) : base(handle)
    {
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();


      View.SetBackgroundImage(AppConfig.MainBackgroundImage);
      //View.ContentMode = UIViewContentMode.ScaleToFill;

      // create a slideout navigation controller with the top navigation controller and the menu view controller
      var menuController = (SettingsTableViewController)Storyboard.InstantiateViewController("SettingsTableViewController");
      var contentController = (UIHotelsViewController)Storyboard.InstantiateViewController("HotelsViewController");
      menuController.View.Frame = new System.Drawing.RectangleF(0, 0, 180, UIScreen.MainScreen.Bounds.Height);


      //      var testController = (TestController)Storyboard.InstantiateViewController("TestController");
      NavController = new UINavigationController(contentController);
      NavController.NavigationBar.BackgroundColor = UIColor.White;

//      NavController.PushViewController(contentController, false);

      NavController.View.AddBorders(UIViewExtensions.BorderFlags.Left, UIColor.Black.ColorWithAlpha(0.5f), 0, 0, 0.5f);

      SidebarController = new SidebarController(this, NavController, menuController);
      SidebarController.MenuLocation = LeadedSky.UI.iOS.SidebarController.MenuLocations.Left;
      SidebarController.MenuWidth = 180;
      SidebarController.StatusBarMoves = false;
      SidebarController.ReopenOnRotate = false;
//
    }
  }
}
