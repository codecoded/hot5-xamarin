﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using LeadedSky.UI.iOS;

namespace Hot5
{
  public static class AppConfig
  {
    // production
    public const string PUSHER_API = "ws://ws-eu.pusher.com/app/e82d8d284afcc0071094?protocol=5";
    // staging
    // public const string PUSHER_API = "ws://ws-eu.pusher.com/app/1163c38d8c21940bd111?protocol=5";
    // development
    //public const string PUSHER_API = "ws://ws-eu.pusher.com/app/e49cfdb8b8f62652d358?protocol=5";


    public  const float FontSize = 16f;
    public const string FONT_NAME = "HelveticaNeue-Light";
    public const string FONT_BOLD_NAME = "HelveticaNeue";
    public static UIColor SEPERATOR_COLOR = UIColor.FromWhiteAlpha(0.5f, 0.5f);
    public static string BACKGROUND_IMAGE_NAME = string.Format("background_{0}.jpg", new Random().Next(1, 8));
    public const string MODAL_BACKGROUND_IMAGE_NAME = "background_blur.jpg";

    public static UIColor PrimaryColor  { get { return UIColor.FromRGB(1f, 0.50196078431372548f, 0f); } }

    public static UIColor SecondayColor { get { return UIColor.FromRGB(0.53725490196078429f, 0.54117647058823526f, 0.53725490196078429f); } }

    private static UIImage mainBackgroundImage = null;

    public static UIImage MainBackgroundImage
    {
      get
      {
        if(mainBackgroundImage == null)
          mainBackgroundImage = UIImage.FromBundle(BACKGROUND_IMAGE_NAME);
        return mainBackgroundImage;
      }
    }

    private static UIImage modalBackgroundImage = null;

    public static UIImage ModalBackgroundImage
    {
      get
      {
        if(modalBackgroundImage == null)
          modalBackgroundImage = UIImage.FromBundle(MODAL_BACKGROUND_IMAGE_NAME);
        return modalBackgroundImage;
      }
    }

    public static UIFont Font(float size = FontSize, string familyName = FONT_NAME)
    {
      return UIFont.FromName(FONT_NAME, size);
    }

    public static UIFont FontBold(float size = FontSize)
    {
      return UIFont.FromName(FONT_BOLD_NAME, size);
    }


    public static NSMutableAttributedString Bold(string text, float size = FontSize)
    {
      var boldAttrib = new UIStringAttributes() { Font = FontBold(size) };
      return new NSMutableAttributedString(text, boldAttrib);
    }

    public static NSMutableAttributedString FontWithSize(string text, float size = FontSize)
    {
      var fontAttrib = new UIStringAttributes() { Font = UIFont.FromName(FONT_NAME, size) };
      return new NSMutableAttributedString(text, fontAttrib);
    }

  }

}

