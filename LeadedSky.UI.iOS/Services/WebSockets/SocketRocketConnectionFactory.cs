using System;
using Pusher;
using LeadedSky.UI.iOS;
using System.Diagnostics;

namespace LeadedSky.UI.iOS
{
  public class SocketRocketConnectionFactory : Pusher.IConnectionFactory
  {

    public Uri Endpoint { get; private set; }

    public static SocketRocketConnectionFactory Create(string endpoint)
    {
      return new SocketRocketConnectionFactory(){ Endpoint = new Uri(endpoint) };
    }

    public IConnection Create()
    {
      return new WebSocket(Endpoint);
    }
  }
}

