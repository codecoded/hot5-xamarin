﻿using System;
using Pusher;
using SuperSocket.ClientEngine;
using Pusher.Events;
using System.Threading.Tasks;
using MonoTouch.Foundation;

namespace LeadedSky.UI.iOS
{
  public class WS4NetWebSocket :  IConnection
  {
    readonly WebSocket4Net.WebSocket websocket;


    public WS4NetWebSocket(string endpoint)
    {

      websocket = new  WebSocket4Net.WebSocket(endpoint);

      websocket.AllowUnstrustedCertificate = true;
      websocket.Opened += (object sender, EventArgs e) => {
        if(OnOpen != null)
          OnOpen(sender, e);
      };
        
      websocket.Closed += (object sender, EventArgs e) => {
        if(OnClose != null)
          OnClose(sender, e);
      };

      websocket.MessageReceived += OnMessageReceived;

      websocket.Error += (sender, e) => {
        if(OnError == null)
          return;

        OnError(this, new ExceptionEventArgs{ Exception = e.Exception });
      };
    }

    public event EventHandler<EventArgs> OnClose;
    
    public event EventHandler<EventArgs> OnOpen;

    public event EventHandler<ExceptionEventArgs> OnError;
    //
    public event EventHandler<Pusher.Events.DataReceivedEventArgs> OnData;

    protected void OnMessageReceived(object sender, WebSocket4Net.MessageReceivedEventArgs e)
    {
      if(OnData == null)
        return;
      OnData(this, new Pusher.Events.DataReceivedEventArgs { TextData = e.Message });
    }

    public void Close()
    {
      if(websocket.State == WebSocket4Net.WebSocketState.Open)
        websocket.Close();
    }

    public Task Open()
    {
      return Task.Factory.StartNew(websocket.Open);
    }

    public Task SendMessage(string data)
    {
      return Task.Factory.StartNew(() => websocket.Send(new NSString(data)));
    }
      
  }
}

