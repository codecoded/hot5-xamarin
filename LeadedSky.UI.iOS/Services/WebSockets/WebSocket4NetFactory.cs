﻿using System;
using Pusher;
using WebSocket4Net;


namespace LeadedSky.UI.iOS
{
  public class WebSocket4NetFactory : Pusher.IConnectionFactory
  {
    public string Endpoint { get; private set; }

    public static WebSocket4NetFactory Create(string endpoint)
    {
      return new WebSocket4NetFactory(){ Endpoint = endpoint };
    }

    public IConnection Create()
    {
     
      return  new  WS4NetWebSocket(Endpoint);
    }
      
  }
}

