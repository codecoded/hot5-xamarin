using System;
using Pusher;
using Pusher.Events;
using System.Threading.Tasks;
using SocketRocket;
using MonoTouch.Foundation;
using System.Diagnostics;

namespace LeadedSky.UI.iOS
{
  public class WebSocket : IDisposable, IConnection
  {

    readonly SRWebSocket socket;

    public WebSocket(Uri endpoint)
    {
      socket = new SRWebSocket(endpoint);
      socket.MessageReceived += OnMessageReceived;

      socket.Closed += ((sender, e) => {
        if(OnClose == null)
          return;
        OnClose(this, e);
      });

      socket.Opened += ((sender, e) => {
        if(OnOpen == null)
          return;
        OnOpen(this, e);
      });

      socket.Error += ((sender, e) => {
        if(OnError == null)
          return;

        OnError(this, new ExceptionEventArgs{ Exception = new Exception(e.Err.LocalizedDescription) });
      });
    }

    public event EventHandler<EventArgs> OnClose;

    public event EventHandler<EventArgs> OnOpen;

    public event EventHandler<ExceptionEventArgs> OnError;

    public event EventHandler<Pusher.Events.DataReceivedEventArgs> OnData;

    protected void OnMessageReceived(object sender, SRMessageReceivedEventArgs e)
    {
      if(OnData == null)
        return;
      OnData(this, new Pusher.Events.DataReceivedEventArgs { TextData = e.Message.ToString() });
    }

    public void Close()
    {
      socket.Close();
    }

    public Task Open()
    {
      return Task.Factory.StartNew(socket.Open);
    }

    public Task SendMessage(string data)
    {
      return Task.Factory.StartNew(() => socket.Send(new NSString(data)));
    }

    public void Dispose()
    {
      socket.Dispose();
    }
  }
}

