﻿using System;
using MonoTouch.CoreLocation;
using MonoTouch.UIKit;

namespace LeadedSky.UI.iOS
{
  public class LocationManager
  {
    public event EventHandler<LocationUpdatedEventArgs> LocationUpdated = delegate { };

    protected CLLocationManager locManager;

    public CLLocationManager LocManager { get { return locManager; } }

    public LocationManager()
    {
      locManager = new CLLocationManager();
      locManager.PausesLocationUpdatesAutomatically = true;
      locManager.RequestAlwaysAuthorization();
//      locManager.RequestWhenInUseAuthorization();
    }

    public void StartLocationUpdates()
    {
      if(CLLocationManager.LocationServicesEnabled)
      {
        LocManager.DesiredAccuracy = 1000;

//        if(LocManager.RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("requestWhenInUseAuthorization")))
//          LocManager.RequestWhenInUseAuthorization();


        if(UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
        {
          LocManager.LocationsUpdated += (sender, e) => LocationUpdated(this, new LocationUpdatedEventArgs(e.Locations[e.Locations.Length - 1]));
        }
        else
        {
          // this won't be called on iOS 6 (deprecated). We will get a warning here when we build.
          LocManager.UpdatedLocation += (sender, e) => LocationUpdated(this, new LocationUpdatedEventArgs(e.NewLocation));
        }
      }
      LocManager.StartUpdatingLocation();
    }

  }
}

