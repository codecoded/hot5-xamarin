﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;

namespace LeadedSky.UI.iOS
{
  public static class UILabelExtensions
  {

    public static void HighlightText(this UILabel label, 
                                     string pattern, 
                                     UIStringAttributes highlightAttrib, 
                                     StringComparison stringComparison = StringComparison.CurrentCulture)
    {
      label.AttributedText = AttributeStringRange(label.AttributedText, pattern, highlightAttrib, stringComparison);
    }

    public static NSMutableAttributedString AttributeStringRange(
      NSAttributedString curString, 
      string pattern, UIStringAttributes attrib, 
      StringComparison stringComparison = StringComparison.CurrentCulture)
    {
      var attribString = new NSMutableAttributedString(curString.Value);
      var index = curString.Value.IndexOf(pattern, stringComparison);

      if(index >= 0)
        attribString.SetAttributes(attrib, new NSRange(index, pattern.Length));
      return attribString;
    }

    public static UIStringAttributes BoldAttrib()
    {
      return new UIStringAttributes {
        Font = UIFont.FromName("Arial-BoldMT", 18f)
      };
    }

    public static void SizeToFitWithAlignmentRight(this UILabel label)
    {
      var beforeFrame = label.Frame;
      label.SizeToFit();
      var afterFrame = label.Frame;
      var width = afterFrame.Size.Width + 10f;
      width = width < 80 ?80 : width;
      var x = UIScreen.MainScreen.Bounds.Width - width;
      label.Frame = new System.Drawing.RectangleF(
        x, 
        afterFrame.Location.Y, 
        width, 
        beforeFrame.Size.Height);
    }


  }
}

