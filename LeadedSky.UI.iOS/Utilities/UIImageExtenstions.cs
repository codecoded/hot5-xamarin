﻿using System;
using MonoTouch.UIKit;
using MonoTouch.CoreImage;
using System.Drawing;
using MonoTouch.CoreGraphics;
using System.Threading.Tasks;
using MonoTouch.OpenGLES;

namespace LeadedSky.UI.iOS
{
  public static class UIImageExtenstions
  {
    public static UIImage Blur(this UIImage image, float blurRadius = 25f)
    {
      if(image == null)
        return null;


      // Create a new blurred image.
      var inputImage = new CIImage(image);
      var blur = new CIGaussianBlur();
      blur.Image = inputImage;
      blur.Radius = blurRadius;

      var outputImage = blur.OutputImage;
      var context = CIContext.FromOptions(new CIContextOptions { UseSoftwareRenderer = false });
      var cgImage = context.CreateCGImage(outputImage, new RectangleF(new PointF(0, 0), image.Size));
      var newImage = UIImage.FromImage(cgImage);

      // Clean up
      inputImage.Dispose();
      context.Dispose();
      blur.Dispose();
      outputImage.Dispose();
      cgImage.Dispose();

      return newImage;
    }

    public  static UIImage Colourize(this UIImage image, UIColor color)
    {
      UIGraphics.BeginImageContext(image.Size);

      var context = UIGraphics.GetCurrentContext();
      var area = new RectangleF(0, 0, image.Size.Width, image.Size.Height);
      context.ScaleCTM(1, -1);
      context.TranslateCTM(0, -area.Size.Height);
      context.SaveState();
      context.ClipToMask(area, image.CGImage);
      context.SetFillColorWithColor(color.CGColor);
      context.FillRect(area);
      context.RestoreState();
      context.SetBlendMode(CGBlendMode.Multiply);
      context.DrawImage(area, image.CGImage);
      UIImage colorizedImage = UIGraphics.GetImageFromCurrentImageContext();
      UIGraphics.EndImageContext();

      return colorizedImage;
    }

    public static UIColor ToUIColor(this UIImage image)
    {
      return UIColor.FromPatternImage(image);
    }

    public static UIImage Vignette(this UIImage image, float intensity = 2.5f, float radius = 1f)
    {
      var beginImage = CIImage.FromCGImage(image.CGImage);

      EAGLContext myEAGLContext = new EAGLContext(EAGLRenderingAPI.OpenGLES2);
//      NSDictionary *options = @{ kCIContextWorkingColorSpace : [NSNull null] };
//      CIContext *myContext = [CIContext contextWithEAGLContext:myEAGLContext options:options];


      CIContext context = CIContext.FromContext(myEAGLContext);

//      var context = CIContext.FromOptions(new CIContextOptions(){UseSoftwareRenderer=false});

      var vignette = new CIVignette {
        Image = beginImage,
        Intensity = intensity,
        Radius = radius
      };

      var outputImage = vignette.OutputImage;

      CGImage cgimg = context.CreateCGImage(outputImage, outputImage.Extent);
      UIImage newImage = UIImage.FromImage(outputImage);
      cgimg.Dispose();
      cgimg = null;

      return newImage;
      ;
    }
  }
}

