﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreImage;
using MonoTouch.CoreGraphics;

namespace LeadedSky.UI.iOS
{
  public class ImageFilters
  {

    public static UIColor BlurImage(string filename, int radius, RectangleF size)
    {
      return UIImage.FromBundle(filename).Blur(radius).ToUIColor();
    }



  }
}

