﻿using System;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
using MonoTouch.Foundation;
using System.Drawing;

namespace LeadedSky.UI.iOS
{
  public static class NibLoader
  {
    public static T Create<T>()  where T : UIView, new()
    {
      return getViewFromNib<T>(typeof(T).Name);
    }

    public static T Create<T>(RectangleF frame)  where T : UIView, new()
    {
      return getViewFromNib<T>(typeof(T).Name, frame);
    }

    public static T Create<T>(string nibName, RectangleF frame)  where T : UIView, new()
    {
      return getViewFromNib<T>(nibName, frame);
    }

    static T getViewFromNib<T>(string nibName, RectangleF? frame = null) where T : UIView, new()
    {
      var viewInstance = new T();
      var array = NSBundle.MainBundle.LoadNib(nibName, viewInstance, null);
      var view = Runtime.GetNSObject(array.ValueAt(0)) as T;
      viewInstance.Frame = frame == null ?new RectangleF(PointF.Empty, new SizeF(view.Frame.Size)) : frame.Value;     
      viewInstance.AddSubview(view);
      return viewInstance;
    }


  }
}

