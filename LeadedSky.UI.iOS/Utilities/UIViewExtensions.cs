﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace LeadedSky.UI.iOS
{
  public static class UIViewExtensions
  {
    [Flags]
    public enum BorderFlags
    {
      Top = 1,
      Right = 2,
      Bottom = 4,
      Left = 8
    }

    public static void AddShadow(this UIView view)
    {
      var layer = view.Layer;
      layer.ShadowColor = UIColor.Black.CGColor;
      layer.ShadowOpacity = 0.8f;
      layer.ShadowRadius = 20;
      layer.ShadowOffset = new SizeF(0, 2);
    }

    public static void SetBackgroundImage(this UIView view, UIImage image)
    {
      UIImageView imageView = new UIImageView(view.Frame);
      imageView.Image = image;
      imageView.BackgroundColor = UIColor.Black.ColorWithAlpha(0.5f);
      imageView.ContentMode = UIViewContentMode.ScaleToFill;
      view.AddSubview(imageView);
      view.SendSubviewToBack(imageView);
    }

    public static void AddBorders(this UIView view, BorderFlags borderFlags, UIColor color, float marginLeft = 0, float marginRight = 0, float thickness = 1)
    {
      var width = view.Frame.Width - (marginRight + marginLeft);

      if((borderFlags & BorderFlags.Top) == BorderFlags.Top)
        view.AddBorder(color, new RectangleF(marginLeft, 0, width, thickness));

      if((borderFlags & BorderFlags.Bottom) == BorderFlags.Bottom)
        view.AddBorder(color, new RectangleF(marginLeft, view.Frame.Height - thickness, width, thickness));

      if((borderFlags & BorderFlags.Left) == BorderFlags.Left)
        view.AddBorder(color, new RectangleF(marginLeft, marginLeft, thickness, view.Frame.Height - marginRight));

      if((borderFlags & BorderFlags.Right) == BorderFlags.Right)
        view.AddBorder(color, new RectangleF(view.Frame.Width, marginLeft, thickness, view.Frame.Height - marginRight));
    }

    public static void AddBorder(this UIView view, UIColor color, RectangleF frame)
    {
      var separatorView = new UIView(frame);
      separatorView.BackgroundColor = color;
      view.AddSubview(separatorView);
    }


    public static void AdjustFrame(this UIView view, float? x = null, float? y = null, float? width = null, float?  height = null)
    {
      var f = view.Frame;
      f.X = x ?? f.X;
      f.Y = y ?? f.Y;
      f.Width = width ?? f.Width;
      f.Height = height ?? f.Height;

      view.Frame = f;
    }

    public static void RoundEdges(this UIView view, float radius)
    {
      view.ClipsToBounds = true;
      view.Layer.CornerRadius = radius;
    }
  }
}

