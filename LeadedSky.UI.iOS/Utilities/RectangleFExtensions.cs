﻿using System;
using System.Drawing;

namespace LeadedSky.UI.iOS
{
  public static class RectangleFExtensions
  {
    public static RectangleF ModX(this RectangleF frame, float newX)
    {
      return new RectangleF(new PointF(newX, frame.Y), frame.Size);
    }

    public static RectangleF ModY(this RectangleF frame, float newY)
    {
      return new RectangleF(new PointF(frame.X, newY), frame.Size);
    }

  }
}

