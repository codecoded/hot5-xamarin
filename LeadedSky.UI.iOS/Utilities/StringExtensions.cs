﻿using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace LeadedSky.UI.iOS
{
  public static class StringExtensions
  {
    public static NSMutableAttributedString ToMutAttrib(this string value)
    {
      return new NSMutableAttributedString(value);
    }

    public static NSMutableAttributedString Style(this string value, UIFont font)
    {
      var style = new UIStringAttributes() { Font = font };
      return new NSMutableAttributedString(value, style);
    }

    public static NSMutableAttributedString Concatenate(this string origString, 
                                                        NSMutableAttributedString extraString)
    {
      return origString.ToMutAttrib().Concatenate(extraString);
    }

    public static NSMutableAttributedString Concatenate(this NSMutableAttributedString origString, 
                                                        NSMutableAttributedString extraString)
    {
      origString.Append(extraString);
      return origString;
    }

    public static NSMutableAttributedString Concatenate(this NSMutableAttributedString origString, 
                                                        string extraString)
    {
      origString.Append(extraString.ToMutAttrib());
      return origString;
    }

    public static void Log(this object text, params object[] stringArgs)
    {
      if(stringArgs != null && stringArgs.Length > 0)
        Console.WriteLine(string.Format(text.ToString(), stringArgs));
      else
        Console.WriteLine(text);
    }
      

  }
}

