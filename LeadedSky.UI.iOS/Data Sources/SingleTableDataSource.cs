﻿using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using System.Drawing;

using System.Collections.Generic;
using System.Linq;
using LeadedSky.Shared;

namespace LeadedSky.UI.iOS
{
  public class SingleTableDataSource : UITableViewSource
  {
    public static NSString ItemTableCellId = new NSString("TableRowItemCellId");

    public TableRowSelectedDelegate TableRowSelected { get; set; }

    public List<TableRow> TableRows;
    public UIColor SeperatorColor;

    public SingleTableDataSource(List<TableRow> tableRows, UIColor seperatorColor)
    {
      TableRows = tableRows;
      SeperatorColor = seperatorColor;
    }

    public override int NumberOfSections(UITableView tableView)
    {
      return 1;
    }

    public override int RowsInSection(UITableView tableview, int sectionIndex)
    {
      return TableRows.Count();
    }

    public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
    {     
      var tableItem = TableRows[indexPath.Row];
      if(tableItem is TableRowHeader)
        return headerCell(tableView, indexPath, tableItem as TableRowHeader);     
      return itemCell(tableView, indexPath, tableItem as TableRowItem);     
    }

    UITableViewCell headerCell(UITableView tableView, NSIndexPath indexPath, TableRowHeader tableRowHeader)
    {  
      var cell = (TableRowHeaderCell)tableView.DequeueReusableCell(TableRowHeaderCell.Identifier, indexPath);
      cell.Title.Text = tableRowHeader.Title;
      if(cell.DetailLabel != null)
        cell.DetailLabel.Text = tableRowHeader.Detail ?? "Any";

      cell.UserInteractionEnabled = tableRowHeader.Selectable == null || tableRowHeader.Selectable.Value;
      cell.ContentView.AddBorders(UIViewExtensions.BorderFlags.Bottom, SeperatorColor, 10, 10);
      return cell;
    }

    UITableViewCell itemCell(UITableView tableView, NSIndexPath indexPath, TableRowItem tableRowItem)
    {  
      var cell = (TableRowItemCell)tableView.DequeueReusableCell(TableRowItemCell.Identifier, indexPath);
      cell.Title.Text = tableRowItem.Title;
      cell.Accessory = tableRowItem.Selected ?UITableViewCellAccessory.Checkmark : UITableViewCellAccessory.None;
      cell.AddBorders(UIViewExtensions.BorderFlags.Bottom, SeperatorColor, cell.Title.Frame.X, 10);
      return cell;
    }

    public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
    {
      var selectedTableRow = TableRows[indexPath.Row];
      TableRowSelected(selectedTableRow);
    }

    public override UIView GetViewForFooter(UITableView tableView, int section)
    {
      return new UIView(RectangleF.Empty);
    }

    public override UIView GetViewForHeader(UITableView tableView, int section)
    {
      return new UIView(RectangleF.Empty);
    }

    public override float GetHeightForHeader(UITableView tableView, int section)
    {
      return 0;
    }

  }
}

