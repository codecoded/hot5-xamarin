﻿using System;
using MonoTouch.UIKit;

namespace LeadedSky.UI.iOS
{
  public class NavBarDelegate : UINavigationBarDelegate
  {
    public UIBarPosition BarPosition { get; private set;}

    public NavBarDelegate(UIBarPosition barPositioning): base()
    {
      this.BarPosition = barPositioning;
    }

    public override UIBarPosition GetPositionForBar(IUIBarPositioning barPositioning)
    {
      return BarPosition; 
    }
     
  }
}

