using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace LeadedSky.UI.iOS
{
	partial class TableRowItemCell : UITableViewCell
	{
    public static NSString Identifier = new NSString("TableRowItemCellId");

    public UILabel Title { get { return lblTitle; } }

		public TableRowItemCell (IntPtr handle) : base (handle)
		{
		}
	}
}
