using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using LeadedSky.Shared;
using System.Linq;
using System.Text;


namespace LeadedSky.UI.iOS
{
  public partial class SingleTableView : UITableView, ITableRowSelectable
  {
    public List<TableRow> TableRows;
    List<TableRow> VisibleTableRows;

    List<TableRowHeader> TableRowHeaders { get { return TableRows.Where(e => e is TableRowHeader).Cast<TableRowHeader>().ToList(); } }

    public TableRowSelectedDelegate TableSelectionComplete { get; set; }


    public SingleTableView(IntPtr handle) : base(handle)
    {
    }

    public void Initialize(List<TableRow> tableRows, UIColor seperatorColor)
    {
      TableRows = tableRows;
      VisibleTableRows = getTableRowHeaders();
      var source = new SingleTableDataSource(VisibleTableRows, seperatorColor);
      source.TableRowSelected = TableRowSelected;
      Source = source;
    }

    List<TableRow> getTableRowHeaders()
    {
      var headers = TableRows.Where(e => e is TableRowHeader).ToList();

      if(headers.Count == 0)
        return TableRows.ToList();

      var visibleHeadersWithItems = new List<TableRow>();

      foreach(TableRowHeader header in headers)
      {
        visibleHeadersWithItems.Add(header);

        updateHeaderDetail(header);
        if(header.Expanded)
          visibleHeadersWithItems.AddRange(itemsForHeader(header));
      }
      return visibleHeadersWithItems;
    }

    public void TableRowSelected(TableRow tableRow)
    {
      BeginUpdates();
      if(tableRow is TableRowHeader)
        tableRowHeaderSelected(tableRow as TableRowHeader);
      else if(tableRow is TableRowItem)
        tableRowItemSelected(tableRow as TableRowItem);
      EndUpdates();

      if(TableSelectionComplete != null)
        TableSelectionComplete(tableRow);
    }

    void tableRowHeaderSelected(TableRowHeader tableRowHeader)
    {
      if(!tableRowHeader.Expanded)
      {
        expandHeader(tableRowHeader);
      }
      else
      {
        collapseHeader(tableRowHeader);
      }

      tableRowHeader.Expanded = !tableRowHeader.Expanded;
    }

    void expandHeader(TableRowHeader tableRowHeader)
    {
      collapseAll();

      var childNodes = itemsForHeader(tableRowHeader);
      var indexOfHeader = VisibleTableRows.IndexOf(tableRowHeader) + 1;
      var indexPaths = getIndexPaths(indexOfHeader, childNodes.Count);

      VisibleTableRows.InsertRange(indexOfHeader, childNodes);
      InsertRows(indexPaths, UITableViewRowAnimation.Bottom);
    }

    void collapseHeader(TableRowHeader tableRowHeader)
    {
      var childNodes = itemsForHeader(tableRowHeader);
      var indexOfHeader = VisibleTableRows.IndexOf(tableRowHeader) + 1;
      var indexPaths = getIndexPaths(indexOfHeader, childNodes.Count);

      VisibleTableRows.RemoveRange(indexOfHeader, childNodes.Count);
      DeleteRows(indexPaths, UITableViewRowAnimation.Top);
    }

    NSIndexPath[] getIndexPaths(int start, int count)
    {
      return Enumerable.Range(start, count).Select(e => NSIndexPath.FromRowSection(e, 0)).ToArray();
    }

    void collapseAll()
    {        
      var expandedRows = VisibleTableRows.Select(e => e as TableRowHeader).Where(e => e != null && e.Expanded).ToList();
      if(expandedRows.Count > 0)
        expandedRows.ForEach(e => tableRowHeaderSelected(e));
    }

    void tableRowItemSelected(TableRowItem tableRowItem)
    {
      var indexPaths = new List<NSIndexPath>();

      tableRowItem.Selected = !tableRowItem.Selected;

      var header = TableRowHeaders.Find(e => e.Value == tableRowItem.HeaderValue);

      indexPaths.Add(getIndexPath(header));
      indexPaths.Add(getIndexPath(tableRowItem));

      updateHeaderDetail(header);

      if(tableRowItem.IsSingleSelection)
        indexPaths.AddRange(updateRadioSelection(tableRowItem));

      ReloadRows(indexPaths.ToArray(), UITableViewRowAnimation.Fade);
    }

    List<NSIndexPath> updateRadioSelection(TableRowItem tableRowItem)
    {
      var indexPaths = new List<NSIndexPath>();
      var rowItemGroup = VisibleTableRows
        .Select(e => e as TableRowItem)
        .Where(e => e != null && (e.HeaderValue == tableRowItem.HeaderValue && e.SelectType == TableRowItemSelectType.Radio) && e != tableRowItem)
        .ToList();

      foreach(var item in rowItemGroup)
      {
        item.Selected = false;
        indexPaths.Add(getIndexPath(item));
      }

      return indexPaths;
    }

    void updateHeaderDetail(TableRowHeader header)
    {
      var items = itemsForHeader(header);
      var selected = items.Where(e=>e.Selected).Select(e=>e.Title).ToList();

      if(selected.Count == items.Count)
        header.Detail = header.ChoiceType.ToString();
      else if(selected.Count > 0)
        header.Detail = string.Join(", ", selected);
      else
        header.Detail = "Any";
    }

    NSIndexPath getIndexPath(TableRow tableRow)
    {
      var indexOfItem = VisibleTableRows.IndexOf(tableRow);
      return NSIndexPath.FromRowSection(indexOfItem, 0);
    }

    List<TableRowItem> itemsForHeader(TableRowHeader tableRowHeader)
    {
      return TableRows
        .Select(e => e as TableRowItem)
        .Where(e => e != null && e.HeaderValue == tableRowHeader.Value)
        .ToList();
    }
  }
}
