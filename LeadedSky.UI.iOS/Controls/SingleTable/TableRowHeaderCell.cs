using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;
using LeadedSky.Shared;

namespace LeadedSky.UI.iOS
{
	partial class TableRowHeaderCell : UITableViewCell
	{
    public static NSString Identifier = new NSString("TableRowHeaderCellId");

    public UILabel Title { get { return lblTitle; } }
    public UILabel DetailLabel { get { return lblDetail; } }
    public UIImage Icon { get { return imgIcon.Image; } }

		public TableRowHeaderCell (IntPtr handle) : base (handle)
		{
		}

	}
}
