﻿using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace LeadedSky.UI.iOS
{
  public class LoadingOverlay : UIView
  {
    // control declarations
    UIActivityIndicatorView activitySpinner;
    public UILabel LoadingLabel;
    public bool Hiding = false;

    public string Title
    { 
      get { return LoadingLabel.Text; } 
      set { LoadingLabel.Text = value; }
    }

    public LoadingOverlay(RectangleF frame) : base(frame)
    {
      // configurable bits
      BackgroundColor = UIColor.Black.ColorWithAlpha(0.50f);
      //Alpha = 0.75f;
      //AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;

      float labelHeight = 22;
      float labelWidth = frame.Width - 20;

      // derive the center x and y
      float centerX = frame.Width / 2;
      float centerY = frame.Height / 2;

      // create the activity spinner, center it horizontall and put it 5 points above center x
      activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
      activitySpinner.Frame = new RectangleF(
        centerX - (activitySpinner.Frame.Width / 2),
        centerY - activitySpinner.Frame.Height - 20,
        activitySpinner.Frame.Width,
        activitySpinner.Frame.Height);
      activitySpinner.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
      AddSubview(activitySpinner);
      activitySpinner.StartAnimating();

      // create and configure the "Loading Data" label
      LoadingLabel = new UILabel(new RectangleF(
        centerX - (labelWidth / 2),
        centerY + 20,
        labelWidth,
        labelHeight
      ));
      LoadingLabel.BackgroundColor = UIColor.Clear;
      LoadingLabel.TextColor = UIColor.White;
      //LoadingLabel.Text = Title;
      LoadingLabel.TextAlignment = UITextAlignment.Center;
      LoadingLabel.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
      AddSubview(LoadingLabel);
    }

    /// <summary>
    /// Fades out the control and then removes it from the super view
    /// </summary>
    public void Hide()
    {
      Hiding = true;
      UIView.Animate(
        0.1, // duration
        () => {
          Alpha = 0;
        },
        () => { 
          RemoveFromSuperview(); 
          Hiding = false;
        }
      );
    }
  };
}

