﻿using NUnit.Framework;
using System;
using Hot5.Core;

using LeadedSky.Shared;
using System.IO;
using System.Collections.Generic;

namespace Hot5.Tests
{
  [TestFixture()]
  public class SearchTableViewTest
  {
    public SearchResponse Response
    {
      get { return JsonSearchResponseParseTest.Response; }
    }

    private SearchResponseView view;
    public SearchResponseView View
    {
      get
      {
        if(view == null)
          view = new SearchResponseView(Response);
        return view;
      }
    }

    [Test()]
    public void CanTakeSearchResponseConstructor()
    {
      Assert.IsNotNull(View);
      Assert.IsNotNull(View.SearchResponse);
    }

    [Test()]
    public void CanCreateSearchResultDictionary()
    {
      Assert.IsNotNull(View.Sections);
      Assert.IsNotEmpty(View.Sections);
      Assert.AreEqual(5, View.Sections.Count);
    }

    [Test()]
    public void CanIndexSectionsDictionaryByKey()
    {
      Assert.IsNotNull(View.Sections);

      Assert.DoesNotThrow(() => {
        IndexSection("city");
      });

      Assert.DoesNotThrow(delegate {
        IndexSection("hotel"); 
      });
        
      Assert.Throws(typeof(KeyNotFoundException), delegate {
        IndexSection("malformed");
      });

      Assert.Throws(typeof(KeyNotFoundException), delegate {
        IndexSection("Hotels");
      });

    }

    void IndexSection(string term)
    {
      var s = View.Sections[term];
    }

    [Test()]
    public void CanIndexSectionsByTerm()
    {
      Assert.IsNotNull(View.Sections);

      Assert.DoesNotThrow(delegate {
        View.GetSectionFor("city");
      });

      Assert.IsNotNull(View.GetSectionFor("city"));
      Assert.IsNull(View.GetSectionFor("country"));

      Assert.DoesNotThrow(delegate {
        View.GetSectionFor("country");
      });

      Assert.DoesNotThrow(delegate {
        View.GetSectionFor("malformed");
      });

    }

    [Test()]
    public void CanParseCitySearchResults()
    {
      var searchResults = View.Sections["city"];
      Assert.IsNotNull(searchResults);
      Assert.IsNotEmpty(searchResults);

      Assert.AreEqual(2, searchResults.Count);
    }

    [Test()]
    public void CanParseCitySearchResult()
    {
      CanParseCitySearchResults();
      var searchResult = View.Sections["city"][1];
      Assert.IsNotNull(searchResult);
      Assert.AreEqual(1847510, searchResult.Id);
      Assert.AreEqual("London", searchResult.Term);
      Assert.AreEqual(1956, searchResult.Score);
      Assert.IsNotNull(searchResult.Data);
      Assert.AreEqual("london-hotels", searchResult.Data.Slug);

    }

  }
}

