﻿using System;
using Hot5.Core;
using LeadedSky.Shared;
using NUnit.Framework;
using System.IO;

namespace Hot5.Tests
{
  public class JsonSearchResponseParseTest
  {
    public const string JsonSearchResponse = @"{
  ""term"": ""lo"",
  ""results"": {
    ""city"": [
      {
        ""id"": 1848300,
        ""term"": ""Louveciennes"",
        ""score"": 2756,
        ""data"": {
          ""slug"": ""louveciennes-hotels"",
          ""title"": ""Louveciennes, France""
        }
      },
      {
        ""id"": 1847510,
        ""term"": ""London"",
        ""score"": 1956,
        ""data"": {
          ""slug"": ""london-hotels"",
          ""title"": ""London, England, United Kingdom""
        }
      }
    ],
    ""region"": [
    ],
    ""hotel"": [
      {
        ""id"": 94504,
        ""term"": ""Hotel Tugu Lombok"",
        ""score"": 3822,
        ""data"": {
          ""slug"": ""hotel-tugu-lombok"",
          ""title"": ""Hotel Tugu Lombok, Tanjung""
        }
      },
      {
        ""id"": 15747,
        ""term"": ""The Stafford London, by Kempinski"",
        ""score"": 3822,
        ""data"": {
          ""slug"": ""the-stafford-london,-by-kempinski"",
          ""title"": ""The Stafford London, by Kempinski, London""
        }
      },
      {
        ""id"": 174348,
        ""term"": ""Solitaire Lodge"",
        ""score"": 3636,
        ""data"": {
          ""slug"": ""solitaire-lodge"",
          ""title"": ""Solitaire Lodge, Buried village""
        }
      }
    ],
    ""place"": [
      {
        ""id"": 1848306,
        ""term"": ""Louvre - Place Vendome"",
        ""score"": 3041,
        ""data"": {
          ""slug"": ""louvre---place-vendome"",
          ""title"": ""Louvre - Place Vendome (1 arr. and 2 arr.), Paris, France""
        }
      },
      {
        ""id"": 1831957,
        ""term"": ""Ile Saint Louis"",
        ""score"": 3036,
        ""data"": {
          ""slug"": ""ile-saint-louis"",
          ""title"": ""Ile Saint Louis, Paris, France""
        }
      }
    ],
    ""landmark"": [
      {
        ""id"": 1841117,
        ""term"": ""La Carrousel du Louvre"",
        ""score"": 3034,
        ""data"": {
          ""slug"": ""la-carrousel-du-louvre"",
          ""title"": ""La Carrousel du Louvre, Paris, France""
        }
      }
    ]
  }
}";

    private static SearchResponse response;
    public static SearchResponse Response
    {
      get
      {
        if(response == null)
          response = JsonMarshaller.Deserialise<SearchResponse>(JsonSearchResponse);
        return response;
      }
    }

    public SearchResponse fileResponse
    {
      get
      {
        return JsonMarshaller.Deserialise<SearchResponse>(File.ReadAllText("search.json"));
      }
    }

    [Test()]
    public void CanParseResponseUsingJsonObject()
    {
      Assert.IsNotNull(Response);
      Assert.AreEqual("lo", Response.Term);
    }

    [Test()]
    public void CanParseFileResponseUsingJsonObject()
    {
      Assert.IsNotNull(fileResponse);
      Assert.AreEqual("lo", fileResponse.Term);
      Assert.IsNotNull(fileResponse.Results);
      Assert.IsNotNull(fileResponse.Results.Hotel);
      Assert.IsNotNull(fileResponse.Results.Place);
      Assert.AreEqual(7, fileResponse.Results.Hotel.Count);
      Assert.AreEqual(7, fileResponse.Results.Place.Count);


    }

    [Test()]
    public void CanParseSearchResultsUsingJsonObject()
    {
      Assert.IsNotNull(Response);
      var results = Response.Results;

      Assert.IsNotNull(results);
      Assert.IsNotNull(results.City);
      Assert.IsNotNull(results.Place);
      Assert.IsNotNull(results.Hotel);
      Assert.IsNotNull(results.Region);
      Assert.IsNotNull(results.Landmark);

      Assert.IsNull(results.Country);
      Assert.AreEqual(2, results.City.Count);
      Assert.IsEmpty(results.Region);

    }

    [Test()]
    public void CanParseHotelSearchResultUsingJsonObject()
    {
      Assert.IsNotNull(Response);
      var results = Response.Results;
      Assert.IsNotNull(results);
      var hotelResult = results.Hotel;
      Assert.IsNotNull(hotelResult);
      Assert.AreEqual(3, hotelResult.Count);
    }

    [Test()]
    public void CanParsePlaceSearchResultUsingJsonObject()
    {
      Assert.IsNotNull(Response);
      var results = Response.Results;
      Assert.IsNotNull(results);
      var placeResult = results.Place;
      Assert.IsNotNull(placeResult);
      Assert.AreEqual(2, placeResult.Count);
    }

    [Test()]
    public void CanParseSearchResultUsingJsonObject()
    {
      CanParsePlaceSearchResultUsingJsonObject();
      var searchResult = Response.Results.Place[1];
      Assert.IsNotNull(searchResult);
      Assert.AreEqual(1831957, searchResult.Id);
      Assert.AreEqual("Ile Saint Louis", searchResult.Term);
      Assert.AreEqual(3036, searchResult.Score);
      Assert.IsNotNull(searchResult.Data);
    }

    [Test()]
    public void CanParseSearchResultDataUsingJsonObject()
    {
      CanParseSearchResultUsingJsonObject();
      var searchResultData = Response.Results.Place[1].Data;
      Assert.IsNotNull(searchResultData);
      Assert.AreEqual("ile-saint-louis", searchResultData.Slug);
      Assert.AreEqual("Ile Saint Louis, Paris, France", searchResultData.Title);
    }
  }
}

