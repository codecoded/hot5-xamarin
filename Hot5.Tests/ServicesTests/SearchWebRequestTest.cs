﻿using NUnit.Framework;
using System;
using System.Net;
using Hot5.Core;
using LeadedSky.Shared;

using RestSharp;
using System.Threading;

namespace Hot5.Tests
{
  [TestFixture()]
  public class SearchWebRequestTest
  {


    [Test()]
    public void CanMakeInitialSearchRequest()
    {
//
      var searchTerm = "lon";
//      var limit = 3;
//      var types = new String[]{"city", "hotel"};
//

      ApiClient.Search(searchTerm, ProcessResponse); 
      //Assert.IsNotNull(response);

    }

    void ProcessResponse(SearchResponse searchResponse)
    {
      Assert.IsInstanceOf<SearchResponse>(searchResponse);
      Assert.AreEqual("lon", searchResponse.Term);
      Assert.AreEqual(7, searchResponse.Results.City.Count);
    }
  }
}

