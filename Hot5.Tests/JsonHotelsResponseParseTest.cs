﻿using NUnit.Framework;
using System;
using Hot5.Core;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using LeadedSky.Shared;

namespace Hot5.Tests
{
  [TestFixture()]
  public class JsonHotelsResponseParseTest
  {

    public const string JsonHotelsResponse = @"
{
  ""info"": {
    ""query"": ""Norwich"",
    ""slug"": ""norwich-hotels"",
    ""channel"": ""hot5-com-2014-07-03-2014-07-04-gbp-norwich-hotels"",
    ""key"": ""f72606ca633d10676d35d5dd36dd61da"",
    ""sort"": ""recommended"",
    ""total_hotels"": 107,
    ""available_hotels"": 57,
    ""min_price"": 35,
    ""max_price"": 145,
    ""min_price_filter"": 25,
    ""max_price_filter"": null,
    ""price_values"": [
      40,
      50,
      55,
      60,
      65,
      70,
      75,
      80,
      85,
      90,
      95,
      100,
      110,
      115,
      120,
      125,
      140,
      145
    ],
    ""star_ratings"": null,
    ""amenities"": null,
    ""longitude"": 1.29426,
    ""latitude"": 52.628548,
    ""zoom"": 10,
    ""page_size"": 15
  },
  ""criteria"": {
    ""start_date"": ""2014-07-03"",
    ""end_date"": ""2014-07-04"",
    ""total_nights"": 1,
    ""currency_code"": ""GBP"",
    ""currency_symbol"": ""\u00a3"",
    ""country_code"": ""GB""
  },
  ""state"": ""finished"",
  ""hotels"": [
    {
      ""id"": 14614,
      ""name"": ""Holiday Inn Norwich City"",
      ""address"": ""Carrow Road"",
      ""city"": ""norwich"",
      ""state_province"": ""norfolk"",
      ""postal_code"": ""NR1 1HU"",
      ""latitude"": 52.622081468298,
      ""longitude"": 1.3110087811947,
      ""star_rating"": 4,
      ""description"": ""<p><b>Property Location<\/b> <br \/>Holiday Inn Norwich City is centrally located in Norwich, walking distance from Carrow Road and close to Dragon Hall. This hotel is within close proximity of Norwich Castle and Market Place.<\/p><p><b>Rooms<\/b> <br \/>Make yourself at home in one of the 150 air-conditioned guestrooms. High-speed (wired) Internet access (surcharge) keeps you connected, and cable programming is available for your entertainment. Bathrooms have bathtubs and hair dryers. Conveniences include direct-dial phones, as well as desks and coffee\/tea makers.<\/p><p><b>Rec, Spa, Premium Amenities<\/b> <br \/>Enjoy the recreation opportunities such as a fitness facility or make use of other amenities including complimentary wireless Internet access.<\/p><p><b>Dining<\/b> <br \/>Satisfy your appetite at the hotel's restaurant, which serves breakfast, lunch, and dinner, or stay in and take advantage of 24-hour room service. Quench your thirst with your favorite drink at a bar\/lounge.<\/p><p><b>Business, Other Amenities<\/b> <br \/>Featured amenities include a 24-hour business center, audiovisual equipment, and currency exchange. Self parking is available onsite.<\/p>"",
      ""amenities"": 543,
      ""slug"": ""holiday-inn-norwich-city"",
      ""offer"": {
        ""provider"": ""easy_to_book"",
        ""link"": ""http:\/\/www.easytobook.com\/hotel_proxy.php?hotel_id=25759&lang=en&arrival=03-07-2014&departure=04-07-2014&currency=GBP&prs_arr%5B0%5D=2&amu=280828334&utm_source=Broadbase+Ventures+Ltd&utm_medium=affiliate&utm_term=Norwich&utm_content=etb4&utm_campaign=en"",
        ""min_price"": 109,
        ""max_price"": 109,
        ""saving"": 0
      },
      ""ratings"": {
        ""overall"": 88,
        ""agoda"": 0,
        ""booking"": 87,
        ""splendia"": 0,
        ""laterooms"": 0,
        ""easy_to_book"": 90
      },
      ""main_image"": {
        ""image_url"": ""http:\/\/media.expedia.com\/hotels\/2000000\/1710000\/1700800\/1700787\/1700787_157_b.jpg"",
        ""thumbnail_url"": ""http:\/\/media.expedia.com\/hotels\/2000000\/1710000\/1700800\/1700787\/1700787_157_t.jpg""
      },
      ""providers"": [
        {
          ""provider"": ""easy_to_book"",
          ""provider_hotel_id"": 25759,
          ""room_count"": 6,
          ""min_price"": 109,
          ""max_price"": 134,
          ""ranking"": 2,
          ""link"": ""http:\/\/www.easytobook.com\/hotel_proxy.php?hotel_id=25759&lang=en&arrival=03-07-2014&departure=04-07-2014&currency=GBP&prs_arr%5B0%5D=2&amu=280828334&utm_source=Broadbase+Ventures+Ltd&utm_medium=affiliate&utm_term=Norwich&utm_content=etb4&utm_campaign=en"",
          ""loaded"": true
        },
        {
          ""provider"": ""expedia"",
          ""provider_hotel_id"": 1700787,
          ""room_count"": 6,
          ""min_price"": 109,
          ""max_price"": 134,
          ""ranking"": ""2"",
          ""link"": ""http:\/\/clkuk.tradedoubler.com\/click?p=21874&a=2351254&g=952779&url=http:\/\/www.expedia.co.uk\/pubspec\/scripts\/eap.asp?GOTO=HOTDETAILS&Indate=03\/07\/2014&Outdate=04\/07\/2014&NumAdult=2&Numroom=1&HotId=1700787&tabtype=0"",
          ""loaded"": true
        },
        {
          ""provider"": ""booking"",
          ""provider_hotel_id"": 34639,
          ""room_count"": 6,
          ""min_price"": 109,
          ""max_price"": 134,
          ""link"": ""http:\/\/www.booking.com\/hotel\/gb\/holiday-inn-norwich-city.html?aid=371919&label=hotel-34639&utm_source=hot5&utm_medium=SPPC&utm_content=search&utm_campaign=en&utm_term=hotel-34639&lang=en&checkin=2014-07-03&checkout=2014-07-04&selected_currency=GBP"",
          ""loaded"": true
        }
      ],
      ""channel"": ""hot5-com-2014-07-03-2014-07-04-gbp-14614""
    },
    {
      ""id"": 24315,
      ""name"": ""Stracey Hotel"",
      ""address"": ""2 Stracey Road"",
      ""city"": ""norwich"",
      ""state_province"": ""norfolk"",
      ""postal_code"": ""NR1 1EZ"",
      ""latitude"": 52.626466227269,
      ""longitude"": 1.311702132225,
      ""star_rating"": 3,
      ""description"": ""<p><b>Property Location<\/b> <br \/>With a stay at Stracey Hotel in Norwich, you'll be minutes from Carrow Road and close to University of East Anglia. This hotel is within close proximity of Norwich Cathedral and Norwich Castle.<\/p><p><b>Rooms<\/b> <br \/>Make yourself at home in one of the 20 guestrooms. Conveniences include phones, as well as safes and coffee\/tea makers.<\/p><p><b>Rec, Spa, Premium Amenities<\/b> <br \/>Take in the views from a garden and make use of amenities such as tour\/ticket assistance.<\/p><p><b>Dining<\/b> <br \/>Enjoy a satisfying meal at a restaurant serving guests of Stracey Hotel. Quench your thirst with your favorite drink at a bar\/lounge.<\/p><p><b>Business, Other Amenities<\/b> <br \/>The front desk is staffed during limited hours.<\/p>"",
      ""amenities"": 518,
      ""slug"": ""stracey-hotel"",
      ""offer"": {
        ""provider"": ""booking"",
        ""link"": ""http:\/\/www.booking.com\/hotel\/gb\/stracey.html?aid=371919&label=hotel-48237&utm_source=hot5&utm_medium=SPPC&utm_content=search&utm_campaign=en&utm_term=hotel-48237&lang=en&checkin=2014-07-03&checkout=2014-07-04&selected_currency=GBP"",
        ""min_price"": 99,
        ""max_price"": 139,
        ""saving"": 28.776978417266
      },
      ""ratings"": {
        ""overall"": 86,
        ""agoda"": 88,
        ""booking"": 85,
        ""splendia"": 0,
        ""laterooms"": 0,
        ""easy_to_book"": 0
      },
      ""main_image"": {
        ""image_url"": ""http:\/\/media.expedia.com\/hotels\/3000000\/2880000\/2876500\/2876460\/2876460_45_b.jpg"",
        ""thumbnail_url"": ""http:\/\/media.expedia.com\/hotels\/3000000\/2880000\/2876500\/2876460\/2876460_45_t.jpg""
      },
      ""providers"": [
        {
          ""provider"": ""booking"",
          ""provider_hotel_id"": 48237,
          ""room_count"": 2,
          ""min_price"": 99,
          ""max_price"": 139,
          ""link"": ""http:\/\/www.booking.com\/hotel\/gb\/stracey.html?aid=371919&label=hotel-48237&utm_source=hot5&utm_medium=SPPC&utm_content=search&utm_campaign=en&utm_term=hotel-48237&lang=en&checkin=2014-07-03&checkout=2014-07-04&selected_currency=GBP"",
          ""loaded"": true
        },
        {
          ""provider"": ""agoda"",
          ""provider_hotel_id"": 161125,
          ""room_count"": 1,
          ""min_price"": 139,
          ""max_price"": 139,
          ""ranking"": 0,
          ""link"": ""http:\/\/www.agoda.com\/partners\/xml2012_landing.aspx?siteid=1620684&currency=GBP&asq=yxCf1QvOC%2bF1AKLpuLv6E4xFpXW%2bnIxeuP2%2bZR%2frRHyDMTxEYkK4YNPB2X5r%2f0vhJ71RzmxoWjOlLHSXapkyMGYTV%2bK2fH1Vo5Euyg4fZYHW%2fNhrUBUs0UCCgIDeIk%2fvdmxOfWDbsW115m5VEHA0Iz0KegnuMPFzB6Z7OF1ba%2fQtPuTgSeSDaCPypXbK1%2fVDKz0y0YA4uv%2bCZaGDg7ZUKCbTwGh0GDYNuaH0H0RU8jc4ggZ8dqFz906l%2bqTfD%2bVyvH9sWc5jRphSpvHG0Id5hpdBASqdJuxrXGvrZWoUXyN85awlJ%2bMSSoVGMZ45cOjq4vYBSd86EVFMQNW14nE%2fIg%3d%3d&tag=hid161125"",
          ""loaded"": true
        },
        {
          ""provider"": ""easy_to_book"",
          ""loaded"": false
        },
        {
          ""provider"": ""expedia"",
          ""loaded"": false
        }
      ],
      ""channel"": ""hot5-com-2014-07-03-2014-07-04-gbp-24315""
    }
  ]
}";

    public HotelsResponse response
    {
      get
      {
        return JsonMarshaller.Deserialise<HotelsResponse>(JsonHotelsResponse);
      }
    }

    public HotelsResponse fileResponse
    {
      get
      {
        return JsonMarshaller.Deserialise<HotelsResponse>(File.ReadAllText("hotels.json"));
      }
    }

    [Test()]
    public void CanParseResponseFileUsingJsonObject()
    {
      Assert.IsNotNull(fileResponse);
      Assert.IsNotNull(fileResponse.Hotels);
      Assert.IsNotNull(fileResponse.Info);
      Assert.IsNotNull(fileResponse.Criteria);
    }

    [Test()]
    public void CanParseResponseUsingJsonObject()
    {
      Assert.IsNotNull(response);
      Assert.AreEqual(SearchState.Finished, response.State);
    }

    [Test()]
    public void CanParseInfoUsingJsonObject()
    {
      Assert.IsNotNull(response.Info);
      Assert.AreEqual("Norwich", response.Info.Query);
      Assert.IsNull(response.Info.StarRatings);
      Assert.AreEqual(18, response.Info.PriceValues.Length);
      Assert.AreEqual(1.29426f, response.Info.Longitude.Value);
      Assert.AreEqual("recommended", response.Info.Sort);
    }

    [Test()]
    public void CanParseCriteriaUsingJsonObject()
    {
      Assert.IsNotNull(response.Criteria);
      Assert.AreEqual("GBP", response.Criteria.CurrencyCode);
      Assert.AreEqual(new DateTime(2014, 07, 03), response.Criteria.StartDate);
    }

    [Test()]
    public void CanParseHotelsUsingJsonObject()
    {
      Assert.IsNotNull(response.Hotels);
      Assert.AreEqual(2, response.Hotels.Count);
      var hotel = response.Hotels[1];
      Assert.AreEqual(24315, hotel.Id);
      Assert.AreEqual(52.626466227269f, hotel.Latitude);
      Assert.AreEqual("stracey-hotel", hotel.Slug);
      Assert.IsNotNull(hotel.Offer);
      Assert.IsNotNull(hotel.Ratings);
      Assert.IsNotNull(hotel.MainImage);
      Assert.IsNotNull(hotel.Providers);
      Assert.AreEqual(4, hotel.Providers.Count);
    }

    [Test()]
    public void CanParseHeroOffersUsingJsonObject()
    {
      var hotel = GetHotelAt(1);
      var offer = hotel.Offer;

      Assert.IsNotNull(offer);
      Assert.AreEqual("booking", offer.Provider);
      Assert.IsNotNull(offer.Link);
      Assert.AreEqual(28.776978417266f, offer.Saving);
    }

    [Test()]
    public void CanParseRatingsUsingJsonObject()
    {
      var hotel = GetHotelAt(1);
      var ratings = hotel.Ratings;

      Assert.IsNotNull(ratings);
      Assert.AreEqual(86, ratings.Overall);
      Assert.AreEqual(0, ratings.EasyToBook);
    }

    [Test()]
    public void CanParseMainImageUsingJsonObject()
    {
      var hotel = GetHotelAt(1);
      var mainImage = hotel.MainImage;

      Assert.IsNotNull(mainImage);
      Assert.AreEqual(@"http://media.expedia.com/hotels/3000000/2880000/2876500/2876460/2876460_45_b.jpg", mainImage.ImageUrl);
      Assert.AreEqual(@"http://media.expedia.com/hotels/3000000/2880000/2876500/2876460/2876460_45_t.jpg", mainImage.ThumbnailUrl);
    }

    [Test()]
    public void CanParseProvidersUsingJsonObject()
    {
      var hotel = GetHotelAt(1);
      var providers = hotel.Providers;

      Assert.IsNotNull(providers);
      Assert.AreEqual(4, providers.Count);
    }

    [Test()]
    public void CanParseProviderUsingJsonObject()
    {
      var hotel = GetHotelAt(1);
      var providers = hotel.Providers;
      Assert.IsNotNull(providers);

      var provider = providers[1];
      Assert.IsNotNull(provider);

      Assert.AreEqual("agoda", provider.Provider);
      Assert.AreEqual(161125, provider.ProviderHotelId);
      Assert.IsNotNull(provider.Link);
      Assert.AreEqual(139, provider.MinPrice);
      Assert.AreEqual(true, provider.Loaded);

    }

    private Hotel GetHotelAt(int index)
    {
      var hotel = response.Hotels[index];
      Assert.IsNotNull(hotel);
      return hotel;
    }

  }
}

