﻿using System;
using Newtonsoft.Json;
using Xamarin;

namespace LeadedSky.Shared
{
  public class JsonMarshaller
  {
    public static T Deserialise<T>(string json, JsonSerializerSettings settings = null)
    {
      if(string.IsNullOrEmpty(json))
        return default(T);
      settings = settings ?? JsonMarshallerSettings.Default;
      try
      {
        return JsonConvert.DeserializeObject<T>(json, settings);
      }
      catch(Exception ex)
      {
        Insights.Report(ex);
        return default(T);
      }
    }

    public static string Serialise(object data, JsonSerializerSettings settings = null)
    {
      settings = settings ?? JsonMarshallerSettings.Default;
      return JsonConvert.SerializeObject(data, settings);
    }
  }
}

