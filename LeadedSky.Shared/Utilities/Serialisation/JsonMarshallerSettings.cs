﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LeadedSky.Shared
{
  public class JsonMarshallerSettings
  {
    public static JsonSerializerSettings Default { 
      get
      {
        return new JsonSerializerSettings { 
          MissingMemberHandling = MissingMemberHandling.Ignore,
          NullValueHandling = NullValueHandling.Ignore,
          ContractResolver = new SnakeCasePropertyNamesContractResolver()
        };
      }
    }
  }
}

