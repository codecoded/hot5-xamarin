﻿using System.Collections.Generic;
using System.Linq;

namespace LeadedSky.Shared
{
  public interface IPagingEventsListener
  {
    void HandlePageChanged(int currentPage);

  }

  public class PagingCoordinator
  {
    public List<IPagingEventsListener> Listeners;

    public PagingCoordinator()
    {
      Listeners = new List<IPagingEventsListener>();
    }

    public void AddListener(IPagingEventsListener listener)
    {
      if(Listeners.Contains(listener))
        return;
      Listeners.Add(listener);
    }

    public void RemoveListener(IPagingEventsListener listener)
    {
      Listeners.Remove(listener);
    }

    public void NotifyPageChanged(IPagingEventsListener sender, int currentPage)
    {
      foreach(var listener in Listeners.Where(e => e != sender))
        listener.HandlePageChanged(currentPage);
    }

  }
}

