﻿using System.Collections.Generic;
using System.Linq;
using System.Drawing;

namespace LeadedSky.Shared
{
  public interface IScrollingEventsListener
  {
    void HandleScrolled(object sender);

  }

  public class ScrollingCoordinator
  {

    public List<IScrollingEventsListener> Listeners;

    public ScrollingCoordinator()
    {
      Listeners = new List<IScrollingEventsListener>();
    }

    public void AddListener(IScrollingEventsListener listener)
    {
      if(Listeners.Contains(listener))
        return;
      Listeners.Add(listener);
    }

    public void RemoveListener(IScrollingEventsListener listener)
    {
      Listeners.Remove(listener);
    }

    public void NotifyScrolled(object sender)
    {
      foreach(var listener in Listeners.Where(e => e != sender))
        listener.HandleScrolled(sender);
    }

  }
}

