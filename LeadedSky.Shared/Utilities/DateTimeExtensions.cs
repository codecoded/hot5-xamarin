﻿using System;

namespace LeadedSky.Shared
{
  public static class DateTimeExtensions
  {
    public static DateTime Now { get { return DateTime.Now; } }
    public static DateTime Today  { get { return Now.Date; } }
    public static DateTime Tomorrow  { get { return Today.AddDays(1); } }


    public static bool CurrentMonth(this DateTime value)
    {
      return value.Month == Now.Month;
    }

    public static bool PastDate(this DateTime value)
    {
      return  value.Date < Today;
    }

    public static bool FutureDate(this DateTime value)
    {
      return  value.Date > Today;
    }

    public static bool PresentDate(this DateTime value)
    {
      return  value.Date == Today;
    }

    public static bool FirstDayOfMonth(this DateTime value)
    {
      return  value.Date.Day == 1;
    }

    public static bool WithinDateRange(this DateTime value, DateRange dateRange)
    {
      return dateRange.Contains(value);
    }

    public static bool OutsideDateRange(this DateTime value, DateRange dateRange)
    {
      return !value.WithinDateRange(dateRange);
    }

  }
}

