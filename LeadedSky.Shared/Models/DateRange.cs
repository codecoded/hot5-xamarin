﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace LeadedSky.Shared
{
  public class DateRange
  {
    public static DateTime Now { get { return DateTime.Now; } }

    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }
    public TimeSpan Difference { get { return EndDate - StartDate; } }
    public int TotalDays { get { return (int)Difference.TotalDays; } }

    private List<DateTime> days;

    public List<DateTime> Days
    {
      get
      {
        if(days == null) populateDays();
        return days;
      }
    }

    public DateRange(DateTime startDate) : this(startDate, startDate)
    {
    }

    public DateRange(DateTime startDate, DateTime endDate)
    {
      this.StartDate = startDate.Date;
      this.EndDate = endDate.Date;
    }


    public static DateRange Today()
    {
      return new DateRange(Now, Now);
    }

    public static DateRange DaysFromNow(int days)
    {
      return new DateRange(Now, Now.AddDays(days));
    }

    public DateRange ByDaysFromStartDate(int days)
    {
      return new DateRange(StartDate, StartDate.AddDays(days));
    }

    public static DateRange WeeksFromNow(int weeks)
    {
      return DaysFromNow(weeks * 7);
    }

    public static DateRange MonthsFromNow(int months)
    {
      return new DateRange(Now, Now.AddMonths(months));
    }

    public static DateRange YearsFromNow(int years)
    {
      return new DateRange(Now, Now.AddYears(years));
    }

    public bool Contains(DateTime dateTime)
    {
      var date = dateTime.Date;
      return StartDate <= dateTime && EndDate >= dateTime;
    }
      
    public bool Excludes(DateTime dateTime)
    {
      return !Contains(dateTime);
    }
     
    void populateDays()
    {
      days = Enumerable.Range(0, TotalDays)
        .Select(dayIndex => StartDate.AddDays(dayIndex))
        .ToList();
    }

  }
}

