﻿using System;
using System.Collections.Generic;

namespace LeadedSky.Shared
{
  public class CalendarTableView
  {
    public CalendarDataSource DataSource { get; private set; }
    public IList<DateTime> Sections { get { return DataSource.Days; } }

//    public List<CalendarDay> Days = new List<CalendarDay>();

    public CalendarTableView(CalendarDataSource dataSource)
    {
      this.DataSource = dataSource;
//      populateDays();
    }

//    public string GetSectonTitleAt(int index)
//    {
//      return GetSectionAt(index).Title;
//    }

//    public CalendarMonth GetSectionAt(int index)
//    {
//      if(index < 0 || index > Sections.Count)
//        index = 0;
//
//      return Sections[index];
//    }
//      
//    protected void populateDays()
//    {
//      foreach(var month in Sections)
//        Days.AddRange(month.DaysInMonth);
////      Sections = DataSource.ToDictionary();
//    }

  }
}

