﻿using System;
using System.Collections.Generic;

namespace LeadedSky.Shared
{
  public class CalendarDataSource
  {

    public IList<CalendarMonth> Months { get; set; }

    public CalendarDataSource()
    {
      Months = new List<CalendarMonth>();
    }

    public static CalendarDataSource CreateRange(DateTime startDate, DateTime endDate)
    {
      var monthDiff = Math.Abs((startDate.Year * 12 + startDate.Month) - (endDate.Year * 12 + endDate.Month));

      var calDataSource = new CalendarDataSource();

      for(int i = 0; i < monthDiff; i++)
        calDataSource.AddMonth(startDate.AddMonths(i));

      return calDataSource;
    }
      

    public DateTime? GetDateByIndex(int monthIndex, int dayIndex)
    {
      var month = GetMonthByIndex(monthIndex);
      if(month == null)
        return null;

      var day = month.GetDayByIndex(dayIndex);
      if(day == null)
        return null;

      return day.Value;
    }

    public CalendarMonth GetMonthByIndex(int monthIndex)
    {
      if(Months.Count < monthIndex)
        return null;
      return Months[monthIndex];
    }

    public Dictionary<string, CalendarMonth> ToDictionary()
    {
      var dictionary = new Dictionary<string, CalendarMonth>();
      foreach(var month  in Months)
        dictionary.Add(month.ToUniqueIdentifier() , month);
      return dictionary;
    }

    protected void AddMonth(DateTime date)
    {
      Months.Add(CalendarMonth.Create(date));
    }
  }
}

