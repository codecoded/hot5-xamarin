﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace LeadedSky.Shared
{
  public class CalendarDataSource
  {

    public List<DateTime> Days { get; set; }

    public CalendarDataSource()
    {
      Days = new List<DateTime>();
    }

    public static CalendarDataSource CreateRange(DateTime startDate, DateTime endDate)
    {
      return CreateRange(new DateRange(startDate, endDate));
    }

    public static CalendarDataSource CreateRange(DateRange calendarRange)
    {
      var daysDiff = calendarRange.TotalDays;
      var calDataSource = new CalendarDataSource();
      calDataSource.Days = calendarRange.Days;
      return calDataSource;
    }

  }
}

