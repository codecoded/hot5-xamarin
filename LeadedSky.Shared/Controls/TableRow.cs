﻿using System;

namespace LeadedSky.Shared
{
  public enum TableRowItemSelectType
  {
    Checkbox,
    Radio
  }

  public enum TableHeaderChoiceType
  {
    All,
    Any
  }

  public class TableRow
  {
    public string Title { get; set; }

    public string Detail { get; set; }

    public string Value { get; set; }

    public bool? Selectable { get; set; }
  }

  public class TableRowHeader : TableRow
  {
    public bool Expanded { get; set; }
    public TableHeaderChoiceType ChoiceType {get;set;}
  }

  public class TableRowItem : TableRow
  {
    public bool IsSingleSelection{ get { return HeaderValue != null && SelectType == TableRowItemSelectType.Radio; } }

    public string HeaderValue { get; set; }

    public bool Selected { get; set; }

    public TableRowItemSelectType SelectType { get; set; }

    public object Tag { get; set; }
  }
}

