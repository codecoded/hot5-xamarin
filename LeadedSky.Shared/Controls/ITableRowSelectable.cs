﻿using System;
using System.Collections.Generic;

namespace LeadedSky.Shared
{
  public delegate void TableRowSelectedDelegate(TableRow tableRow);
  public delegate void TableRowsSelectedDelegate(List<TableRow> tableRows);


  public interface ITableRowSelectable
  {

    void TableRowSelected(TableRow selectedTableRow);
  }
}

