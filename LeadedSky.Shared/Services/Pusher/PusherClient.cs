﻿using Pusher;
using System.Threading.Tasks;
using System.Diagnostics;
using System;

namespace LeadedSky.Shared
{
  public static class PusherClient
  {
    public static Pusher.Pusher Pusher;
    //    static Pusher.Channel channel;

    public static event EventEmitter.EventEmittedHandler OnEventEmitted;

    public static async Task ConnectAndSubscribe(IConnectionFactory connectionFactory, string channel, Pusher.Options options)
    {
      await ConnectAsync(connectionFactory, options);
      await SubscribeAsync(channel);
    }

    public static async Task ConnectAsync(IConnectionFactory connectionFactory, Pusher.Options options)
    {
      Pusher = new Pusher.Pusher(connectionFactory, options);
      await Pusher.ConnectAsync();
    }

    public static void Connect(IConnectionFactory connectionFactory, Pusher.Options options)
    {
      Pusher = new Pusher.Pusher(connectionFactory, options);
      Pusher.Connect();
    }

    public  static async Task ChangeChannel(string oldChannel, string newChannel)
    {
      await UnsubscribeFromChannelAsync(oldChannel);
      await SubscribeAsync(newChannel);
    }

    public static async Task UnsubscribeFromChannelAsync(string channelName)
    {
//      if(channel == null)
//        return;
      await Pusher.UnsubscribeFromChannelAsync(channelName);
      Debug.WriteLine(string.Format("PusherClient::UnsubscribeFromChannelAsync channel={0}", channelName));

    }

    public static  void UnsubscribeFromChannel(string channelName)
    {
      Pusher.UnsubscribeFromChannelAsync(channelName);
      Debug.WriteLine(string.Format("PusherClient::UnsubscribeFromChannelAsync channel={0}", channelName));

    }

    public async static Task SubscribeAsync(string channelName)
    {
      if(Pusher.State != ConnectionState.Connected || Pusher.Connection == null)
        Pusher.Connect();

      if(Pusher.AlreadySubscribed(channelName))
        return;

      var newChannel = await Pusher.SubscribeToChannelAsync(channelName);
      Debug.WriteLine(string.Format("PusherClient::Subscribe channel={0}", channelName));
      newChannel.EventEmitted += OnEventEmitted;
    }

    public async static Task SubscribeAsync(string channelName, Action callback)
    {
      if(Pusher.State != ConnectionState.Connected || Pusher.Connection == null)
        Pusher.Connect();

      if(Pusher.AlreadySubscribed(channelName))
      {
        callback();
        return;
      }

      var newChannel = await Pusher.SubscribeToChannelAsync(channelName);
      Debug.WriteLine(string.Format("PusherClient::Subscribe channel={0}", channelName));
      newChannel.EventEmitted += OnEventEmitted;
      callback();
    }

    //    static void PublishEvent(object sender, IIncomingEvent incomingEvent)
    //    {
    //      Debug.WriteLine(string.Format("Pusher: eventName={0}, channel={1}, data={2}", incomingEvent.EventName, incomingEvent.Channel, incomingEvent.Data));
    //    }
  }
}

